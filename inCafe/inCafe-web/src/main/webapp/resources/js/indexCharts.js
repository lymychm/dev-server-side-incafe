/**
 * by LM
 */
function drawIndexCharts(visits, orders, calls) {

	google.charts.load('current', {
		'packages' : [ 'bar' ]
	});
	google.charts.setOnLoadCallback(drawStuff);

	function drawStuff() {
		var dataVisit = new google.visualization.arrayToDataTable([
				[ 'Date', 'Visits' ], visits ]);

		var optionsVisit = {

			axes : {
				x : {
					0 : {
						side : 'top',
						label : 'Visits per day'
					}
				}
			},
			bar : {
				groupWidth : "90%"
			},
			colors : [ 'orange' ]
		};

		var chartVisit = new google.charts.Bar(document
				.getElementById('top_x_visits'));
		chartVisit.draw(dataVisit, google.charts.Bar
				.convertOptions(optionsVisit));
		var dataOrders = new google.visualization.arrayToDataTable([
				[ 'Date', 'Orders' ], orders ]);

		var optionsOrders = {

			axes : {
				x : {
					0 : {
						side : 'top',
						label : 'Orders per day'
					}
				}
			},
			bar : {
				groupWidth : "90%"
			},
			colors : [ '#5c1189' ]
		};
		var chartOrder = new google.charts.Bar(document
				.getElementById('top_x_orders'));
		chartOrder.draw(dataOrders, google.charts.Bar
				.convertOptions(optionsOrders));

		var dataCalls = new google.visualization.arrayToDataTable([
				[ 'Date', 'Calls' ], calls ]);

		var optionsCalls = {

			axes : {
				x : {
					0 : {
						side : 'top',
						label : 'Calls  per day'
					}
				}
			},
			bar : {
				groupWidth : "90%"
			},
			colors : [ 'orange' ]
		};

		var chartCall = new google.charts.Bar(document
				.getElementById('top_x_calls'));
		chartCall.draw(dataCalls, google.charts.Bar
				.convertOptions(optionsCalls));
	}
	;
}