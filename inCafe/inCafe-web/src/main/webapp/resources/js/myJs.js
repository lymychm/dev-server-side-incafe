/**
 * by LM
 */

function goToTop() {
	window.scrollTo(0, 0);
}

$(document).ready(function() {
	$("#msgs").click(function() {
		$("div#msgs ul").hide("slow");
	});
});

$(document).ready(function() {
	$("div#msgs ul").show(0).delay(3000).hide(500, "linear");
});
