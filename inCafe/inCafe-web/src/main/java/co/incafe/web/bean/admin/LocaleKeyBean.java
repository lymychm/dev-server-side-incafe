package co.incafe.web.bean.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Locale;
import co.incafe.ejb.entity.WebLocaleKey;
import co.incafe.ejb.entity.WebLocaleValue;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.model.WebLocaleKeyService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.util.Pagination;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class LocaleKeyBean extends Pagination implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private WebLocaleKeyService service;

	@Inject
	private ApplicationBean applBean;

	private List<WebLocaleKey> entities;

	private WebLocaleKey entity;

	private String searchField;

	@PostConstruct
	public void init() {
		try {
			if (entities == null) {
				searchField = "";
				firstPage();
			}
			initEntity();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	private void initEntity() {
		entity = new WebLocaleKey();
		List<WebLocaleValue> values = new ArrayList<>();
		for (Locale value : applBean.getLocales()) {
			WebLocaleValue keyValue = new WebLocaleValue();
			keyValue.setLocale(value);
			keyValue.setWebLocaleKey(entity);
			values.add(keyValue);
		}
		entity.setWebLocaleValues(values);
	}

	public void menu(ActionEvent event) {
		try {
			searchField = "";
			WebUtils.goToPage("/faces/pages/locale/localeKey.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public String add() {
		String name = entity.getName().trim();
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			service.insert(entity);
			initEntity();
			count = count();
			refillEntities();
			fixLeftRightLimit();
			String msg = name + WebUtils.getMsg("msg.added");
			WebUtils.addMessage(FacesMessage.SEVERITY_INFO, msg, msg);
		} catch (EntityExistException e) {
			String msg = name + WebUtils.getMsg("msg.exist");
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/locale/localeKey.xhtml?faces-redirect=true";
	}

	public List<WebLocaleKey> getEntities() {
		return entities;
	}

	public void setEntities(List<WebLocaleKey> entities) {
		this.entities = entities;
	}

	public WebLocaleKey getEntity() {
		return entity;
	}

	public void setEntity(WebLocaleKey entity) {
		this.entity = entity;
	}

	@Override
	public void refillEntities() {
		try {
			int from = (page - 1) * quantityOnPage;
			if ((from == count) && count > 0) {
				page = page - 1;
			}
			entities = service.selectLimited((page - 1) * quantityOnPage, quantityOnPage, searchField);
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public long count() {
		try {
			return service.count(searchField);
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
		return 0;
	}

	@Override
	public int size() {
		return entities.size();
	}

	public String getSearchField() {
		return searchField;
	}

	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

}
