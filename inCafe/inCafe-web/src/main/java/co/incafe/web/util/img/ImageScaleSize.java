package co.incafe.web.util.img;

import java.awt.image.BufferedImage;

/**
 * @author LM
 *
 */
public class ImageScaleSize {

	private int width;

	private int height;

	public ImageScaleSize(BufferedImage originalImage, int etalonWidth, int etalonHeight) {
		int width = originalImage.getWidth();
		int height = originalImage.getHeight();
		double percent = 0d;
		if (width > height) {
			percent = width > etalonWidth ? (double) width / etalonWidth : 1;
		} else {
			percent = height > etalonHeight ? (double) height / etalonHeight : 1;
		}
		this.width = (int) (width / percent);
		this.height = (int) (height / percent);
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
