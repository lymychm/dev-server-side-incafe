package co.incafe.web.util.location;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.faces.context.FacesContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import co.incafe.util.general.LoggerUtil;

/**
 * @author LM
 * 
 */
public class Geolocation {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final String SRVICE_URL = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("google-geocode-service-url");

	private static final String LNG_XPATH = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("lng-xpath");

	private static final String LAT_XPATH = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("lat-xpath");

	public static Location getLocation(String address) {
		HttpURLConnection connection = null;
		try {
			URL url = new URL(SRVICE_URL + URLEncoder.encode(address, "UTF-8"));
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/xml");
			if (connection.getResponseCode() == 200) {
				return getLocationFromServiceResult(connection);
			}
		} catch (Exception e) {
			logger.error(Geolocation.class.getName(), e);
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return new Location("", "");
	}

	private static Location getLocationFromServiceResult(URLConnection connection) throws IOException, ParserConfigurationException, SAXException, XPathExpressionException {
		InputSource source = new InputSource(connection.getInputStream());
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.parse(source);

		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();

		String latitude = xpath.evaluate(LAT_XPATH, document);
		String longitude = xpath.evaluate(LNG_XPATH, document);
		return new Location(latitude, longitude);
	}

}
