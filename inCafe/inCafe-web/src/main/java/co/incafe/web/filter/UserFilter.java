package co.incafe.web.filter;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;
import co.incafe.web.listener.UserSessionCountBean;

public class UserFilter implements Filter {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@Inject
	private UserSessionCountBean userSessionCountBean;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		logger.info("UserFilter init: " + userSessionCountBean);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		logger.info(req.getRemoteUser() + " - " + req.getSession().getId());
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {

	}

}
