package co.incafe.web.bean.admin;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.model.UserService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.util.Pagination;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class UserBean extends Pagination implements Serializable {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final long serialVersionUID = 1L;

    @EJB
    private UserService userService;

    @Inject
    private UserDetailsBean userDetailsBean;

    private List<User> users;

    private String searchField;

    @PostConstruct
    public void init() {
	try {
	    searchField = "";
	    if (users == null) {
		firstPage();
	    }
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	    WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
	}
    }

    public void delete(User user) {
	try {
	    userService.delete(user);
	    count = count();
	    refillEntities();
	    fixLeftRightLimit();
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	    WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
	}
    }

    public void details(User user) {
	try {
	    userDetailsBean.setSelectedUser(user);
	    userDetailsBean.setMagic(null);
	    userDetailsBean.setDayTime("");
	    userDetailsBean.setSelectedRes(new Restaurant());
	    WebUtils.goToPage("/faces/pages/user/details.xhtml");
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	    WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
	}
    }

    public void menu(ActionEvent event) {
	try {
	    searchField = "";
	    // userDetailsBean.setUser(null);
	    firstPage();
	    WebUtils.goToPage("/faces/pages/user/user.xhtml");
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	}
    }

    @Override
    public void refillEntities() {
	try {
	    int from = (page - 1) * quantityOnPage;
	    if ((from == count) && count > 0) {
		page = page - 1;
	    }
	    users = userService.selectLimited((page - 1) * quantityOnPage, quantityOnPage, searchField);
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	}
    }

    @Override
    public long count() {
	try {
	    return userService.count(searchField);
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	}
	return 0;
    }

    @Override
    public int size() {
	return users.size();
    }

    public String getSearchField() {
	return searchField;
    }

    public void setSearchField(String searchField) {
	this.searchField = searchField;
    }

    public List<User> getUsers() {
	return users;
    }

    public void setUsers(List<User> Users) {
	this.users = Users;
    }

}
