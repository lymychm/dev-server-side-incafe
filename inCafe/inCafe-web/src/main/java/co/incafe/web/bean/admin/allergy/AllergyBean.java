package co.incafe.web.bean.admin.allergy;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Allergy;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.AllergyService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.bean.admin.DefaultAdminEntityBean;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class AllergyBean extends DefaultAdminEntityBean<Allergy> implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private AllergyService service;

	@Inject
	private ApplicationBean applBean;

	@Inject
	private AllergyDetailsBean allergyDetailsBean;

	private Map<Long, Boolean> selectedTags;

	@PostConstruct
	public void init() {
		try {
			defaultInit("/pages/allergy/allergy.xhtml", service, new Allergy());
			selectedTags = new HashMap<>();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public String add() {
		String name = getEntity().getName().trim();
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			getEntity().setTags(WebUtils.getSelectedListById(selectedTags, applBean.getTags()));
			service.insert(getEntity());
			setEntity(new Allergy());
			refillAndOrginizePagination();
			addMsgForShow(name + WebUtils.getMsg("msg.added"));
			WebUtils.clearSelected(selectedTags);
		} catch (EntityExistException e) {
			String msg = name + WebUtils.getMsg("msg.exist");
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		} catch (InCafeException e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/allergy/allergy.xhtml?faces-redirect=true";
	}

	@Override
	public void save(Allergy entity) {
		entity.setEditable(false);
		String name = entity.getName().trim();
		try {
			entity.setName(name);
			service.update(entity);
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " save", e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	@Override
	public void delete(Allergy entity) {
		try {
			logger.info("DELETE ALLERGY - " + entity + ", by user - " + FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			service.delete(entity);
			refillAndOrginizePagination();
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " delete", e);
		}
	}

	public void details(Allergy allergy) {
		try {
			allergyDetailsBean.setEditMode(false);
			allergyDetailsBean.setEntity(allergy);
			allergyDetailsBean.setSelectedTags(new HashMap<Long, Boolean>());
			for (Tag tag : allergy.getTags()) {
				allergyDetailsBean.getSelectedTags().put(tag.getId(), true);
			}
			WebUtils.goToPage("/faces/pages/allergy/details.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public Map<Long, Boolean> getSelectedTags() {
		return selectedTags;
	}

	public void setSelectedTags(Map<Long, Boolean> selectedTags) {
		this.selectedTags = selectedTags;
	}

}
