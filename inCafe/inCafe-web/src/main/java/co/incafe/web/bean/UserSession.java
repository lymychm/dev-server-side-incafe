package co.incafe.web.bean;

import java.io.Serializable;

/**
 * @author Mykhailo Lymych
 * 
 */
public class UserSession implements Serializable {

    private static final long serialVersionUID = 4882702820946601750L;

    private String seesionId;

    private String userName;

    private String userIp;

    public UserSession() {
    }

    public UserSession(String seesionId) {
	super();
	this.seesionId = seesionId;
    }

    public UserSession(String seesionId, String userName, String userIp) {
	super();
	this.seesionId = seesionId;
	this.userName = userName;
	this.userIp = userIp;
    }

    public String getSeesionId() {
	return seesionId;
    }

    public String getUserName() {
	return userName;
    }

    public String getUserIp() {
	return userIp;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((seesionId == null) ? 0 : seesionId.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	UserSession other = (UserSession) obj;
	if (seesionId == null) {
	    if (other.seesionId != null) {
		return false;
	    }
	} else if (!seesionId.equals(other.seesionId)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "UserSession [seesionId=" + seesionId + ", userName=" + userName + "]";
    }

}
