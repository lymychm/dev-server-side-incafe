package co.incafe.web.bean.chart;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.Visit;
import co.incafe.util.date.DateUtil;

public class VisitChart implements IncafeChart<Visit> {

    enum VisitType {

	UNIQUE,

	NEW

    }

    private String result;

    private int visitsTotal;

    private int uniqueVisitUsers;

    @Override
    public String data() {
	return result;
    }

    public void prepareData(List<Visit> visits) {
	Map<Calendar, Map<VisitType, List<Visit>>> testMap = new TreeMap<>();
	setVisitData(visits, testMap);
	setMissingDays(testMap);
	StringBuffer sb = new StringBuffer();
	prepareVisitStringResult(testMap, sb);
	if (sb.length() > 0) {
	    sb.setLength(sb.length() - 1);
	    result = sb.toString();
	} else {
	    result = null;
	}
    }

    private void setVisitData(List<Visit> visits, Map<Calendar, Map<VisitType, List<Visit>>> testMap) {
	Set<User> uniqueUsers = new HashSet<>();
	for (Visit visit : visits) {
	    Calendar inTime = DateUtil.removeTimeFromDate(visit.getInTime());
	    if (testMap.containsKey(inTime)) {
		Map<VisitType, List<Visit>> map = testMap.get(inTime);
		boolean newUserThisDay = true;
		List<Visit> list = map.get(VisitType.UNIQUE);
		if (list == null) {
		    map.put(VisitType.UNIQUE, new ArrayList<>());
		} else {
		    for (Visit v : list) {
			if (v.getUser().equals(visit.getUser())) {
			    newUserThisDay = false;
			}
		    }
		}
		if (newUserThisDay) {
		    visitsTotal += 1;
		    list.add(visit);
		}

		if (!uniqueUsers.contains(visit.getUser())) {
		    map.get(VisitType.NEW).add(visit);
		}
	    } else {
		visitsTotal += 1;
		List<Visit> unique = new ArrayList<>();
		unique.add(visit);
		Map<VisitType, List<Visit>> typeMap = new HashMap<>();
		typeMap.put(VisitType.UNIQUE, unique);
		typeMap.put(VisitType.NEW, new ArrayList<>());
		if (!uniqueUsers.contains(visit.getUser())) {
		    typeMap.get(VisitType.NEW).add(visit);
		}
		List<Visit> total = new ArrayList<>();
		total.add(visit);
		testMap.put(inTime, typeMap);
	    }
	    uniqueUsers.add(visit.getUser());
	}
	uniqueVisitUsers = uniqueUsers.size();
    }

    private void prepareVisitStringResult(Map<Calendar, Map<VisitType, List<Visit>>> entries, StringBuffer sb) {
	for (Map.Entry<Calendar, Map<VisitType, List<Visit>>> map : entries.entrySet()) {
	    int day = map.getKey().get(Calendar.DAY_OF_MONTH);
	    int month = map.getKey().get(Calendar.MONTH) + 1;
	    int year = map.getKey().get(Calendar.YEAR);
	    int unique = map.getValue().get(VisitType.UNIQUE).size();

	    int newSize = map.getValue().get(VisitType.NEW).size();
	    sb.append("[").append("'").append(fixNumber(day)).append("-").append(fixNumber(month)).append("-").append(year).append("'").append(", ").append(unique).append(", ")
		    .append(newSize).append("],");
	}
    }

    private String fixNumber(int number) {
	if (number < 10) {
	    return "0" + number;
	}
	return "" + number;
    }

    private <T> void setMissingDays(Map<Calendar, Map<VisitType, List<Visit>>> testMap) {
	Calendar currDate = Calendar.getInstance();
	Calendar jan19 = Calendar.getInstance();
	jan19.set(Calendar.YEAR, 2016);
	jan19.set(Calendar.MONTH, Calendar.JANUARY);
	jan19.set(Calendar.DAY_OF_MONTH, 19);
	int days = 0;
	while (jan19.before(currDate)) {
	    jan19.add(Calendar.DAY_OF_MONTH, 1);
	    days++;
	}
	for (int i = 0; i < days; i++) {
	    Calendar calendar = Calendar.getInstance();
	    calendar = DateUtil.removeTimeFromDate(calendar);
	    calendar.add(Calendar.DAY_OF_MONTH, -i);
	    if (!testMap.containsKey(calendar)) {
		Map<VisitType, List<Visit>> hashMap = new HashMap<>();
		hashMap.put(VisitType.NEW, new ArrayList<>());
		hashMap.put(VisitType.UNIQUE, new ArrayList<>());
		testMap.put(calendar, hashMap);
	    }
	}
    }

    public int getVisitsTotal() {
	return visitsTotal;
    }

    public int getUniqueVisitUsers() {
	return uniqueVisitUsers;
    }

    public String getResult() {
	return result;
    }

}
