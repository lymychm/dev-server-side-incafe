package co.incafe.web.listener;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.UserSession;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
public class UserSessionCountBean implements Serializable, HttpSessionListener {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final long serialVersionUID = -2967765539327431784L;

    private static AtomicInteger sessions = new AtomicInteger(0);

    @Override
    public void sessionCreated(HttpSessionEvent se) {
	logger.info("sessionCreated: " + se.getSession().getId());
	sessions.incrementAndGet();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void sessionDestroyed(HttpSessionEvent se) {
	String sessionId = se.getSession().getId();
	try {
	    ServletContext sc = se.getSession().getServletContext();
	    List<UserSession> userSessions = (List<UserSession>) sc.getAttribute("userSessions");
	    UserSession userSession = new UserSession(sessionId);
	    if (userSessions != null && userSessions.contains(userSession)) {
		userSessions.remove(userSession);
	    }
	    logger.info("sessionDestroyed: " + sessionId);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	}
	sessions.decrementAndGet();
    }

    public AtomicInteger getSessions() {
	return sessions;
    }

}
