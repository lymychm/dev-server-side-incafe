package co.incafe.web.bean;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@RequestScoped
public class AuthorizationBean implements Serializable {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = -6695787749987658753L;

	@Inject
	private ApplicationBean applicationBean;

	@Inject
	private GeneralBean generalBean;

	private String user;

	public String getUser() {
		user = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
		return user;
	}

	public static boolean isRole(String role) {
		return FacesContext.getCurrentInstance().getExternalContext().isUserInRole(role);
	}

	public void logout(ActionEvent event) {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) context.getRequest();
		try {
			applicationBean.getUserSessions().remove(generalBean.getUserSession());
			WebUtils.goToPage("/");
			request.logout();
			context.invalidateSession();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

}