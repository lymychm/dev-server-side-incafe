package co.incafe.web.bean.admin.dishtype;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.model.DishTypeService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class DishTypeDetailsBean implements Serializable {

    protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final long serialVersionUID = 1L;

    @EJB
    private DishTypeService service;

    @Inject
    private ApplicationBean applBean;

    private Map<Long, Boolean> selectedMainTags;

    private DishType entity;

    private boolean editMode;

    @PostConstruct
    public void init() {
	try {
	    selectedMainTags = new HashMap<>();
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	}
    }

    public void edit() {
	editMode = true;
    }

    public String save() {
	try {
	    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    editMode = false;
	    entity.setMainTags(WebUtils.getSelectedListById(selectedMainTags, applBean.getMainTags()));
	    service.update(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
	}
	return "/pages/dishType/details.xhtml?faces-redirect=true";
    }

    public void cancel() {
	editMode = false;
    }

    public Map<Long, Boolean> getSelectedMainTags() {
	return selectedMainTags;
    }

    public void setSelectedMainTags(Map<Long, Boolean> selectedMainTags) {
	this.selectedMainTags = selectedMainTags;
    }

    public DishType getEntity() {
	return entity;
    }

    public void setEntity(DishType entity) {
	this.entity = entity;
    }

    public boolean isEditMode() {
	return editMode;
    }

    public void setEditMode(boolean editMode) {
	this.editMode = editMode;
    }

}
