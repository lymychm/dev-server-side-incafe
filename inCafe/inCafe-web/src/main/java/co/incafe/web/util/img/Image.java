package co.incafe.web.util.img;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.inject.Named;
import javax.servlet.http.Part;

import net.coobird.thumbnailator.Thumbnails;

/**
 * @author LM
 * 
 */
@Named
@ApplicationScoped
public class Image {

	public static final String IMAGES_FOLDER = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("ImageFolder");

	public static final String JPG_FORMAT = ".jpg";

	public static final String PNG_FORMAT = ".png";

	public static String save(Part part, String folder) throws IOException {
		String fileName = System.nanoTime() + JPG_FORMAT;
		BufferedImage image = getBufferedImage(part.getInputStream());
		return saveImage(image, folder, fileName);
	}

	public static String seveImagesToServerOld(InputStream inputStream, String folder, int width, int height) throws IOException {
		BufferedImage img = getBufferedImage(inputStream);
		ImageScaleSize size = new ImageScaleSize(img, width, height);
		String fileName = System.nanoTime() + JPG_FORMAT;
		img = getScaledImage(img, size, img.getType());
		return saveImage(img, folder, fileName);
	}

	public static String seveImagesToServer(InputStream inputStream, String folder, int size) throws IOException {
		try {
			BufferedImage bufferedImage = getBufferedImage(inputStream);
			if (bufferedImage.getWidth() > size || bufferedImage.getWidth() > size) {
				bufferedImage = Thumbnails.of(bufferedImage).width(size).height(size).asBufferedImage();
			}
			String fileName = System.nanoTime() + PNG_FORMAT;
			return saveImage(bufferedImage, folder, fileName);
		} catch (IOException e) {
			throw new IOException(e);
		}
	}

	private static BufferedImage getBufferedImage(InputStream inputStream) throws IOException {
		try {
			return ImageIO.read(inputStream);
		} catch (IOException e) {
			throw new IOException(e);
		} finally {
			inputStream.close();
		}
	}

	private static BufferedImage getScaledImage(BufferedImage originalImage, ImageScaleSize size, int type) {
		BufferedImage resizedImage = new BufferedImage(size.getWidth(), size.getHeight(), type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, size.getWidth(), size.getHeight(), null);
		g.dispose();
		return resizedImage;
	}

	private static String saveImage(BufferedImage image, String folder, String fileName) throws IOException {
		checkPath(folder); // if folders does not exist, folders tree will be created
		String result = folder + fileName;
		ImageIO.write(image, "png", new File(IMAGES_FOLDER + result));
		result = folder + fileName;
		return result;
	}

	private static void checkPath(String path) throws IOException {
		File filePath = new File(IMAGES_FOLDER + path);
		if (!filePath.exists()) {
			boolean result = filePath.mkdirs();
			if (result == false) {
				throw new IOException("Can't create folders - " + filePath);
			}
		}
	}

	public static void deleteFile(String filePath) {
		if (filePath != null && filePath.length() > 0) {
			File file = new File(IMAGES_FOLDER + filePath);
			file.delete();
		}
	}

}
