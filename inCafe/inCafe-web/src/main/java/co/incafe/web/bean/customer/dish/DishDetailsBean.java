package co.incafe.web.bean.customer.dish;

import java.io.Serializable;
import java.util.Calendar;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.model.DishService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.util.WebUtils;
import co.incafe.web.util.img.Image;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class DishDetailsBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final long serialVersionUID = 1L;

    @EJB
    private DishService dishService;

    @Inject
    private DishGeneralBean dishGeneralBean;

    private Dish dish;

    private boolean dishStatus = true;

    private boolean dishOrDrink = true;

    private boolean editMode;

    public void edit() {
	editMode = true;
    }

    public String save() {
	try {
	    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    dishGeneralBean.setData(dish);
	    dish.setStatus(dishStatus ? 1 : 0);
	    dish.setIsDish(dishOrDrink ? 1 : 0);
	    if (dish.getId() == null) {
		dish.setCreateDate(Calendar.getInstance());
		if (dish.getRating() == null) {
		    dish.setRating(0D);
		}
		if (dish.getPositionInMenu() == null) {
		    dish.setPositionInMenu(dish.getDishType().getId().intValue());
		}
		dishService.insert(dish);
		int isDish = dish.getIsDish();
		String msg = dish.getName() + WebUtils.getMsg("msg.added");
		dish = new Dish();
		dish.setIsDish(isDish);
		WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
	    } else {
		dish.setUpdateDate(Calendar.getInstance());
		dishService.update(dish);
	    }
	    editMode = false;
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	    WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
	}
	return "/pages/dish/details.xhtml?faces-redirect=true";
    }

    public String deletePhoto() {
	try {
	    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	    if (dish.getPhotoBig() != null) {
		Image.deleteFile(dish.getPhotoBig());
	    }
	    if (dish.getPhotoMini() != null) {
		Image.deleteFile(dish.getPhotoMini());
	    }
	    dish.setPhotoBig(null);
	    dish.setPhotoMini(null);
	    dishService.update(dish);
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	    WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
	}
	return "/pages/dish/details.xhtml?faces-redirect=true";
    }

    public void cancel() {
	editMode = false;
    }

    public boolean isEditMode() {
	return editMode;
    }

    public void setEditMode(boolean editMode) {
	this.editMode = editMode;
    }

    public Dish getDish() {
	return dish;
    }

    public void setDish(Dish dish) {
	this.dish = dish;
    }

    public boolean isDishStatus() {
	return dishStatus;
    }

    public void setDishStatus(boolean dishStatus) {
	this.dishStatus = dishStatus;
    }

    public boolean isDishOrDrink() {
	return dishOrDrink;
    }

    public void setDishOrDrink(boolean dishOrDrink) {
	this.dishOrDrink = dishOrDrink;
    }

}
