package co.incafe.web.bean.admin;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Event;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.EventService;
import co.incafe.ejb.model.RestaurantService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.GeneralBean;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class EventBean extends DefaultAdminEntityBean<Event> implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	private static final long serialVersionUID = 1L;

	@Inject
	private GeneralBean generalBean;

	@Inject
	private EventDetailsBean eventDetailsBean;

	@EJB
	private EventService eventService;

	@EJB
	private RestaurantService restaurantService;

	private List<Event> entities;

	private List<Restaurant> restaurants;

	private Map<Long, Boolean> selectedRes;

	private String end;

	private String start;

	@PostConstruct
	public void init() {
		try {
			defaultInit("/pages/event/event.xhtml", eventService, new Event());
			restaurants = restaurantService.selectActiveByChain(generalBean.getRestaurantChain());
			selectedRes = new HashMap<>();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public String add() {
		String name = getEntity().getName().trim();
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			Calendar startDate = Calendar.getInstance();
			startDate.setTime(DATE_FORMAT.parse(start));
			getEntity().setStartDate(startDate);
			getEntity().setRestaurantChain(generalBean.getRestaurantChain());
			getEntity().setRestaurants(WebUtils.getSelectedListById(selectedRes, restaurants));

			Calendar endDate = Calendar.getInstance();
			endDate.setTime(DATE_FORMAT.parse(end));
			getEntity().setEndDate(endDate);

			eventService.insert(getEntity());
			setEntity(new Event());
			refillAndOrginizePagination();
			WebUtils.clearSelected(selectedRes);
			addMsgForShow(name + WebUtils.getMsg("msg.added"));
		} catch (EntityExistException e) {
			String msg = name + WebUtils.getMsg("msg.exist");
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/event/event.xhtml?faces-redirect=true";
	}

	@Override
	public void save(Event entity) {
		entity.setEditable(false);
		String name = entity.getName().trim();
		try {
			entity.setName(name);
			eventService.update(entity);
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " save", e);
		}
	}

	@Override
	public void delete(Event entity) {
		try {
			logger.info("DELETE Event - " + entity + ", by user - " + FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			eventService.delete(entity);
			refillAndOrginizePagination();
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " delete", e);
		}
	}

	public void details(Event event) {
		try {
			eventDetailsBean.setEditMode(false);
			eventDetailsBean.setEntity(event);
			eventDetailsBean.setSelectedRes(new HashMap<Long, Boolean>());
			eventDetailsBean.setEnd(DATE_FORMAT.format(event.getEndDate().getTime()));
			eventDetailsBean.setStart(DATE_FORMAT.format(event.getStartDate().getTime()));
			for (Restaurant restaurant : event.getRestaurants()) {
				eventDetailsBean.getSelectedRes().put(restaurant.getId(), true);
			}
			WebUtils.goToPage("/faces/pages/event/details.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	@Override
	public void refillEntities() {
		try {
			int from = (page - 1) * quantityOnPage;
			if ((from == count) && count > 0) {
				page = page - 1;
			}
			entities = eventService.selectLimited((page - 1) * quantityOnPage, quantityOnPage, generalBean.getRestaurantChain(), getSearchField());
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public long count() {
		try {
			return eventService.count(generalBean.getRestaurantChain(), getSearchField());
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
		return 0;
	}

	@Override
	public int size() {
		return entities.size();
	}

	public List<Restaurant> getRestaurants() {
		return restaurants;
	}

	public void setRestaurants(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public Map<Long, Boolean> getSelectedRes() {
		return selectedRes;
	}

	public void setSelectedRes(Map<Long, Boolean> selectedRes) {
		this.selectedRes = selectedRes;
	}

	public List<Event> getEntities() {
		return entities;
	}

	public void setEntities(List<Event> entities) {
		this.entities = entities;
	}

}
