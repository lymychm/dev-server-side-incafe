package co.incafe.web.bean.customer.dish;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.MainTag;
import co.incafe.ejb.entity.MealTime;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.bean.GeneralBean;
import co.incafe.web.util.WebUtils;
import co.incafe.web.util.img.Image;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class DishGeneralBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final long serialVersionUID = 1L;

    private Part file;

    public static final String IMG_FOLDER = "dish/";

    @Inject
    private ApplicationBean applBean;

    @Inject
    private DishDetailsBean dishDetailsBean;

    @Inject
    private GeneralBean generalBean;

    private DishType dishType;

    private MainTag mainTag;

    private Map<Long, Boolean> selectedRest;

    private Map<Long, Boolean> selectedTags;

    private Map<Long, Boolean> selectedMealTime;

    @PostConstruct
    public void init() {
	try {
	    selectedTags = new HashMap<>();
	    selectedMealTime = new HashMap<>();
	    selectedRest = new HashMap<>();
	    dishType = new DishType();
	    mainTag = new MainTag();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	}
    }

    public void clear() {
	file = null;
	WebUtils.clearSelected(selectedTags, selectedMealTime, selectedRest);
    }

    public void setData(Dish dish) throws InCafeException {
	try {
	    dish.setTags(WebUtils.getSelectedListById(selectedTags, applBean.getTags()));
	    dish.setMealTimes(WebUtils.getSelectedListById(selectedMealTime, applBean.getMealTimes()));
	    dish.setRestaurants(WebUtils.getSelectedListById(selectedRest, generalBean.getRestaurantChain().getRestaurants()));
	    dish.setDishType(applBean.getDishTypes().get(applBean.getDishTypes().indexOf(dishType)));
	    if (mainTag.getId() != null) {
		dish.setMainTag(applBean.getMainTags().get(applBean.getMainTags().indexOf(mainTag)));
	    }
	    if (file != null && file.getSize() > 0) {
		if (dish.getId() != null) {
		    Image.deleteFile(dish.getPhotoBig());
		    Image.deleteFile(dish.getPhotoMini());
		}
		setPhotoData(dish);
	    }
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    public void setDataForDetails(Dish dish) {
	dishDetailsBean.setEditMode(false);
	dishDetailsBean.setDishStatus((dish.getStatus() == null || dish.getStatus() == 0) ? false : true);
	dishDetailsBean.setDishOrDrink((dish.getStatus() == null || dish.getIsDish() == 0) ? false : true);
	clear();
	for (Tag tag : dish.getTags()) {
	    selectedTags.put(tag.getId(), true);
	}
	for (MealTime time : dish.getMealTimes()) {
	    selectedMealTime.put(time.getId(), true);
	}
	for (Restaurant res : dish.getRestaurants()) {
	    selectedRest.put(res.getId(), true);
	}
	dishDetailsBean.setDish(dish);
	dishType = dish.getDishType();
	if (dish.getMainTag() != null) {
	    mainTag = dish.getMainTag();
	}
    }

    private void setPhotoData(Dish dish) throws Exception {
	String path = IMG_FOLDER + generalBean.getRestaurantChain().getName() + "/";
	String photoBig = Image.seveImagesToServer(file.getInputStream(), path, ApplicationBean.DISH_IMG_BIG_SIZE);
	dish.setPhotoBig(photoBig);

	String photoPathMini = Image.seveImagesToServer(file.getInputStream(), path, ApplicationBean.DISH_IMG_SMALL_SIZE);
	dish.setPhotoMini(photoPathMini);
    }

    public Part getFile() {
	return file;
    }

    public void setFile(Part file) {
	this.file = file;
    }

    public Map<Long, Boolean> getSelectedTags() {
	return selectedTags;
    }

    public void setSelectedTags(Map<Long, Boolean> selectedTags) {
	this.selectedTags = selectedTags;
    }

    public Map<Long, Boolean> getSelectedMealTime() {
	return selectedMealTime;
    }

    public void setSelectedMealTime(Map<Long, Boolean> selectedMealTime) {
	this.selectedMealTime = selectedMealTime;
    }

    public Map<Long, Boolean> getSelectedRest() {
	return selectedRest;
    }

    public void setSelectedRest(Map<Long, Boolean> selectedRest) {
	this.selectedRest = selectedRest;
    }

    public DishType getDishType() {
	return dishType;
    }

    public void setDishType(DishType dishType) {
	this.dishType = dishType;
    }

    public MainTag getMainTag() {
	return mainTag;
    }

    public void setMainTag(MainTag mainTag) {
	this.mainTag = mainTag;
    }

}
