package co.incafe.web.bean.customer.restaurant;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.model.RestaurantService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.GeneralBean;
import co.incafe.web.util.Pagination;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class RestaurantBean extends Pagination implements Serializable {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private RestaurantService restaurantService;

	@Inject
	private GeneralBean generalBean;

	@Inject
	private RestaurantDetailsBean restaurantDetailsBean;

	@Inject
	private RestaurantGeneralBean restaurantGeneralBean;

	private List<Restaurant> restaurants;

	private String searchField;

	@PostConstruct
	public void init() {
		try {
			searchField = "";
			if (restaurants == null) {
				firstPage();
			}
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public void delete(Restaurant restaurant) {
		try {
			restaurantService.delete(restaurant);
			refillEntities();
			fixLeftRightLimit();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public void details(Restaurant restaurant) {
		try {
			restaurantDetailsBean.setRestaurant(restaurant);
			restaurantGeneralBean.setCountry(restaurant.getCity().getCountry());
			restaurantGeneralBean.setCity(restaurant.getCity());
			restaurantDetailsBean.setEditMode(true);
			WebUtils.goToPage("/faces/pages/restaurant/details.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public void menu(ActionEvent event) {
		try {
			searchField = "";
			firstPage();
			WebUtils.goToPage("/faces/pages/restaurant/restaurant.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public void refillEntities() {
		try {
			int from = (page - 1) * quantityOnPage;
			if ((from == count) && count > 0) {
				page = page - 1;
			}
			restaurants = restaurantService.selectLimitedByChain((page - 1) * quantityOnPage, quantityOnPage, generalBean.getRestaurantChain(), searchField);
			// restaurants = restaurantService.selectByChain(generalBean.getRestaurantChain());
			// TODO
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public long count() {
		try {
			return restaurantService.count(generalBean.getRestaurantChain(), searchField);
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
		return 0;
	}

	@Override
	public int size() {
		return restaurants.size();
	}

	public String getSearchField() {
		return searchField;
	}

	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	public List<Restaurant> getRestaurants() {
		return restaurants;
	}

	public void setRestaurants(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}

}
