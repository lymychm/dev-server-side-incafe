package co.incafe.web.bean.admin.prefer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.PreferService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.bean.admin.DefaultAdminEntityBean;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class PreferBean extends DefaultAdminEntityBean<Prefer> implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private PreferService service;

	@Inject
	private ApplicationBean applBean;

	@Inject
	private PreferDetailsBean preferDetailsBean;

	private Map<Long, Boolean> selectedTags;

	@PostConstruct
	public void init() {
		try {
			defaultInit("/pages/prefer/prefer.xhtml", service, new Prefer());
			selectedTags = new HashMap<>();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public String add() {
		String name = getEntity().getName().trim();
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

			getEntity().setName(name);

			getEntity().setTags(WebUtils.getSelectedListById(selectedTags, applBean.getTags()));

			service.insert(getEntity());

			setEntity(new Prefer());
			refillAndOrginizePagination();
			WebUtils.clearSelected(selectedTags);
			addMsgForShow(name + WebUtils.getMsg("msg.added"));
		} catch (EntityExistException e) {
			String msg = name + WebUtils.getMsg("msg.exist");
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		} catch (InCafeException e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/prefer/prefer.xhtml?faces-redirect=true";
	}

	@Override
	public void save(Prefer entity) {
		// NOP
	}

	@Override
	public void delete(Prefer entity) {
		try {
			logger.info("DELETE Prefer - " + entity + ", by user - " + FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			service.delete(entity);
			refillAndOrginizePagination();
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " delete", e);
		}
	}

	public void menu(ActionEvent event) {
		try {
			setSearchField("");
			setEntity(new Prefer());
			preferDetailsBean.setEntity(null);
			firstPage();
			WebUtils.goToPage("/faces" + getWebPage());
			WebUtils.clearSelected(selectedTags);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public void details(Prefer prefer) {
		try {
			preferDetailsBean.setEditMode(false);
			preferDetailsBean.setEntity(prefer);
			preferDetailsBean.setSelectedTags(new HashMap<Long, Boolean>());
			for (Tag tag : prefer.getTags()) {
				preferDetailsBean.getSelectedTags().put(tag.getId(), true);
			}
			WebUtils.goToPage("/faces/pages/prefer/details.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public Map<Long, Boolean> getSelectedTags() {
		return selectedTags;
	}

	public void setSelectedTags(Map<Long, Boolean> selectedTags) {
		this.selectedTags = selectedTags;
	}

}
