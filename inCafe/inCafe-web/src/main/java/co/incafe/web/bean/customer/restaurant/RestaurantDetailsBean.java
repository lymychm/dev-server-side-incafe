package co.incafe.web.bean.customer.restaurant;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.model.RestaurantService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class RestaurantDetailsBean implements Serializable {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private RestaurantService restaurantService;

	private boolean editMode;

	private Restaurant Restaurant;

	@PostConstruct
	private void init() {
		logger.info("RestaurantDetailsBean init");
	}

	public void edit() {
		editMode = false;
	}

	public String save() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			if (FacesContext.getCurrentInstance().getMessageList().size() == 0) {
				editMode = true;
			}
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/restaurant/details.xhtml?faces-redirect=true";
	}

	public void cancel() {
		editMode = true;
	}

	public Restaurant getRestaurant() {
		return Restaurant;
	}

	public void setRestaurant(Restaurant Restaurant) {
		this.Restaurant = Restaurant;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

}
