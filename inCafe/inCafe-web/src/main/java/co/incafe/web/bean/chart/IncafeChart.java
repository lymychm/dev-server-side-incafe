package co.incafe.web.bean.chart;

import java.util.List;

public interface IncafeChart<T> {

    public String data();

    public void prepareData(List<T> visits);

}
