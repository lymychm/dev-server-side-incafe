package co.incafe.web.bean.customer.dish;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.model.DishService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.GeneralBean;
import co.incafe.web.util.Pagination;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class DishBean extends Pagination implements Serializable {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private DishService dishService;

	@Inject
	private GeneralBean generalBean;

	@Inject
	private DishGeneralBean dishGeneralBean;

	@Inject
	private DishDetailsBean dishDetailsBean;

	private List<Dish> dishs;

	private String searchField;

	@PostConstruct
	public void init() {
		try {
			searchField = "";
			if (dishs == null) {
				firstPage();
			}
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public void add() {
		try {
			logger.info("ADD DISH");
			dishGeneralBean.clear();
			dishGeneralBean.setDishType(new DishType());
			Dish dish = new Dish();
			dish.setIsDish(1);
			dish.setPositionInMenu(99);
			dishDetailsBean.setDish(dish);
			dishDetailsBean.setEditMode(true);
			for (Restaurant restaurant : generalBean.getRestaurantChain().getRestaurants()) {
				dishGeneralBean.getSelectedRest().put(restaurant.getId(), true);
			}
			WebUtils.goToPage("/faces/pages/dish/details.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public void delete(Dish dish) {
		try {
			dishService.delete(dish);
			count = count();
			refillEntities();
			fixLeftRightLimit();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public void details(Dish dish) {
		try {
			dishGeneralBean.setDataForDetails(dish);
			WebUtils.goToPage("/faces/pages/dish/details.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public void menu(ActionEvent event) {
		try {
			searchField = "";
			dishDetailsBean.setDish(null);
			firstPage();
			WebUtils.goToPage("/faces/pages/dish/dish.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public void refillEntities() {
		try {
			int from = (page - 1) * quantityOnPage;
			if ((from == count) && count > 0) {
				page = page - 1;
			}
			dishs = dishService.selectLimitedByRestaurantChain((page - 1) * quantityOnPage, quantityOnPage, generalBean.getRestaurantChain(), searchField);
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public long count() {
		try {
			return dishService.count((page - 1) * quantityOnPage, quantityOnPage, generalBean.getRestaurantChain(), searchField);
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
		return 0;
	}

	@Override
	public int size() {
		return dishs.size();
	}

	public String getSearchField() {
		return searchField;
	}

	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

	public List<Dish> getDishs() {
		return dishs;
	}

	public void setDishs(List<Dish> dishs) {
		this.dishs = dishs;
	}

}
