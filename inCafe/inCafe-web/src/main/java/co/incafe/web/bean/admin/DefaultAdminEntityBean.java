package co.incafe.web.bean.admin;

import static co.incafe.web.util.WebUtils.addMessage;

import java.lang.reflect.Field;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.model.InCafeWebAdminBeanService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.util.Pagination;
import co.incafe.web.util.WebUtils;

/**
 * @author LM
 * 
 * @param <T>
 */
public abstract class DefaultAdminEntityBean<T> extends Pagination {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private String webPage;

	private InCafeWebAdminBeanService<T> service;

	private List<T> entities;

	private T entity;

	private String searchField;

	protected void defaultInit(String webPage, InCafeWebAdminBeanService<T> service, T entity) throws Exception {
		searchField = "";
		this.webPage = webPage;
		this.service = service;
		this.entity = entity;
		if (entities == null) {
			firstPage();
		}
	}

	@Override
	public void refillEntities() {
		try {
			int from = (page - 1) * quantityOnPage;
			if ((from == count) && count > 0) {
				page = page - 1;
			}
			entities = service.selectLimited((page - 1) * quantityOnPage, quantityOnPage, searchField);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
		}
	}

	@Override
	public long count() {
		try {
			return service.count(searchField);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
		}
		return 0;
	}

	@Override
	public int size() {
		return entities.size();
	}

	@SuppressWarnings("unchecked")
	public void menu(ActionEvent event) {
		try {
			searchField = "";
			setEntity((T) getEntity().getClass().newInstance());
			firstPage();
			WebUtils.goToPage("/faces" + webPage);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	protected void addMsgForShow(String msg) {
		addMessage(FacesMessage.SEVERITY_INFO, msg, msg);
	}

	protected void destroyBean() {
		service = null;
		entities = null;
		entity = null;
		count = null;
		page = null;
		quantityOnPage = null;
		searchField = null;
	}

	public void refillAndOrginizePagination() {
		count = count();
		refillEntities();
		fixLeftRightLimit();
	}

	public abstract String add();

	public abstract void save(T entity);

	public abstract void delete(T entity);

	public void edit(T entity) {
		try {
			String idFieldName = "id";
			String editableFieldName = "editable";
			long entityId = (long) field(entity, idFieldName).get(entity);
			for (T e : entities) {
				long id = (long) field(e, idFieldName).get(e);
				if (id == entityId) {
					field(entity, editableFieldName).set(entity, true);
				} else {
					field(e, editableFieldName).set(e, false);
				}
			}
		} catch (Exception e) {
			logger.info(getClass().getName() + " edit: ", e);
		}
	}

	public Field field(T entity, String fieldName) throws NoSuchFieldException {
		Field field = entity.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);
		return field;
	}

	public void cancel(T entity) {
		try {
			field(entity, "editable").set(entity, false);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void setService(InCafeWebAdminBeanService<T> service) {
		this.service = service;
	}

	public String getWebPage() {
		return webPage;
	}

	public void setWebPage(String webPage) {
		this.webPage = webPage;
	}

	public List<T> getEntities() {
		return entities;
	}

	public void setEntities(List<T> entities) {
		this.entities = entities;
	}

	public T getEntity() {
		return entity;
	}

	public void setEntity(T entity) {
		this.entity = entity;
	}

	public String getSearchField() {
		return searchField;
	}

	public void setSearchField(String searchField) {
		this.searchField = searchField;
	}

}
