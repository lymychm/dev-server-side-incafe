package co.incafe.web.bean.admin;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.model.DishService;
import co.incafe.ejb.model.MagicService;
import co.incafe.ejb.model.RestaurantService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class UserDetailsBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final long serialVersionUID = 1L;

    @EJB
    private DishService dishService;

    @EJB
    private MagicService magicService;

    @EJB
    private RestaurantService restaurantService;

    private List<Dish> magic;

    private User selectedUser;

    private Restaurant selectedRes;

    private List<Restaurant> restaurants;

    private String dayTime;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @PostConstruct
    public void init() {
	try {
	    selectedRes = new Restaurant();
	    restaurants = restaurantService.selectAllTest();
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	    WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
	}
    }

    public void doMagic() {
	try {
	    Date date = sdf.parse("1970-01-01 " + dayTime);
	    magic = magicService.magicDishes(selectedUser.getGuid(), restaurants.get(restaurants.indexOf(selectedRes)).getTables().get(0).getIbeacon(), date.getTime());
	    // magic.forEach(dish -> logger.info("TYPE: " + dish.getDishType().getId() + ", dish: " + dish));
	} catch (Exception e) {
	    logger.info(getClass().getName(), e);
	    WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
	}
    }

    public List<Dish> getMagic() {
	return magic;
    }

    public void setMagic(List<Dish> magic) {
	this.magic = magic;
    }

    public User getSelectedUser() {
	return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
	this.selectedUser = selectedUser;
    }

    public List<Restaurant> getRestaurants() {
	return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
	this.restaurants = restaurants;
    }

    public Restaurant getSelectedRes() {
	return selectedRes;
    }

    public void setSelectedRes(Restaurant selectedRes) {
	this.selectedRes = selectedRes;
    }

    public String getDayTime() {
	return dayTime;
    }

    public void setDayTime(String dayTime) {
	this.dayTime = dayTime;
    }

}
