package co.incafe.web.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo.Lymych
 * 
 */
@RequestScoped
@Named
public class WebUtils {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final int FOOTER_YEAR = Calendar.getInstance().get(Calendar.YEAR);

	private static final String CONTEXT = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();

	public static void addMessage(FacesMessage.Severity severity, String message, String detail) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, message, detail));
	}

	public static void addMessage(FacesMessage.Severity severity, String message) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(severity, message, null));
	}

	public static void redirect() {
		try {
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletRequest request = (HttpServletRequest) context.getRequest();
			context.redirect(request.getRequestURI());
		} catch (Exception e) {
			logger.info(WebUtils.class.getName(), e);
		}
	}

	public static String getMsg(String key) {
		FacesContext ctx = FacesContext.getCurrentInstance();
		ResourceBundle bundle = FacesContext.getCurrentInstance().getApplication().getResourceBundle(ctx, "msgs");
		return bundle.getString(key);
	}

	public static void goToPage(String pagePath) {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect(CONTEXT + pagePath);
		} catch (IOException e) {
			logger.info(WebUtils.class.getName(), e);
		}
	}

	public static String getMD5(String input) {
		String result = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			result = number.toString(16);
			while (result.length() < 32) {
				result = "0" + result;
			}
		} catch (NoSuchAlgorithmException e) {
			logger.info(WebUtils.class.getName(), e);
		}
		return result;
	}

	public static void putSessionVariable(String valueName, Object value) {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		Map<String, Object> sessionMap = externalContext.getSessionMap();
		sessionMap.put(valueName, value);
	}

	@SafeVarargs
	public static void clearSelected(Map<Long, Boolean>... selected) {
		for (Map<Long, Boolean> maps : selected) {
			for (Map.Entry<Long, Boolean> map : maps.entrySet()) {
				map.setValue(false);
			}
		}
	}

	public static String toUtr(String toUrl) {
		try {
			return URLEncoder.encode(toUrl, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.info(WebUtils.class.getName(), e);
		}
		return "";
	}

	public int getFooterYear() {
		return FOOTER_YEAR;
	}

	public static <T> List<T> getSelectedListById(Map<Long, Boolean> selected, List<T> list) {
		List<T> result = new ArrayList<>();
		try {
			for (T t : list) {
				Field field;
				field = t.getClass().getDeclaredField("id");
				field.setAccessible(true);
				long id = (long) field.get(t);
				if (selected.get(id)) {
					result.add(t);
				}
			}
		} catch (Exception e) {
			logger.error("getSelectedListById: ", e);
		}
		return result;
	}

}
