package co.incafe.web.locale;

import static co.incafe.web.util.WebUtils.redirect;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo.Lymych
 * 
 */
@ManagedBean
@SessionScoped
public class InCafeLocaleBean implements Serializable {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	private Locale locale;

	private String language;

	@PostConstruct
	public void init() {
		String userLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale().toString();
		locale = new Locale(userLocale);
		language = userLocale;
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
		logger.info(locale);
	}

	public void localeListener(AjaxBehaviorEvent event) {
		locale = new Locale(language);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
		redirect();
	}

	public Locale getLocale() {
		return locale;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}