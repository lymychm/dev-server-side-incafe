package co.incafe.web.util;

import javax.faces.application.ViewHandler;
import javax.faces.application.ViewHandlerWrapper;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;

/**
 * @author Mykhailo.Lymych
 * 
 */
public class MyViewHandler extends ViewHandlerWrapper {

	private ViewHandler defaultViewHandler;

	public MyViewHandler() {
	}

	public MyViewHandler(ViewHandler defaultViewHandler) {
		super();
		this.defaultViewHandler = defaultViewHandler;
	}

	@Override
	public ViewHandler getWrapped() {
		return defaultViewHandler;
	}

	@Override
	public UIViewRoot restoreView(FacesContext facesContext, String viewId) {
		UIViewRoot root = null;
		root = defaultViewHandler.restoreView(facesContext, viewId);
		if (root == null) {
			root = createView(facesContext, viewId);
		}
		return root;
	}

}