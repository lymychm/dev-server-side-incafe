package co.incafe.web.util;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo.Lymych
 * 
 */
@SuppressWarnings("serial")
@WebServlet(name = "image", urlPatterns = { "/image/*" })
public class ImageServlet extends HttpServlet {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final String IMAGE_FOLDER = "ImageFolder";

	private static final String IMAGE_ID = "imageId";

	private static final String CONTENT_TYPE = "image/png";

	private static final int BUFFER_SIZE = 8192;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		try (BufferedOutputStream stream = new BufferedOutputStream(response.getOutputStream(), BUFFER_SIZE);) {
			String imageParam = request.getParameter(IMAGE_ID);
			String folderParam = getServletContext().getInitParameter(IMAGE_FOLDER);
			Path path = Paths.get(folderParam + imageParam);
			byte[] imageBytes = Files.readAllBytes(path);
			if (imageBytes != null) {
				stream.write(imageBytes);
			}
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

}