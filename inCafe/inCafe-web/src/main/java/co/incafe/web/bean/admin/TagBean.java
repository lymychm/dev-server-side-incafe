package co.incafe.web.bean.admin;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.TagService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class TagBean extends DefaultAdminEntityBean<Tag> implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private TagService service;

	@Inject
	private ApplicationBean applBean;

	@PostConstruct
	public void init() {
		try {
			defaultInit("/pages/tag/tag.xhtml", service, new Tag());
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public String add() {
		String name = getEntity().getName().trim();
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			service.insert(getEntity());
			applBean.getTags().add(getEntity());
			setEntity(new Tag());
			refillAndOrginizePagination();
			addMsgForShow(name + WebUtils.getMsg("msg.added"));
		} catch (EntityExistException e) {
			String msg = name + WebUtils.getMsg("msg.exist");
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		} catch (InCafeException e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/tag/tag.xhtml?faces-redirect=true";
	}

	@Override
	public void save(Tag entity) {
		entity.setEditable(false);
		String name = entity.getName().trim();
		try {
			entity.setName(name);
			service.update(entity);
			applBean.getTags().set(applBean.getTags().indexOf(entity), entity);
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " save", e);
		}
	}

	@Override
	public void delete(Tag entity) {
		try {
			logger.info("DELETE Tag - " + entity + ", by user - " + FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			service.delete(entity);
			applBean.removeFromList(applBean.getTags(), entity);
			refillAndOrginizePagination();
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " delete", e);
		}
	}

}
