package co.incafe.web.bean.admin.maintag;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.MainTag;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.MainTagService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.bean.admin.DefaultAdminEntityBean;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class MainTagBean extends DefaultAdminEntityBean<MainTag> implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private MainTagService service;

	@Inject
	private ApplicationBean applBean;

	private DishType dishType;

	@PostConstruct
	public void init() {
		try {
			dishType = new DishType();
			defaultInit("/pages/mainTag/mainTag.xhtml", service, new MainTag());
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public String add() {
		String name = getEntity().getName().trim();
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			getEntity().setDishType(dishType);
			service.insert(getEntity());
			applBean.getMainTags().add(getEntity());
			setEntity(new MainTag());
			dishType = new DishType();
			refillAndOrginizePagination();
			addMsgForShow(name + WebUtils.getMsg("msg.added"));
		} catch (EntityExistException e) {
			String msg = name + WebUtils.getMsg("msg.exist");
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		} catch (InCafeException e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/mainTag/mainTag.xhtml?faces-redirect=true";
	}

	@Override
	public void save(MainTag entity) {
		entity.setEditable(false);
		String name = entity.getName().trim();
		try {
			entity.setName(name);
			service.update(entity);
			applBean.getMainTags().set(applBean.getMainTags().indexOf(entity), entity);
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " save", e);
		}
	}

	@Override
	public void delete(MainTag entity) {
		try {
			logger.info("DELETE MainTag - " + entity + ", by user - " + FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			service.delete(entity);
			applBean.removeFromList(applBean.getMainTags(), entity);
			refillAndOrginizePagination();
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " delete", e);
		}
	}

	public DishType getDishType() {
		return dishType;
	}

	public void setDishType(DishType dishType) {
		this.dishType = dishType;
	}

}
