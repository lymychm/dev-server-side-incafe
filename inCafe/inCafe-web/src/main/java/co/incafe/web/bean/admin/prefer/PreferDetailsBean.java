package co.incafe.web.bean.admin.prefer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.model.PreferService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class PreferDetailsBean implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private PreferService service;

	@Inject
	private ApplicationBean applBean;

	private Map<Long, Boolean> selectedTags;

	private Prefer entity;

	private boolean editMode;

	@PostConstruct
	public void init() {
		try {
			selectedTags = new HashMap<>();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	public void edit() {
		editMode = true;
	}

	public String save() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			editMode = false;
			entity.setTags(WebUtils.getSelectedListById(selectedTags, applBean.getTags()));
			service.update(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/prefer/details.xhtml?faces-redirect=true";
	}

	public void cancel() {
		editMode = false;
	}

	public Map<Long, Boolean> getSelectedTags() {
		return selectedTags;
	}

	public void setSelectedTags(Map<Long, Boolean> selectedTags) {
		this.selectedTags = selectedTags;
	}

	public Prefer getEntity() {
		return entity;
	}

	public void setEntity(Prefer entity) {
		this.entity = entity;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

}
