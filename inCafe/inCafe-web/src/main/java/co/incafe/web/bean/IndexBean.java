package co.incafe.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.entity.enums.CallingCode;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.ClientCallService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.VisitService;
import co.incafe.util.date.DateUtil;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.chart.VisitChart;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@RequestScoped
public class IndexBean implements Serializable {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final long serialVersionUID = 1L;

    @EJB
    private VisitService visitService;

    @EJB
    private OrderService orderService;

    @EJB
    private ClientCallService callService;

    @Inject
    private GeneralBean generalBean;

    private String visitResult;

    private String orderResult;

    private String callResult;

    private String callStatisitcResult;

    private int visitsTotal;

    private int uniqueVisitUsers;

    private VisitChart visitChart;

    private double checkSumTotal;

    private double checkSumfirst15Min;

    private int totalOrders;

    @PostConstruct
    private void init() {
	try {
	    RestaurantChain restaurantChain = generalBean.getRestaurantChain();
	    if (restaurantChain != null) {
		List<Restaurant> results = restaurantChain.getRestaurants();
		if (results != null && !results.isEmpty()) {
		    Long resId = results.get(0).getId();
		    List<Visit> visits = visitService.selectFromJan19Statistic(resId);
		    visitChart = new VisitChart();
		    visitChart.prepareData(visits);
		    visitResult = visitChart.getResult();
		    visitsTotal = visitChart.getVisitsTotal();
		    uniqueVisitUsers = visitChart.getUniqueVisitUsers();
		    setOrders(resId);
		    setCalls(resId);
		}
	    }
	} catch (InCafeException e) {
	    logger.error(e.getClass().getName(), e);
	}
    }

    private <T> void setMissingDays(Map<Calendar, List<T>> map) {
	Calendar currDate = Calendar.getInstance();
	Calendar jan19 = Calendar.getInstance();
	jan19.set(Calendar.YEAR, 2016);
	jan19.set(Calendar.MONTH, Calendar.JANUARY);
	jan19.set(Calendar.DAY_OF_MONTH, 19);
	int days = 0;
	while (jan19.before(currDate)) {
	    jan19.add(Calendar.DAY_OF_MONTH, 1);
	    days++;
	}
	for (int i = 0; i < days; i++) {
	    Calendar calendar = Calendar.getInstance();
	    calendar = DateUtil.removeTimeFromDate(calendar);
	    calendar.add(Calendar.DAY_OF_MONTH, -i);
	    if (!map.containsKey(calendar)) {
		map.put(calendar, new ArrayList<T>());
	    }
	}
    }

    private void setOrders(Long resId) throws InCafeException {
	List<Order> orders = orderService.selectFromJan19Statistic(resId);
	totalOrders = orders.size();
	Map<Calendar, List<Order>> dayOrders = new TreeMap<>();
	setOrdersData(orders, dayOrders);
	setMissingDays(dayOrders);
	StringBuffer sb = new StringBuffer();
	prepareStringResult(dayOrders, sb);
	if (sb.length() > 0) {
	    sb.setLength(sb.length() - 1);
	    orderResult = sb.toString();
	} else {
	    orderResult = null;
	}
    }

    private void setOrdersData(List<Order> orders, Map<Calendar, List<Order>> uniqueDayOrders) {
	for (Order order : orders) {
	    checkSumTotal += order.getChecksum();
	    Calendar orderDate = order.getOrderDate();

	    orderDate = DateUtil.removeTimeFromDate(orderDate);
	    if (uniqueDayOrders.containsKey(orderDate)) {
		List<Order> list = uniqueDayOrders.get(orderDate);
		list.add(order);
	    } else {
		List<Order> list = new ArrayList<>();
		list.add(order);
		uniqueDayOrders.put(orderDate, list);
	    }
	}
    }

    private void setCalls(Long resId) throws InCafeException {
	List<ClientCall> calls = callService.selectFromJan19Statistic(resId);
	setCallsStatistic(calls);
	Map<Calendar, List<ClientCall>> dayCalls = new TreeMap<>();
	setCallsData(calls, dayCalls);
	setMissingDays(dayCalls);
	StringBuffer sb = new StringBuffer();
	prepareStringResult(dayCalls, sb);
	if (sb.length() > 0) {
	    sb.setLength(sb.length() - 1);
	    callResult = sb.toString();
	} else {
	    callResult = null;
	}
    }

    private void setCallsData(List<ClientCall> calls, Map<Calendar, List<ClientCall>> dayCalls) {
	for (ClientCall call : calls) {
	    Calendar orderDate = call.getCallTime();
	    orderDate = DateUtil.removeTimeFromDate(orderDate);
	    if (dayCalls.containsKey(orderDate)) {
		List<ClientCall> list = dayCalls.get(orderDate);
		list.add(call);
	    } else {
		List<ClientCall> list = new ArrayList<>();
		list.add(call);
		dayCalls.put(orderDate, list);
	    }
	}
    }

    private <T> void prepareStringResult(Map<Calendar, List<T>> entries, StringBuffer sb) {
	for (Map.Entry<Calendar, List<T>> map : entries.entrySet()) {
	    int day = map.getKey().get(Calendar.DAY_OF_MONTH);
	    int month = map.getKey().get(Calendar.MONTH) + 1;
	    int year = map.getKey().get(Calendar.YEAR);
	    int value = map.getValue().size();
	    sb.append("[").append("'").append(fixNumber(day)).append("-").append(fixNumber(month)).append("-").append(year).append("'").append(", ").append(value).append("],");
	}
    }

    private void setCallsStatistic(List<ClientCall> calls) {
	Map<CallingCode, List<ClientCall>> result = new HashMap<>();
	for (ClientCall call : calls) {
	    CallingCode key = call.getCallCode();
	    if (result.containsKey(key)) {
		result.get(key).add(call);
	    } else {
		List<ClientCall> list = new ArrayList<>();
		list.add(call);
		result.put(key, list);
	    }
	}
	prepareCallStatisticResult(result, calls.size());
    }

    private void prepareCallStatisticResult(Map<CallingCode, List<ClientCall>> entries, int total) {
	StringBuffer sb = new StringBuffer();
	sb.append("[").append("'").append("TOTAL").append("'").append(", ").append(total).append("],");
	for (Map.Entry<CallingCode, List<ClientCall>> map : entries.entrySet()) {
	    int value = map.getValue().size();
	    CallingCode key = map.getKey();
	    sb.append("[").append("'").append(key.name()).append("'").append(", ").append(value).append("],");
	}
	if (sb.length() > 0) {
	    sb.setLength(sb.length() - 1);
	    callStatisitcResult = sb.toString();
	} else {
	    callStatisitcResult = null;
	}
    }

    private String fixNumber(int number) {
	if (number < 10) {
	    return "0" + number;
	}
	return "" + number;
    }

    public void menu(ActionEvent event) {
	try {
	    WebUtils.goToPage("/faces/pages/index.xhtml");
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	}
    }

    public String getVisitResult() {
	return visitResult;
    }

    public void setVisitResult(String visitResult) {
	this.visitResult = visitResult;
    }

    public String getOrderResult() {
	return orderResult;
    }

    public void setOrderResult(String orderResult) {
	this.orderResult = orderResult;
    }

    public String getCallResult() {
	return callResult;
    }

    public void setCallResult(String callResult) {
	this.callResult = callResult;
    }

    public int getTotalOrders() {
	return totalOrders;
    }

    public void setTotalOrders(int totalOrders) {
	this.totalOrders = totalOrders;
    }

    public String getCallStatisitcResult() {
	return callStatisitcResult;
    }

    public void setCallStatisitcResult(String callStatisitcResult) {
	this.callStatisitcResult = callStatisitcResult;
    }

    public double getCheckSumTotal() {
	return checkSumTotal;
    }

    public void setCheckSumTotal(double checkSumTotal) {
	this.checkSumTotal = checkSumTotal;
    }

    public double getCheckSumfirst15Min() {
	return checkSumfirst15Min;
    }

    public void setCheckSumfirst15Min(double checkSumfirst15Min) {
	this.checkSumfirst15Min = checkSumfirst15Min;
    }

    public int getVisitsTotal() {
	return visitsTotal;
    }

    public void setVisitsTotal(int visitsTotal) {
	this.visitsTotal = visitsTotal;
    }

    public int getUniqueVisitUsers() {
	return uniqueVisitUsers;
    }

    public void setUniqueVisitUsers(int uniqueVisitUsers) {
	this.uniqueVisitUsers = uniqueVisitUsers;
    }

    public VisitChart getVisitChart() {
	return visitChart;
    }

    public void setVisitChart(VisitChart visitChart) {
	this.visitChart = visitChart;
    }

}
