package co.incafe.web.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.City;
import co.incafe.ejb.entity.Country;
import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.Locale;
import co.incafe.ejb.entity.MainTag;
import co.incafe.ejb.entity.MealTime;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.entity.WebLocaleKey;
import co.incafe.ejb.entity.WebLocaleValue;
import co.incafe.ejb.model.AllergyService;
import co.incafe.ejb.model.CityService;
import co.incafe.ejb.model.CountryService;
import co.incafe.ejb.model.DishTypeService;
import co.incafe.ejb.model.LocaleService;
import co.incafe.ejb.model.MainTagService;
import co.incafe.ejb.model.MealTimeService;
import co.incafe.ejb.model.TagService;
import co.incafe.ejb.model.WebLocaleKeyService;
import co.incafe.util.general.LoggerUtil;

/**
 * @author LM
 * 
 */
@Named
@ApplicationScoped
public class ApplicationBean {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    public static final Integer DISH_IMG_BIG_SIZE = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("imgBigSize"));

    public static final Integer DISH_IMG_SMALL_SIZE = Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getInitParameter("imgSmallSize"));

    @EJB
    private WebLocaleKeyService localeKeyService;

    @EJB
    private LocaleService localeService;

    @EJB
    private CountryService countryService;

    @EJB
    private CityService cityService;

    @EJB
    private DishTypeService dishTypeService;

    @EJB
    private TagService tagService;

    @EJB
    private MealTimeService mealTimeService;

    @EJB
    private AllergyService allergyService;

    @EJB
    private MainTagService mainTagService;

    private Map<String, Map<String, String>> localeBundles;

    private Map<Long, List<City>> citiesByCountry;

    private List<Tag> tags;

    private List<MealTime> mealTimes;

    private List<DishType> dishTypes;

    private List<Locale> locales;

    private List<Country> countries;

    private List<UserSession> userSessions;

    private List<MainTag> mainTags;

    @PostConstruct
    private void init() {
	try {
	    userSessions = new CopyOnWriteArrayList<>();
	    ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
	    servletContext.setAttribute("userSessions", userSessions);
	    List<WebLocaleKey> values = localeKeyService.selectAll();
	    initLocale(values);

	    locales = localeService.selectAll();

	    countries = countryService.selectAll();
	    setCities(countries);

	    tags = new CopyOnWriteArrayList<>(tagService.selectAll());

	    mealTimes = new CopyOnWriteArrayList<>(mealTimeService.selectAll());

	    dishTypes = new CopyOnWriteArrayList<>(dishTypeService.selectAll());

	    mainTags = new CopyOnWriteArrayList<>(mainTagService.selectAll());
	} catch (Exception e) {
	    logger.info(getClass().getName() + " init", e);
	}
    }

    private void initLocale(List<WebLocaleKey> keys) {
	localeBundles = new HashMap<>();
	for (WebLocaleKey key : keys) {
	    addKeyData(key);
	}
    }

    public void addKeyData(WebLocaleKey key) {
	String keyName = key.getName();
	List<WebLocaleValue> values = key.getWebLocaleValues();
	for (WebLocaleValue value : values) {
	    Locale locale = value.getLocale();
	    Map<String, String> keysAndValues = localeBundles.get(locale.getName());
	    if (keysAndValues == null) {
		keysAndValues = new HashMap<>();
		keysAndValues.put(keyName, value.getName());
		localeBundles.put(locale.getName(), keysAndValues);
	    } else {
		keysAndValues.put(keyName, value.getName());
	    }
	}
    }

    public void setCities(List<Country> countries) {
	citiesByCountry = new ConcurrentHashMap<>();
	for (Country country : countries) {
	    List<City> cities = new CopyOnWriteArrayList<>(country.getCities());
	    for (City city : cities) {
		city.setRestaurants(null);
	    }
	    citiesByCountry.put(country.getId(), cities);
	}
    }

    public <T> void removeFromList(List<T> list, T entity) {
	for (int i = 0; i < list.size(); i++) {
	    if (list.get(i).equals(entity)) {
		list.remove(i);
	    }
	}
    }

    public Map<String, String> getMessagesByLanguage(String language) {
	return localeBundles.get(language);
    }

    public Map<String, Map<String, String>> getLocaleBundles() {
	return localeBundles;
    }

    public List<Locale> getLocales() {
	return locales;
    }

    public void setLocales(List<Locale> locales) {
	this.locales = locales;
    }

    public List<Country> getCountries() {
	return countries;
    }

    public void setCountries(List<Country> countries) {
	this.countries = countries;
    }

    public Map<Long, List<City>> getCitiesByCountry() {
	return citiesByCountry;
    }

    public void setCitiesByCountry(Map<Long, List<City>> citiesByCountry) {
	this.citiesByCountry = citiesByCountry;
    }

    public List<Tag> getTags() {
	return tags;
    }

    public void setTags(List<Tag> tags) {
	this.tags = tags;
    }

    public List<MealTime> getMealTimes() {
	return mealTimes;
    }

    public void setMealTimes(List<MealTime> mealTimes) {
	this.mealTimes = mealTimes;
    }

    public List<DishType> getDishTypes() {
	return dishTypes;
    }

    public void setDishTypes(List<DishType> dishTypes) {
	this.dishTypes = dishTypes;
    }

    public List<UserSession> getUserSessions() {
	return userSessions;
    }

    public void setUserSessions(List<UserSession> userSessions) {
	this.userSessions = userSessions;
    }

    public List<MainTag> getMainTags() {
	return mainTags;
    }

    public void setMainTags(List<MainTag> mainTags) {
	this.mainTags = mainTags;
    }

}
