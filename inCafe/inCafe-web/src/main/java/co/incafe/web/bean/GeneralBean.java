package co.incafe.web.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.model.InCafeUserService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class GeneralBean implements Serializable {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@Inject
	private ApplicationBean applicationBean;

	@EJB
	private InCafeUserService userService;

	private RestaurantChain restaurantChain;

	private UserSession userSession;

	@PostConstruct
	public void init() {
		try {
			restaurantChain = userService.selectByUserName(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			logger.info("restaurantChain - " + restaurantChain);
			HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String seesionId = req.getSession().getId();
			String remoteUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
			userSession = new UserSession(seesionId, remoteUser, req.getRemoteAddr());
			applicationBean.getUserSessions().add(userSession);
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public RestaurantChain getRestaurantChain() {
		return restaurantChain;
	}

	public void setRestaurantChain(RestaurantChain restaurantChain) {
		this.restaurantChain = restaurantChain;
	}

	public UserSession getUserSession() {
		return userSession;
	}

}
