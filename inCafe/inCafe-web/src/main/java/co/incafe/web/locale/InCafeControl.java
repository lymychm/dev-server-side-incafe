package co.incafe.web.locale;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;

import javax.faces.context.FacesContext;

import co.incafe.web.bean.ApplicationBean;

/**
 * @author LM
 *
 */
public class InCafeControl extends Control {
	
	private ApplicationBean bean = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{applicationBean}", ApplicationBean.class);

	@Override
	public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException, InstantiationException, IOException {
		return new InCafeBundle(bean.getMessagesByLanguage(locale.getLanguage()));
	}

}