package co.incafe.web.bean.admin;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Event;
import co.incafe.ejb.model.EventService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class EventDetailsBean implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private EventService service;

	@Inject
	private EventBean eventBean;

	private Map<Long, Boolean> selectedRes;

	private Event entity;

	private boolean editMode;

	private String end;

	private String start;

	@PostConstruct
	public void init() {
		try {
			selectedRes = new HashMap<>();
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	public void edit() {
		editMode = true;
	}

	public String save() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			editMode = false;
			entity.setRestaurants(WebUtils.getSelectedListById(selectedRes, eventBean.getRestaurants()));

			Calendar startDate = Calendar.getInstance();
			startDate.setTime(EventBean.DATE_FORMAT.parse(start));
			if (!entity.getStartDate().equals(startDate)) {
				entity.setStartDate(startDate);
			}

			Calendar endDate = Calendar.getInstance();
			endDate.setTime(EventBean.DATE_FORMAT.parse(end));
			entity.setEndDate(endDate);
			if (!entity.getEndDate().equals(endDate)) {
				entity.setEndDate(endDate);
			}

			service.update(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/event/details.xhtml?faces-redirect=true";
	}

	public void cancel() {
		editMode = false;
	}

	public Map<Long, Boolean> getSelectedRes() {
		return selectedRes;
	}

	public void setSelectedRes(Map<Long, Boolean> selectedRes) {
		this.selectedRes = selectedRes;
	}

	public Event getEntity() {
		return entity;
	}

	public void setEntity(Event entity) {
		this.entity = entity;
	}

	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

}
