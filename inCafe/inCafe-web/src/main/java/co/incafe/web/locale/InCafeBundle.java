package co.incafe.web.locale;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

/**
 * @author LM
 * 
 */
public class InCafeBundle extends ResourceBundle {

	protected static final String BASE_NAME = "incafe.messages";

	public static final Control CONTROL = new InCafeControl();

	private Map<String, String> messages;

	public InCafeBundle() {
		setParent(ResourceBundle.getBundle(BASE_NAME, FacesContext.getCurrentInstance().getViewRoot().getLocale(), CONTROL));
	}

	protected InCafeBundle(Map<String, String> messages) {
		this.messages = messages;
	}

	@Override
	protected Object handleGetObject(String key) {
		return messages != null ? messages.get(key) : parent.getObject(key);
	}

	@Override
	public Enumeration<String> getKeys() {
		return messages != null ? Collections.enumeration(messages.keySet()) : parent.getKeys();
	}

	public Map<String, String> getMessages() {
		return messages;
	}

}
