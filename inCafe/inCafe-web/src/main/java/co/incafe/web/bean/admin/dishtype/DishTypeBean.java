package co.incafe.web.bean.admin.dishtype;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.MainTag;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.DishTypeService;
import co.incafe.util.general.LoggerUtil;
import co.incafe.web.bean.ApplicationBean;
import co.incafe.web.bean.admin.DefaultAdminEntityBean;
import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo.Lymych
 * 
 */
@Named
@SessionScoped
public class DishTypeBean extends DefaultAdminEntityBean<DishType> implements Serializable {

	protected final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final long serialVersionUID = 1L;

	@EJB
	private DishTypeService service;

	@Inject
	private ApplicationBean applBean;

	@Inject
	private DishTypeDetailsBean dishTypeDetailsBean;

	private Map<Long, Boolean> selectedMainTags;

	@PostConstruct
	public void init() {
		try {
			selectedMainTags = new HashMap<>();
			defaultInit("/pages/dishType/dishType.xhtml", service, new DishType());
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
		}
	}

	@Override
	public String add() {
		String name = getEntity().getName().trim();
		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			getEntity().setMainTags(WebUtils.getSelectedListById(selectedMainTags, applBean.getMainTags()));
			service.insert(getEntity());
			applBean.getDishTypes().add(getEntity());
			setEntity(new DishType());
			refillAndOrginizePagination();
			addMsgForShow(name + WebUtils.getMsg("msg.added"));
			WebUtils.clearSelected(selectedMainTags);
		} catch (EntityExistException e) {
			String msg = name + WebUtils.getMsg("msg.exist");
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg);
		} catch (InCafeException e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
		return "/pages/dishType/dishType.xhtml?faces-redirect=true";
	}

	@Override
	public void save(DishType entity) {
		entity.setEditable(false);
		String name = entity.getName().trim();
		try {
			entity.setName(name);
			service.update(entity);
			applBean.getDishTypes().set(applBean.getDishTypes().indexOf(entity), entity);
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " save", e);
		}
	}

	@Override
	public void delete(DishType entity) {
		try {
			logger.info("DELETE DishType - " + entity + ", by user - " + FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			service.delete(entity);
			applBean.removeFromList(applBean.getDishTypes(), entity);
			refillAndOrginizePagination();
		} catch (InCafeException e) {
			logger.error(getClass().getName() + " delete", e);
		}
	}

	public void details(DishType dishType) {
		try {
			dishTypeDetailsBean.setEditMode(false);
			dishTypeDetailsBean.setEntity(dishType);
			dishTypeDetailsBean.setSelectedMainTags(new HashMap<Long, Boolean>());
			for (MainTag tag : dishType.getMainTags()) {
				dishTypeDetailsBean.getSelectedMainTags().put(tag.getId(), true);
			}
			WebUtils.goToPage("/faces/pages/dishType/details.xhtml");
		} catch (Exception e) {
			logger.info(getClass().getName(), e);
			WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()); // TODO
		}
	}

	public Map<Long, Boolean> getSelectedMainTags() {
		return selectedMainTags;
	}

	public void setSelectedMainTags(Map<Long, Boolean> selectedMainTags) {
		this.selectedMainTags = selectedMainTags;
	}

}
