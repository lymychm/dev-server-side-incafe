package co.incafe.web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import co.incafe.web.util.WebUtils;

/**
 * @author Mykhailo Lymych
 * 
 */
@FacesValidator("name")
public class NameValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		FacesMessage msg = new FacesMessage();
		if (value.toString().trim().length() == 0) {
			msg.setSummary(WebUtils.getMsg("msg.name"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}

		if (value.toString().length() > 50) {
			msg.setSummary(":max length 50 symbols");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
