package co.incafe.web.util;

import javax.faces.event.AjaxBehaviorEvent;

/**
 * @author LM
 * 
 */
public abstract class Pagination {

	// private final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	// quantity of page links
	private static final int N_OF_P = 4;

	protected Long count;

	protected Integer page = 1;

	protected Integer quantityOnPage = 5;

	protected int leftLimit = 0;

	protected int rightLimit = 4;

	protected void organizePageLimits(Integer page) {
		if ((page - rightLimit) > 0) {
			rightLimit += N_OF_P;
			leftLimit += N_OF_P;
			rightLimit = checkRightLimit(rightLimit);
		}
		if (page <= leftLimit) {
			rightLimit -= N_OF_P;
			leftLimit -= N_OF_P;
			rightLimit += N_OF_P - (rightLimit - leftLimit);
		}
	}

	public void fixLeftRightLimit() {
		rightLimit = checkRightLimit(rightLimit);
		checLeftRightLimit();
	}

	protected int checkRightLimit(int rightLimit) {
		int limit = rightLimit * quantityOnPage;
		if (limit <= count) {
			if ((count - limit < quantityOnPage) && (count - limit > 0)) {
				rightLimit += 1;
			}
			return rightLimit;
		} else {
			return checkRightLimit(rightLimit - 1);
		}
	}

	protected void checLeftRightLimit() {
		if (rightLimit <= 0) {
			rightLimit = 1;
		}
		if (rightLimit > 0 && rightLimit == leftLimit) {
			leftLimit -= N_OF_P;
		}
		fixRight();
	}

	public void firstPage() {
		page = 1;
		leftLimit = 0;
		this.count = count();
		refillEntities();
		rightLimit = checkRightLimit(N_OF_P);
		if (rightLimit - leftLimit > N_OF_P) {
			rightLimit = N_OF_P;
		}
	}

	public void lastPage() {
		this.count = count();
		page = (int) ((count % quantityOnPage) > 0 ? (count / quantityOnPage) + 1 : (count / quantityOnPage));
		page = page <= 0 ? 1 : page;
		rightLimit = page;
		leftLimit = rightLimit > N_OF_P ? rightLimit - ((rightLimit % N_OF_P) == 0 ? N_OF_P : (rightLimit % N_OF_P)) : 0;
		refillEntities();
		if (size() == 0) {
			firstPage();
		}
	}

	public void skipForward() {
		page = rightLimit + 1;
		this.count = count();
		refillEntities();
		if (size() == 0) {
			firstPage();
		} else {
			organizePageLimits(page);
			fixRight();
		}
	}

	public void skipBack() {
		page = leftLimit;
		rightLimit = leftLimit;
		leftLimit = leftLimit - N_OF_P;
		this.count = count();
		refillEntities();
		if (size() == 0) {
			firstPage();
		}
	}

	public void goToPage(Integer page) {
		this.page = page;
		this.count = count();
		refillEntities();
		if (size() == 0) {
			firstPage();
		} else {
			organizePageLimits(page);
			fixRight();
		}
	}

	private void fixRight() {
		if (rightLimit - leftLimit > N_OF_P) {
			rightLimit = rightLimit - (rightLimit - leftLimit - N_OF_P);
		}
	}

	public void quantityOnPageListener(AjaxBehaviorEvent event) {
		firstPage();
	}

	public abstract void refillEntities();

	public abstract long count();

	public abstract int size();

	public void search(AjaxBehaviorEvent event) {
		firstPage();
	}

	// public void search(ActionEvent event) {
	// try {
	// logger.info("SEARCH");
	// firstPage();
	// WebUtils.redirect();
	// } catch (Exception e) {
	// logger.error(getClass().getName(), e);
	// String msg = "Search error";
	// WebUtils.addMessage(FacesMessage.SEVERITY_ERROR, msg, msg); // TODO
	// }
	// }

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getQuantityOnPage() {
		return quantityOnPage;
	}

	public void setQuantityOnPage(Integer quantityOnPage) {
		this.quantityOnPage = quantityOnPage;
	}

	public int getLeftLimit() {
		return leftLimit;
	}

	public void setLeftLimit(int leftLimit) {
		this.leftLimit = leftLimit;
	}

	public int getRightLimit() {
		return rightLimit;
	}

	public void setRightLimit(int rightLimit) {
		this.rightLimit = rightLimit;
	}

}
