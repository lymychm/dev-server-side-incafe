package co.incafe.filter;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;

public class ClientIpFilter implements Filter {

    private static final long TIME = 10000000L;

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static long count = 0;

    private static Map<String, List<Long>> ips;

    private static Map<String, Calendar> ban;

    public void init(FilterConfig config) throws ServletException {
	if (ips == null) {
	    ips = new ConcurrentHashMap<>();
	}
	if (ban == null) {
	    ban = new ConcurrentHashMap<>();
	}
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws java.io.IOException, ServletException {
	String ipAddress = request.getRemoteAddr();

	HttpServletRequest httpRequest = (HttpServletRequest) request;
	ServletContext context = httpRequest.getSession().getServletContext();

	if (spammer(context, ipAddress)) {
	    if (ban.containsKey(ipAddress)) {
		count++;
	    }
	    if (count % 1000 == 0) {
		logger.info(count);
	    }
	} else {
	    chain.doFilter(request, response);
	}
    }

    private boolean spammer(ServletContext context, String ipAddress) {
	if (ban.containsKey(ipAddress)) {
	    Calendar now = Calendar.getInstance();
	    if (ban.get(ipAddress).after(now)) {
		ban.remove(ipAddress);
		return false;
	    } else {
		return true;
	    }
	}
	List<Long> values = ips.get(ipAddress);
	if (values == null) {
	    values = new CopyOnWriteArrayList<>();
	    values.add(System.nanoTime());
	    ips.put(ipAddress, values);
	} else {
	    int sizeBefore = values.size();
	    long currentTime = System.nanoTime() - TIME;
	    for (Long time : values) {
		if (time >= currentTime) {
		    values.add(System.nanoTime());
		    break;
		}
	    }
	    int sizeAfter = values.size();
	    if (sizeBefore == sizeAfter) {
		values = new CopyOnWriteArrayList<>();
		values.add(System.nanoTime());
		ips.put(ipAddress, values);
	    }
	    if (values.size() == 5) {
		logger.info("SPAMMER");
		ban.put(ipAddress, Calendar.getInstance());
		for (Long l : values) {
		    logger.info("l: " + l);
		}
		ips.remove(ipAddress);
		return true;
	    }
	}
	return false;
    }

    public void destroy() {
    }

    public static void main(String[] args) throws InterruptedException {
	for (int i = 0; i < 5; i++) {
	    logger.info("l: " + System.nanoTime());
	    Thread.sleep(100);
	}
	long l1 = System.nanoTime();
	Thread.sleep(100);
	long l2 = System.nanoTime();
	System.out.println(l2 - l1);

    }

}
