package co.incafe.filter;

import java.io.BufferedReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

public class RedirectFilter implements Filter {

    private static final String REDIRECT_URL = "http://78.47.206.177:8080";
    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws java.io.IOException, ServletException {

	HttpServletRequest req = (HttpServletRequest) request;
	logger.info("URL: " + req.getRequestURI());
	HttpServletResponse res = (HttpServletResponse) response;
	switch (req.getMethod()) {
	case "GET":
	    StringBuffer params = new StringBuffer();
	    params.append('?');
	    Map<String, String[]> parameterMap = req.getParameterMap();
	    for (Map.Entry<String, String[]> map : parameterMap.entrySet()) {
		params.append(map.getKey()).append('=');
		for (String s : map.getValue()) {
		    params.append(s);

		}
		params.append('&');
	    }
	    params.setLength(params.length() - 1);
	    logger.info("params: " + params);
	    res.sendRedirect(REDIRECT_URL + req.getRequestURI() + params.toString());
	    logger.info("STATUS: " + res.getStatus());
	    break;
	case "POST":
	    logger.info("POST: ");
	    StringBuffer json = new StringBuffer();
	    String line = null;
	    try {
		BufferedReader reader = request.getReader();
		while ((line = reader.readLine()) != null) {
		    json.append(line);
		}
		logger.info("json: " + json);
		HttpURLConnection conn = null;
		try {
		    URL url = new URL(REDIRECT_URL + req.getRequestURI());
		    conn = (HttpURLConnection) url.openConnection();
		    conn.setRequestMethod(HttpMethod.POST);
		    conn.setDoOutput(true);
		    conn.setRequestProperty("Content-Type", RestUtil.JSON_MEDIA_TYPE);

		    OutputStream os = conn.getOutputStream();

		    os.write(json.toString().getBytes());
		    os.flush();

		    logger.info("RESPONSE CODE : " + conn.getResponseCode());
		    logger.info("RESPONSE MSG : " + conn.getResponseMessage());
		    res.setStatus(conn.getResponseCode());
		} finally {
		    conn.disconnect();
		}
	    } catch (Exception e) {
		logger.error(e);
	    }
	    break;
	default:
	    break;
	}

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void destroy() {

    }

}
