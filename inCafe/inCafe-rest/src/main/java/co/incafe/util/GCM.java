package co.incafe.util;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.rest.push.Data;

/**
 * @author Mykhailo Lymych
 * 
 * @param <T>
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class GCM<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Data<T> data;

    private List<String> registration_ids;

    private String to;

    private int time_to_live;

    private boolean delay_while_idle;

    public GCM(Data<T> data, List<String> registration_ids, String to, int time_to_live, boolean delay_while_idle) {
	super();
	this.data = data;
	this.registration_ids = registration_ids;
	this.to = to;
	this.time_to_live = time_to_live;
	this.delay_while_idle = delay_while_idle;
    }

    public Data<T> getData() {
	return data;
    }

    public void setData(Data<T> data) {
	this.data = data;
    }

    public List<String> getRegistration_ids() {
	return registration_ids;
    }

    public void setRegistration_ids(List<String> registration_ids) {
	this.registration_ids = registration_ids;
    }

    public String getTo() {
	return to;
    }

    public void setTo(String to) {
	this.to = to;
    }

    public int getTime_to_live() {
	return time_to_live;
    }

    public void setTime_to_live(int time_to_live) {
	this.time_to_live = time_to_live;
    }

    public boolean isDelay_while_idle() {
	return delay_while_idle;
    }

    public void setDelay_while_idle(boolean delay_while_idle) {
	this.delay_while_idle = delay_while_idle;
    }

}
