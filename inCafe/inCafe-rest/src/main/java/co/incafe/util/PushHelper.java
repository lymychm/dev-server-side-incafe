package co.incafe.util;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.HttpMethod;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import co.incafe.ejb.entity.Tablet;
import co.incafe.rest.push.Data;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
public class PushHelper {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final String GCM = "https://gcm-http.googleapis.com/gcm/send";

    private static final String KEY = "AIzaSyBPDtriaXn4u1xYz3BvZJCWeQrou3hepJo";

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final boolean DELAY_WHILE_IDLE = true;

    private static final int TIME_TO_LIVE = 300;

    public static <T> void push(GCM<T> data) throws Exception {
	HttpURLConnection conn = null;
	try {
	    URL url = new URL(GCM);
	    conn = (HttpURLConnection) url.openConnection();
	    conn.setRequestMethod(HttpMethod.POST);
	    conn.setDoOutput(true);
	    conn.setRequestProperty("Content-Type", RestUtil.JSON_MEDIA_TYPE);
	    conn.setRequestProperty("Authorization", "key=" + KEY);

	    OutputStream os = conn.getOutputStream();
	    String input = toJson(data);
	    logger.info("PUSH DATA: " + input);

	    os.write(input.getBytes());
	    os.flush();

	    logger.info("RESPONSE CODE : " + conn.getResponseCode());
	    logger.info("RESPONSE MSG : " + conn.getResponseMessage());

	} catch (IOException e) {
	    logger.error(e);
	    throw new IOException(e);
	} finally {
	    conn.disconnect();
	}
    }

    public static <T> void pushToTablets(List<Tablet> tablets, Data<T> pushData) throws Exception {
	if (tablets.size() > 1) {
	    List<String> pushIds = getPushIds(tablets);
	    GCM<T> gcm = new GCM<>(pushData, pushIds, null, TIME_TO_LIVE, DELAY_WHILE_IDLE);
	    push(gcm);
	} else if (!tablets.isEmpty()) {
	    GCM<T> gcm = new GCM<>(pushData, null, tablets.get(0).getPushId(), TIME_TO_LIVE, DELAY_WHILE_IDLE);
	    push(gcm);
	}
    }

    private static List<String> getPushIds(List<Tablet> tablets) {
	List<String> result = new ArrayList<>();
	for (Tablet tablet : tablets) {
	    if (tablet.getPushId() != null && !tablet.getPushId().trim().equals("")) {
		result.add(tablet.getPushId());
	    }
	}
	return result;
    }

    public static <T> String toJson(GCM<T> gcm) throws Exception {
	return MAPPER.writeValueAsString(gcm);
    }

}
