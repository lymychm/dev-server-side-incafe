package co.incafe.util;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Mykhailo Lymych
 * 
 */
@ApplicationPath("")
public class RestLoadServlet extends Application {

}
