package co.incafe.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * @author Mykhailo Lymych
 * 
 */
public class ImageUtil {

    private static final String IMAGES_FOLDER = "/InCafe/incafe-images/";

    public static String savePhotoOnServer(byte[] userPhoto) throws Exception {
	InputStream in = new ByteArrayInputStream(userPhoto);
	BufferedImage bufferedImage = ImageIO.read(in);
	String fileName = System.nanoTime() + ".jpg";
	return saveImage(bufferedImage, "user/", fileName);
    }

    public static String saveImage(BufferedImage image, String folder, String fileName) throws IOException {
	checkPath(folder);
	String result = folder + fileName;
	ImageIO.write(image, "jpg", new File(IMAGES_FOLDER + result));
	return result;
    }

    private static void checkPath(String path) throws IOException {
	File filePath = new File(IMAGES_FOLDER + path);
	if (!filePath.exists()) {
	    boolean result = filePath.mkdirs();
	    if (result == false) {
		throw new IOException("Can't create folders - " + filePath);
	    }
	}
    }

    public static void deleteFile(String filePath) {
	if (filePath != null && filePath.length() > 0) {
	    File file = new File(IMAGES_FOLDER + filePath);
	    file.delete();
	}
    }

}
