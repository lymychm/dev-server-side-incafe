package co.incafe.util.client;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.entity.enums.ClientCallStatus;
import co.incafe.ejb.entity.enums.OrderStatus;
import co.incafe.ejb.entity.enums.VisitStatus;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.ClientCallService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.UserService;
import co.incafe.ejb.model.VisitService;

/**
 * @author Mykhailo Lymych
 * 
 */
public class ClientHelper {

    public static User getUserByGuid(String guid, HttpServletRequest request, UserService userService) throws InCafeException {
	User result = userService.selectByGuid(guid);
	if (result == null) {
	    result = new User();
	    result.setGuid(guid);
	    result.setIp(request.getRemoteAddr());
	    String os = request.getHeader("OS");
	    System.out.println("OS: " + os);
	    if (os != null) {
		result.setOs(Integer.valueOf(os));
	    }
	    result.setRegistrationDate(Calendar.getInstance().getTime());
	    userService.insert(result);
	}
	return result;
    }

    public static void checkPreviusVisits(List<Visit> visits, VisitService visitService) throws EntityExistException, InCafeException {
	Calendar lastHour = Calendar.getInstance();
	lastHour.add(Calendar.MINUTE, -60);
	for (Visit visit : visits) {
	    Calendar inTime = visit.getInTime();
	    if (inTime.after(lastHour)) {
		visit.setInTime(Calendar.getInstance());
		visitService.update(visit);
	    }
	}
    }

    public static void checkPreviusVisits(List<Visit> visits, Tables table, VisitService visitService) throws EntityExistException, InCafeException {
	Calendar lastHour = Calendar.getInstance();
	lastHour.add(Calendar.MINUTE, -60);
	for (Visit visit : visits) {
	    Calendar inTime = visit.getInTime();
	    if (inTime.after(lastHour)) {
		visit.setInTime(Calendar.getInstance());
		visit.setStatus(VisitStatus.TABLE);
		visit.setTable(table);
		visitService.update(visit);
	    }
	}
    }

    public static void setVisitData(User user, Tables table, Visit visit, VisitStatus status) {
	if (visit.getInTime() == null) {
	    visit.setInTime(Calendar.getInstance());
	}
	visit.setTable(table);
	visit.setUser(user);
	visit.setStatus(status);
	visit.setRestaurant(table.getRestaurant());
    }

    public static void fixPreviusVisits(List<Visit> visits, VisitService visitService) throws Exception {
	Calendar lastHour = Calendar.getInstance();
	lastHour.add(Calendar.MINUTE, -60);
	for (Visit visit : visits) {
	    visit.setOutTime(Calendar.getInstance());
	    visit.setStatus(VisitStatus.LEFT);
	    visitService.update(visit);
	}
    }

    public static void fixPreviusOrders(Long userId, Long resId, OrderService orderService) throws Exception {
	List<Order> orders = orderService.selectTodayOrdersByUserAndRes(userId, resId);
	for (Order order : orders) {
	    if (order.getStatus() != OrderStatus.CHECK) {
		order.setStatus(OrderStatus.CHECK);
		orderService.update(order);
	    }
	}
    }

    public static void fixPreviusCalls(Long userId, Long resId, ClientCallService clientCallService) throws Exception {
	List<ClientCall> calls = clientCallService.selectTodayUserCallsByResId(userId, resId);
	for (ClientCall call : calls) {
	    call.setStatus(ClientCallStatus.CONFIRMED);
	    clientCallService.update(call);
	}
    }

    public static void fixPreviusOrdersTable(Long userId, Long resId, Tables table, OrderService orderService) throws Exception {
	List<Order> orders = orderService.selectTodayOrdersByUserAndRes(userId, resId);
	Calendar now = Calendar.getInstance();
	now.add(Calendar.MINUTE, -60);
	for (Order order : orders) {
	    Calendar orderDate = order.getOrderDate();
	    if (orderDate.after(now) && !order.getTable().equals(table)) {
		order.setTable(table);
		orderService.update(order);
	    }
	}
    }

    public static void checkVisit(User user, Tables table, Long resId, VisitService visitService) throws InCafeException {
	List<Visit> visits = visitService.selectTodayVisitByUserAndRes(user.getId(), resId);
	if (visits.isEmpty()) {
	    Visit visit = new Visit();
	    visit.setInTime(Calendar.getInstance());
	    setVisitData(user, table, visit, VisitStatus.TABLE);
	    visitService.insert(visit);
	} else {
	    for (Visit visit : visits) {
		if (!table.equals(visit.getTable())) {
		    visit.setOutTime(Calendar.getInstance());
		    visit.setStatus(VisitStatus.LEFT);
		    visitService.update(visit);
		    visit = new Visit();
		    setVisitData(user, table, visit, VisitStatus.TABLE);
		    visitService.insert(visit);
		}
	    }
	}
    }

}
