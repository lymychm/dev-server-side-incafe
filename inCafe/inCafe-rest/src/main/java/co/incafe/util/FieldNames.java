package co.incafe.util;

/**
 * @author Mykhailo Lymych
 * 
 */
public class FieldNames {

	public static final String NAME = "name";

	public static final String DISH = "dish";

	public static final String ID = "id";

	public static final String PRODUCT = "product";

}
