package co.incafe.util;

/**
 * @author Mykhailo Lymych
 * 
 */
public class DataHelper {

	public static final String CODE = " \"code\": ";

	public static final String DATA = " \"data\": ";

	public static final String USER_ID = " \"user_id\": ";

	public static final String USER_NAME = " \"user_name\": ";

	public static final String USER_PHOTO = " \"user_photo\": ";

	public static final String FAVORITE_FOODS = " \"favorite_foods\": ";

	public static final String FAVORITE_DESSERTS = " \"favorite_desserts\": ";

	public static final String DONT_LIKE_FOOD = " \"dont_like_foods\": ";

	public static final String ALLERGIES = " \"allergies\": ";

	public static final String DISH_IDS = " \"dish_ids\": ";

	public static final String TABLE_NUMBER = " \"table_number\": ";

	public static final String CALL_TYPE_ID = " \"call_type_id\": ";

	public static final String QUOTE = "\"";

	public static final String COMA = ",";

	public static final String LEFT_BRACKET = "{";

	public static final String LEFT_SQUARE_BRACKET = "[";

	public static final String RIGHT_BRACKET = "}";

	public static final String RIGHT_SQUARE_BRACKET = "]";

	public static final String ORDER_ID = " \"order_id\": ";

	public static final String ORDERS = " \"orders\": ";

	public static final String CLIENT_CALL_ID = " \"client_call_id\" : ";

	public static final String CALLS = " \"calls\" : ";
}
