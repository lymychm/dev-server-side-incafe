package co.incafe.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */

public class RestUtil {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    public static final String JSON_MEDIA_TYPE = MediaType.APPLICATION_JSON + ";charset=utf-8";

    public static final String XML_MEDIA_TYPE = MediaType.APPLICATION_XML + ";charset=utf-8";

    private static final String URL = "http://78.47.206.177:8080/incafe/admin-panel/image?imageId=";

    private static final String MSG = " REQUIRED PARAMETERS: ";

    public static String getImageUrl(String imgUrl) {
	String result = "";
	if (imgUrl == null || imgUrl.trim().equals("")) {
	    return null;
	}
	if (imgUrl.contains("http://") || imgUrl.contains("https://")) {
	    return imgUrl;
	}
	try {
	    result = RestUtil.URL + URLEncoder.encode(imgUrl, StandardCharsets.UTF_8.toString());
	} catch (UnsupportedEncodingException e) {
	    logger.error(RestUtil.class.getName() + " getImageUrl", e);
	}
	return result;
    }

    public static void printSpentTime(long start, long end, Logger logger) {
	long result = end - start;
	logger.info("time - " + ((double) result / 1000) + ", seconds");
    }

    public static void validateRequestPrams(HttpServletResponse response, String paramNames, Object... params) {
	for (Object param : params) {
	    if (param == null || ((param instanceof String) && ((String) param).trim().equals(""))) {
		try {
		    response.sendError(Response.Status.BAD_REQUEST.getStatusCode(), MSG + paramNames);
		    throw new WebApplicationException(Response.Status.BAD_REQUEST);
		} catch (IOException e) {
		    logger.error(e);
		    throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
	    }
	}
    }

    public static boolean checkUser(Long id) {
	Long[] testUsers = { 5L, 6L, 7L, 8L };
	for (Long userId : testUsers) {
	    if (userId.equals(id)) {
		return true;
	    }
	}
	return false;
    }

}
