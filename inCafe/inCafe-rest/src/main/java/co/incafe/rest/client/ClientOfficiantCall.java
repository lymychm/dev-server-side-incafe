package co.incafe.rest.client;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.enums.CallingCode;
import co.incafe.ejb.entity.enums.ClientCallStatus;
import co.incafe.ejb.model.ClientCallService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.TableService;
import co.incafe.ejb.model.TabletService;
import co.incafe.ejb.model.UserService;
import co.incafe.ejb.model.VisitService;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.rest.enums.InCafeResponse;
import co.incafe.rest.enums.PushCode;
import co.incafe.rest.push.Data;
import co.incafe.rest.push.client.data.ClientWaiterCallData;
import co.incafe.util.PushHelper;
import co.incafe.util.RestUtil;
import co.incafe.util.client.ClientHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("client/officiantCall")
@javax.enterprise.context.RequestScoped
public class ClientOfficiantCall {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private UserService userService;

    @EJB
    private TableService tableService;

    @EJB
    private TabletService tabletService;

    @EJB
    private OrderService orderService;

    @EJB
    private ClientCallService clientCallService;

    @EJB
    private VisitService visitService;

    @GET
    @Produces(JSON_MEDIA_TYPE)
    public String officiantCall(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon, @QueryParam("code") int code, @Context HttpServletRequest request,
	    @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid, ibeacon", guid, ibeacon);
	logger.info("CALL: guid=" + guid + ", ibeacon=" + ibeacon + ", request.IP=" + request.getRemoteAddr());
	try {
	    User user = ClientHelper.getUserByGuid(guid, request, userService);
	    Tables table = tableService.selectByIbaconId(ibeacon);
	    Long resId = table.getRestaurant().getId();

	    // TEST USERS
	    if (!resId.equals(2L) && RestUtil.checkUser(user.getId())) {
		return InCafeResponse.OK.getMsg();
	    }
	    // // TEST USERS END

	    List<Tablet> tablets = tabletService.selectByResIdWithPushId(resId);

	    ClientCall call = new ClientCall();
	    call.setCallCode(CallingCode.getWcCode(code));
	    call.setCallTime(Calendar.getInstance());
	    call.setUser(user);
	    call.setRestaurant(table.getRestaurant());
	    call.setTable(table);
	    call.setStatus(ClientCallStatus.CALL);

	    ClientHelper.checkVisit(user, table, resId, visitService);

	    clientCallService.insert(call);

	    BuilderUtil.setIsUserRegularOrNew(user, resId, orderService);
	    Data<ClientWaiterCallData> pushData = new Data<>(new ClientWaiterCallData(call), PushCode.OFFICIANT_CALL);
	    PushHelper.pushToTablets(tablets, pushData);
	} catch (Exception e) {
	    logger.error(getClass().getName() + " officiantCall: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
	return InCafeResponse.OK.getMsg();
    }
}
