package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.Event;
import co.incafe.ejb.entity.EventStatistic;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.RestaurantType;
import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.entity.TabletServiceCall;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.UserDishRating;
import co.incafe.ejb.entity.UserMainTagRating;
import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.entity.enums.ClientCallStatus;
import co.incafe.ejb.entity.enums.OrderStatus;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.ClientCallService;
import co.incafe.ejb.model.DishService;
import co.incafe.ejb.model.EventService;
import co.incafe.ejb.model.EventStatisticService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.RestaurantService;
import co.incafe.ejb.model.TabletCallService;
import co.incafe.ejb.model.TabletService;
import co.incafe.ejb.model.UserService;
import co.incafe.ejb.model.VisitService;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.rest.enums.InCafeResponse;
import co.incafe.rest.enums.PushCode;
import co.incafe.rest.post.param.ConfirmCall;
import co.incafe.rest.post.param.ConfirmOrder;
import co.incafe.rest.push.Data;
import co.incafe.rest.push.tablet.data.ClientCallData;
import co.incafe.rest.push.tablet.data.ConfirmClientCallData;
import co.incafe.rest.push.tablet.data.ConfirmOrderData;
import co.incafe.rest.push.tablet.data.InfoForTabletData;
import co.incafe.rest.push.tablet.data.OrderData;
import co.incafe.rest.push.user.UserData;
import co.incafe.util.PushHelper;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("tablet")
@javax.enterprise.context.RequestScoped
public class TabletRest {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final String NO_TABLE = "NO_TABLE";

    private static final double DISH_USER_RATING = 0.1D;

    @EJB
    private TabletService tabletService;

    @EJB
    private DishService dishService;

    @EJB
    private RestaurantService resService;

    @EJB
    private OrderService orderService;

    @EJB
    private ClientCallService clientCallService;

    @EJB
    private VisitService visitService;

    @EJB
    private EventStatisticService eventStatisticService;

    @EJB
    private EventService eventService;

    @EJB
    private UserService userService;

    @EJB
    private TabletCallService tabletCallService;

    @GET
    @Path("registration")
    @Produces(JSON_MEDIA_TYPE)
    public Restaurant registration(@QueryParam("guid") String guid, @QueryParam("pushId") String pushId, @Context HttpServletRequest request,
	    @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid, pushId", guid, pushId);
	logger.info("registration: guid=" + guid + ", pushId=" + pushId + ", request.IP=" + request.getRemoteAddr());
	try {
	    Tablet tablet = tabletService.selectByPushId(pushId);
	    if (tablet != null) {
		if (guid.equals(tablet.getGuid()) && pushId.equals(tablet.getPushId())) {
		    Restaurant restaurant = tablet.getRestaurant();
		    prepereRestaurant(restaurant);
		    return restaurant;
		}
		tablet.setPushId(null);
		tabletService.update(tablet);
	    }
	    Restaurant result = tabletService.registration(guid, pushId);
	    prepereRestaurant(result);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " registration: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @POST
    @Path("confirmOrder")
    @Produces(JSON_MEDIA_TYPE)
    @Consumes(JSON_MEDIA_TYPE)
    public String confirmOrder(ConfirmOrder orderParam, @Context HttpServletRequest request, @Context HttpServletResponse response) {
	try {
	    logger.info("ConfirmOrder: orderParam=" + orderParam + ", request.IP=" + request.getRemoteAddr());
	    if (orderParam == null || orderParam.getOrderIds() == null || orderParam.getOrderIds().isEmpty()) {
		throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	    }
	    List<Order> orders = orderService.selectByIds(orderParam.getOrderIds());
	    Restaurant restaurant = null;
	    Iterator<Order> iter = orders.iterator();
	    while (iter.hasNext()) {
		Order order = iter.next();
		if (order.getStatus() == OrderStatus.ORDER) {
		    order.setStatus(OrderStatus.CONFIRM);
		    if (restaurant == null) {
			restaurant = order.getRestaurant();
		    }
		    Event freeCoffeeEvent = orderService.freeCoffee(order.getUser().getId(), restaurant.getId());
		    if (freeCoffeeEvent != null) {
			EventStatistic entity = new EventStatistic();
			entity.setDateTime(Calendar.getInstance());
			entity.setEvent(freeCoffeeEvent);
			entity.setRestaurant(restaurant);
			entity.setUser(order.getUser());
			entity.setOrder(order);
			eventStatisticService.insert(entity);
			order.getEventStatistics().add(entity);
		    }
		    double checkSum = updateDishRating(order);
		    setUserDishRating(order);
		    order.setChecksum(checkSum);
		    orderService.update(order);
		} else {
		    iter.remove();
		}
	    }
	    if (restaurant != null) {
		List<Tablet> tablets = tabletService.selectByResId(restaurant.getId());
		Data<ConfirmOrderData> pushData = new Data<>(new ConfirmOrderData(orders), PushCode.CONFIRM_ORDER);
		PushHelper.pushToTablets(tablets, pushData);
		List<UserData> listUsers = getUsers(orders);
		for (UserData user : listUsers) {
		    Data<UserData> pushDataUser = new Data<>(user, PushCode.USER_UPDATE);
		    PushHelper.pushToTablets(tablets, pushDataUser);
		}
	    }
	} catch (Exception e) {
	    logger.error(getClass().getName() + " confirmOrder: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
	return InCafeResponse.OK.getMsg();
    }

    private double updateDishRating(Order order) throws InCafeException, EntityExistException {
	double result = 0;
	Map<Dish, Integer> map = new HashMap<>();
	for (Dish dish : order.getDishs()) {
	    result += dish.getPrice();
	    Integer count = map.get(dish);
	    if (count == null) {
		map.put(dish, 1);
	    } else {
		map.put(dish, count + 1);
	    }
	}
	for (Map.Entry<Dish, Integer> m : map.entrySet()) {
	    Dish d = dishService.selectById(m.getKey().getId());
	    if (m.getValue() > 2) {
		d.setRating(d.getRating() + (DISH_USER_RATING * 3));
	    } else {
		d.setRating(d.getRating() + DISH_USER_RATING);
	    }
	    dishService.update(d);
	}
	return result;
    }

    private void setUserDishRating(Order order) throws Exception {
	User user = order.getUser();
	List<UserMainTagRating> mainTagRatings = user.getUserMainTagRatings();
	List<UserDishRating> userDishRatings = user.getUserDishRatings();

	for (Dish dish : order.getDishs()) {
	    boolean containsDish = false;
	    for (UserDishRating dishRating : userDishRatings) {
		if (dishRating.getDish().equals(dish)) {
		    containsDish = true;
		    Calendar date = Calendar.getInstance();
		    date.add(Calendar.DAY_OF_MONTH, -1);
		    if (dishRating.getLastUpdate().after(date)) {
			dishRating.setRating(dishRating.getRating() + (DISH_USER_RATING * 3));
		    } else {
			date.add(Calendar.DAY_OF_MONTH, -1);
			if (dishRating.getLastUpdate().after(date)) {
			    dishRating.setRating(dishRating.getRating() + (DISH_USER_RATING * 2));
			} else {
			    dishRating.setRating(dishRating.getRating() + DISH_USER_RATING);
			}
		    }
		    dishRating.setLastUpdate(Calendar.getInstance());
		    break;
		}
	    }
	    if (!containsDish) {
		UserDishRating rating = new UserDishRating();
		rating.setDish(dish);
		rating.setUser(user);
		rating.setLastUpdate(Calendar.getInstance());
		rating.setRating(DISH_USER_RATING);
		userDishRatings.add(rating);
	    }
	    if (dish.getMainTag() != null) {
		boolean containsMtr = false;
		for (UserMainTagRating mainTagRating : mainTagRatings) {
		    if (mainTagRating.getMainTag().equals(dish.getMainTag())) {
			containsMtr = true;
			Calendar date = Calendar.getInstance();
			date.add(Calendar.DAY_OF_MONTH, -1);
			if (mainTagRating.getLastUpdate().after(date)) {
			    mainTagRating.setRating(mainTagRating.getRating() + (DISH_USER_RATING * 3));
			} else {
			    date.add(Calendar.DAY_OF_MONTH, -1);
			    if (mainTagRating.getLastUpdate().after(date)) {
				mainTagRating.setRating(mainTagRating.getRating() + (DISH_USER_RATING * 2));
			    } else {
				mainTagRating.setRating(mainTagRating.getRating() + DISH_USER_RATING);
			    }
			}
			mainTagRating.setLastUpdate(Calendar.getInstance());
			break;
		    }
		}
		if (!containsMtr) {
		    UserMainTagRating rating = new UserMainTagRating();
		    rating.setUser(user);
		    rating.setMainTag(dish.getMainTag());
		    rating.setLastUpdate(Calendar.getInstance());
		    rating.setRating(DISH_USER_RATING);
		    mainTagRatings.add(rating);
		}
	    }
	}
	user.setUserDishRatings(userDishRatings);
	user.setUserMainTagRatings(mainTagRatings);
	userService.update(user);
    }

    private List<UserData> getUsers(List<Order> orders) throws InCafeException {
	List<UserData> result = new ArrayList<>();
	for (Order order : orders) {
	    User user = order.getUser();
	    BuilderUtil.setIsUserRegularOrNew(user, order.getRestaurant().getId(), orderService);
	    UserData userData = new UserData(user);
	    if (!result.contains(userData)) {
		result.add(userData);
	    }
	}
	return result;
    }

    @POST
    @Path("confirmCall")
    @Produces(JSON_MEDIA_TYPE)
    @Consumes(JSON_MEDIA_TYPE)
    public String confirmWaiterCall(ConfirmCall callParam, @Context HttpServletRequest request, @Context HttpServletResponse response) {
	try {
	    logger.info("callParam=" + callParam + ", request.IP=" + request.getRemoteAddr());
	    if (callParam == null || callParam.getCallIds() == null || callParam.getCallIds().isEmpty()) {
		throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	    }
	    List<ClientCall> calls = clientCallService.selectByIds(callParam.getCallIds());
	    Restaurant restaurant = null;
	    Iterator<ClientCall> iter = calls.iterator();
	    while (iter.hasNext()) {
		ClientCall call = iter.next();
		if (call.getStatus() == ClientCallStatus.CALL) {
		    call.setStatus(ClientCallStatus.CONFIRMED);
		    if (restaurant == null) {
			restaurant = call.getRestaurant();
		    }
		    call.setConfirmTime(Calendar.getInstance());
		    clientCallService.update(call);
		} else {
		    iter.remove();
		}
	    }
	    if (restaurant != null) {
		List<Tablet> tablets = tabletService.selectByResId(restaurant.getId());
		Data<ConfirmClientCallData> pushData = new Data<>(new ConfirmClientCallData(calls), PushCode.CONFIRM_WAITER_CALL);
		PushHelper.pushToTablets(tablets, pushData);
	    }
	} catch (Exception e) {
	    logger.error(getClass().getName() + " confirmCall: ", e);
	    return InCafeResponse.ERROR.getMsg();
	}
	return InCafeResponse.OK.getMsg();
    }

    @GET
    @Path("getInfo")
    @Produces(JSON_MEDIA_TYPE)
    public List<InfoForTabletData> getInfo(@QueryParam("resId") Long resId, @QueryParam("guid") String guid, @Context HttpServletRequest request,
	    @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "resId", resId);
	logger.info("getInfo: resId=" + resId + ", request.IP=" + request.getRemoteAddr());
	try {
	    writeInvokation(guid);
	    List<ClientCall> calls = clientCallService.selectTodayCallsByResId(resId);
	    List<Order> orders = orderService.selectTodayOrdersByResId(resId);
	    List<Visit> visits = visitService.selectTodayUsersInRestaurant(resId);
	    return new ArrayList<>(groupByTable(calls, orders, visits, resId).values());
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getInfo: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    private void writeInvokation(String tabletGuid) {
	try {
	    if (tabletGuid != null) {
		TabletServiceCall call = tabletCallService.selectByTabletGuid(tabletGuid);
		if (call != null) {
		    call.setLastCall(Calendar.getInstance());
		} else {
		    call = new TabletServiceCall();
		    call.setLastCall(Calendar.getInstance());
		    call.setTablet(tabletService.selectByGuid(tabletGuid));
		}
		tabletCallService.update(call);
	    }
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getInfo: ", e);
	}
    }

    private Map<String, InfoForTabletData> groupByTable(List<ClientCall> calls, List<Order> orders, List<Visit> visits, Long resId) throws InCafeException {
	Map<String, InfoForTabletData> map = new HashMap<>();
	for (ClientCall call : calls) {
	    String tableName = getTableName(call.getTable());
	    addCalls(map, call, tableName, resId);
	}
	for (Order order : orders) {
	    String tableName = getTableName(order.getTable());
	    addOrders(map, order, tableName, resId);
	}
	for (Visit visit : visits) {
	    String tableName = getTableName(visit.getTable());
	    addUsers(map, visit, tableName, resId);
	}
	return map;
    }

    private String getTableName(Tables table) {
	return table != null ? table.getName() : NO_TABLE;
    }

    private void addCalls(Map<String, InfoForTabletData> map, ClientCall call, String tableName, Long resId) throws InCafeException {
	if (map.containsKey(tableName)) {
	    InfoForTabletData info = map.get(tableName);
	    info.getCalls().add(new ClientCallData(call));
	    checkAndAddUser(call.getUser(), info, resId);
	} else {
	    InfoForTabletData info = new InfoForTabletData();
	    info.getCalls().add(new ClientCallData(call));
	    info.setTable(tableName);
	    checkAndAddUser(call.getUser(), info, resId);
	    map.put(tableName, info);
	}
    }

    private void addOrders(Map<String, InfoForTabletData> map, Order order, String tableName, Long resId) throws InCafeException {
	OrderStatus orderStatus = order.getStatus();
	if (map.containsKey(tableName)) {
	    InfoForTabletData info = map.get(tableName);
	    addOrder(order, orderStatus, info, resId);
	    checkAndAddUser(order.getUser(), info, resId);
	} else {
	    InfoForTabletData info = new InfoForTabletData();
	    addOrder(order, orderStatus, info, resId);
	    info.setTable(tableName);
	    checkAndAddUser(order.getUser(), info, resId);
	    map.put(tableName, info);
	}
    }

    private void addOrder(Order order, OrderStatus orderStatus, InfoForTabletData info, Long resId) throws InCafeException {
	// boolean freeCoffee = BuilderUtil.freeCoffee(order.getUser(), resId,
	// orderService);
	if (orderStatus == OrderStatus.ORDER) {
	    info.getOrders().add(new OrderData(order));
	} else {
	    info.getConfirmedOrders().add(new OrderData(order));
	}
    }

    private void addUsers(Map<String, InfoForTabletData> map, Visit visit, String tableName, Long resId) throws InCafeException {
	if (map.containsKey(tableName)) {
	    InfoForTabletData info = map.get(tableName);
	    checkAndAddUser(visit.getUser(), info, resId);
	} else {
	    InfoForTabletData info = new InfoForTabletData();
	    checkAndAddUser(visit.getUser(), info, resId);
	    info.setTable(tableName);
	    map.put(tableName, info);
	}
    }

    private void checkAndAddUser(User user, InfoForTabletData info, Long resId) throws InCafeException {
	BuilderUtil.setIsUserRegularOrNew(user, resId, orderService);
	UserData userData = new UserData(user);
	if (!info.getUsers().contains(userData)) {
	    info.getUsers().add(userData);
	}
    }

    private void prepereRestaurant(Restaurant restaurant) throws UnsupportedEncodingException {
	restaurant.setTables(null);
	restaurant.setCity(null);
	restaurant.setDishes(null);
	restaurant.setOrders(null);
	restaurant.setTablets(null);
	restaurant.setVisits(null);
	restaurant.setComponents(null);
	restaurant.setCalls(null);
	restaurant.setRestaurantChain(null);
	restaurant.setEventStatistics(null);
	restaurant.setEvents(null);
	for (RestaurantType type : restaurant.getRestaurantTypes()) {
	    type.setRestaurants(null);
	}
    }

}