package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Allergy;
import co.incafe.ejb.model.AllergyService;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("allergy")
@javax.enterprise.context.RequestScoped
public class AllergyRest {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private AllergyService allergyService;

    @GET
    @Path("getAll")
    @Produces(JSON_MEDIA_TYPE)
    public List<Allergy> getAll(@Context HttpServletResponse response) {
	try {
	    logger.info("getAll");
	    List<Allergy> result = allergyService.selectAll();
	    prepereResult(result);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getAll: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getById")
    @Produces(JSON_MEDIA_TYPE)
    public Allergy getById(@QueryParam("id") Long id, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "id", id);
	try {
	    Allergy result = allergyService.selectById(id);
	    prepereResult(Arrays.asList(result));
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getById: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    private void prepereResult(List<Allergy> allergies) {
	for (Allergy allergy : allergies) {
	    allergy.setTags(null);
	    allergy.setUsers(null);
	    allergy.setLocalization(null);
	    allergy.setEditable(null);
	}
    }

}