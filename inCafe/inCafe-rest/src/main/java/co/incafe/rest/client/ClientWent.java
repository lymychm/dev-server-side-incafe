package co.incafe.rest.client;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.model.ClientCallService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.TableService;
import co.incafe.ejb.model.TabletService;
import co.incafe.ejb.model.UserService;
import co.incafe.ejb.model.VisitService;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.rest.enums.InCafeResponse;
import co.incafe.rest.enums.PushCode;
import co.incafe.rest.push.Data;
import co.incafe.rest.push.client.data.ClientWentData;
import co.incafe.util.PushHelper;
import co.incafe.util.RestUtil;
import co.incafe.util.client.ClientHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("client/went")
@javax.enterprise.context.RequestScoped
public class ClientWent {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private UserService userService;

    @EJB
    private TableService tableService;

    @EJB
    private TabletService tabletService;

    @EJB
    private OrderService orderService;

    @EJB
    private ClientCallService clientCallService;

    @EJB
    private VisitService visitService;

    @GET
    @Produces(JSON_MEDIA_TYPE)
    public String clientWent(@QueryParam("guid") String guid, @QueryParam("resId") Long resId, @Context HttpServletRequest request, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid, resId", guid, resId);
	logger.info("WENT: guid=" + guid + ", resId=" + resId + ", request.IP=" + request.getRemoteAddr());
	try {
	    User user = userService.selectByGuid(guid);

	    List<Tablet> tablets = tabletService.selectByResIdWithPushId(resId);
	    List<Visit> visit = visitService.selectTodayVisitByUserAndRes(user.getId(), resId);
	    if (visit != null) {
		ClientHelper.fixPreviusVisits(visit, visitService);
		ClientHelper.fixPreviusOrders(user.getId(), resId, orderService);
		ClientHelper.fixPreviusCalls(user.getId(), resId, clientCallService);
	    }

	    // TEST USERS
	    if (!resId.equals(2L) && RestUtil.checkUser(user.getId())) {
		return "";
	    }
	    // // TEST USERS END

	    BuilderUtil.setIsUserRegularOrNew(user, resId, orderService);
	    Data<ClientWentData> pushData = new Data<>(new ClientWentData(user), PushCode.CLIENT_WENT);
	    PushHelper.pushToTablets(tablets, pushData);
	} catch (Exception e) {
	    logger.error(getClass().getName() + ", went: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
	return InCafeResponse.OK.getMsg();
    }
}
