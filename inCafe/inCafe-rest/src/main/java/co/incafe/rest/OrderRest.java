package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.model.OrderService;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("order")
@javax.enterprise.context.RequestScoped
public class OrderRest {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@EJB
	private OrderService orderService;

	@GET
	@Path("getConfirmedOrders")
	@Produces(JSON_MEDIA_TYPE)
	public List<Order> getByRestaurant(@QueryParam("resId") Long resId, @Context HttpServletResponse response) {
		RestUtil.validateRequestPrams(response, "resId", resId);
		try {
			List<Order> result = orderService.selectConfirmedOrders(resId);
			prepereOrderResult(result);
			return result;
		} catch (Exception e) {
			logger.error(getClass().getName() + " getConfirmedOrders: ", e);
			throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
	}

	@GET
	@Path("getOrdersByRestaurant")
	@Produces(JSON_MEDIA_TYPE)
	public List<Order> getOrdersByResstaurant(@QueryParam("resId") Long resId, @Context HttpServletResponse response) {
		RestUtil.validateRequestPrams(response, "resId", resId);
		try {
			List<Order> result = orderService.selectOrdersByResstaurant(resId);
			prepereOrderResult(result);
			return result;
		} catch (Exception e) {
			logger.error(getClass().getName() + " getOrdersByResstaurant: ", e);
			throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
	}

	private void prepereOrderResult(List<Order> result) {
		for (Order order : result) {
			order.setRestaurant(null);
			order.setUser(null);
			Tables table = order.getTable();
			table.setRestaurant(null);
			for (Dish dish : order.getDishs()) {
				dish.setComponents(null);
				dish.setCookTime(null);
				dish.setCreateDate(null);
				dish.setDishType(null);
				dish.setMealTimes(null);
				dish.setOrders(null);
				dish.setPhotoMini(null);
				dish.setPhotoBig(null);
				dish.setPrice(null);
				dish.setRating(null);
				dish.setRestaurants(null);
				dish.setStatus(null);
				dish.setSuggestions(null);
				dish.setTags(null);
				dish.setUpdateDate(null);
			}
		}
	}

}