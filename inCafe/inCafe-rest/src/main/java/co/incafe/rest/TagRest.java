package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.model.TagService;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("tag")
@javax.enterprise.context.RequestScoped
public class TagRest {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@EJB
	private TagService service;

	@GET
	@Path("getAll")
	@Produces(JSON_MEDIA_TYPE)
	public List<Tag> getAll(@Context HttpServletResponse response) {
		try {
			List<Tag> result = service.selectAll();
			prepareResult(result);
			return result;
		} catch (Exception e) {
			logger.error(getClass().getName() + " getAll: ", e);
			return Collections.emptyList();
		}
	}

	@GET
	@Path("getById")
	@Produces(JSON_MEDIA_TYPE)
	public Tag getById(@QueryParam("id") Long id, @Context HttpServletResponse response) {
		RestUtil.validateRequestPrams(response, "id", id);
		try {
			Tag result = service.selectById(id);
			prepareResult(Arrays.asList(result));
			return result;
		} catch (Exception e) {
			logger.error(getClass().getName() + " getById: ", e);
			return new Tag();
		}
	}

	private void prepareResult(List<Tag> result) {
		for (Tag tag : result) {
			tag.setDishs(null);
			tag.setAllergies(null);
			tag.setProducts(null);
			tag.setEditable(null);
		}

	}

}