package co.incafe.rest.post.param;

import java.util.List;

/**
 * @author Mykhailo Lymych
 * 
 */
public class OrderParam {

	private String guid;

	private String ibeacon;

	private List<Long> dishes;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getIbeacon() {
		return ibeacon;
	}

	public void setIbeacon(String ibeacon) {
		this.ibeacon = ibeacon;
	}

	public List<Long> getDishes() {
		return dishes;
	}

	public void setDishes(List<Long> dishes) {
		this.dishes = dishes;
	}

	@Override
	public String toString() {
		return "OrderParam [guid=" + guid + ", ibeacon=" + ibeacon + ", dishes=" + dishes + "]";
	}

}
