package co.incafe.rest.oneC;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;
import static co.incafe.util.RestUtil.XML_MEDIA_TYPE;

import java.util.Arrays;
import java.util.Calendar;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.model.DishService;
import co.incafe.rest.enums.InCafeResponse;
import co.incafe.rest.oneC.jaxb.Dish1CInput;
import co.incafe.rest.oneC.jaxb.Dish1CInput.Dish1C;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("1c")
@javax.enterprise.context.RequestScoped
public class DishRest1C {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private DishService dishService;

    @POST
    @Path("dish/add")
    @Produces(JSON_MEDIA_TYPE)
    @Consumes(XML_MEDIA_TYPE)
    public String addDish(Dish1CInput input, @Context HttpServletRequest request, @Context HttpServletResponse response) {
	try {
	    String token = request.getHeader("token");
	    logger.info("1C add DISH, TOKEN: " + token);
	    for (Dish1C dish1c : input.getDish1C()) {
		if (dish1c.getResId() == 0L || (dish1c.getResDishId() == null || dish1c.getResDishId().trim().equals(""))) {
		    response.sendError(Response.Status.EXPECTATION_FAILED.getStatusCode(), "Restaurant id or dish id in restaurant must be set");
		    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
		if (dish1c.getResId() >= 2L || dish1c.getResId() <= 5L) {
		    response.sendError(Response.Status.EXPECTATION_FAILED.getStatusCode(), "INVALID Restaurant ID");
		    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
		Dish dish = dishService.selectByRestaurantData(dish1c.getResDishId(), dish1c.getResId());
		if (dish == null) {
		    dish = setDataToInsert(dish1c);
		    dishService.insert(dish);
		} else {
		    setDataToUpdate(dish, dish1c);
		    dishService.update(dish);
		}
	    }
	} catch (Exception e) {
	    logger.error(getClass().getName() + "POST add: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
	return InCafeResponse.OK.getMsg();
    }

    private void setDataToUpdate(Dish dish, Dish1C dish1c) {
	dish.setUpdateDate(Calendar.getInstance());
	if (dish1c.getName() != null) {
	    dish.setName(dish1c.getName());
	}
	if (dish1c.getPrice() != null) {
	    dish.setPrice(dish1c.getPrice());
	}
	if (dish1c.getRating() != null) {
	    dish.setRating(dish1c.getRating());
	}
	if (dish1c.getStatus() != null) {
	    dish.setStatus(dish1c.getStatus().intValue());
	}
	if (dish1c.getIsDish() != null) {
	    dish.setIsDish(dish1c.getIsDish().intValue());
	}
	if (dish1c.getType() != null) {
	    dish.setDishType(new DishType(dish1c.getType()));
	    dish.setPositionInMenu(dish.getDishType().getId().intValue());
	}
    }

    private Dish setDataToInsert(Dish1C dish1c) {
	Dish dish = new Dish();
	dish.setCreateDate(Calendar.getInstance());
	dish.setName(dish1c.getName());
	dish.setResDishId(dish1c.getResDishId());
	dish.setPrice(dish1c.getPrice() != null ? dish1c.getPrice() : 0);
	dish.setRating(dish1c.getRating() != null ? dish1c.getRating() : 0);
	dish.setStatus(dish1c.getStatus() != null ? dish1c.getStatus().intValue() : 0);
	dish.setIsDish(dish1c.getIsDish() != null ? dish1c.getIsDish().intValue() : 1);
	if (dish1c.getType() != null) {
	    dish.setDishType(new DishType(dish1c.getType()));
	    dish.setPositionInMenu(dish.getDishType().getId().intValue());
	}
	dish.setRestaurants(Arrays.asList(new Restaurant(dish1c.getResId())));
	return dish;
    }

}