package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.City;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.RestaurantType;
import co.incafe.ejb.model.RestaurantService;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("restaurant")
@javax.enterprise.context.RequestScoped
public class RestaurantRest {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@EJB
	private RestaurantService service;

	@GET
	@Path("getNameAndAddress")
	@Produces(JSON_MEDIA_TYPE)
	public List<NameAndAddress> getAll(@Context HttpServletResponse response) {
		try {
			List<Restaurant> result = service.selectAll();
			return preperegetNameAndAddress(result);
		} catch (Exception e) {
			logger.error(getClass().getName() + " getAll: ", e);
			throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
	}

	private List<NameAndAddress> preperegetNameAndAddress(List<Restaurant> restaurants) {
		List<NameAndAddress> result = new ArrayList<>();
		for (Restaurant restaurant : restaurants) {
			NameAndAddress nad = new NameAndAddress(restaurant.getName(), restaurant.getContact().getAddress());
			result.add(nad);
		}
		return result;
	}

	@GET
	@Path("getById")
	@Produces(JSON_MEDIA_TYPE)
	public Restaurant getById(@QueryParam("id") Long id, @Context HttpServletResponse response) {
		RestUtil.validateRequestPrams(response, "id", id);
		try {
			Restaurant result = service.selectById(id);
			prepereRestaurantResult(Arrays.asList(result));
			return result;
		} catch (Exception e) {
			logger.error(getClass().getName() + " getAll: ", e);
			throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
	}

	@GET
	@Path("getByIbeacon")
	@Produces(JSON_MEDIA_TYPE)
	public Restaurant getByIbeacon(@QueryParam("ibeacon") String ibeacon, @Context HttpServletResponse response) {
		RestUtil.validateRequestPrams(response, "ibeacon", ibeacon);
		try {
			Restaurant result = service.selectByIbeacon(ibeacon);
			prepereRestaurantResult(Arrays.asList(result));
			return result;
		} catch (Exception e) {
			logger.error(getClass().getName() + " getByIbeacon: ", e);
			throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
	}

	@GET
	@Path("getByNameAndCity")
	@Produces(JSON_MEDIA_TYPE)
	public Restaurant getByNameAndCity(@QueryParam("name") String name, @QueryParam("cityId") Long cityId, @Context HttpServletResponse response) {
		RestUtil.validateRequestPrams(response, "name, cityId", name, cityId);
		try {
			Restaurant result = service.selectByNameAndCity(name, cityId);
			prepereRestaurantResult(Arrays.asList(result));
			logger.info(result);
			return result;
		} catch (Exception e) {
			logger.error(getClass().getName() + " getByNameAndCity: ", e);
			throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
	}

	@GET
	@Path("getRestaurantRegionUUIDs")
	@Produces(JSON_MEDIA_TYPE)
	public List<String> getRestaurantRegionUUIDs(@Context HttpServletResponse response) {
		try {
			List<String> result = service.getUids();
			logger.info(result);
			return result;
		} catch (Exception e) {
			logger.error(getClass().getName() + " getRestaurantRegionUUIDs: ", e);
			throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
	}

	private void prepereRestaurantResult(List<Restaurant> result) throws UnsupportedEncodingException {
		for (Restaurant restaurant : result) {
			restaurant.setTables(null);
			City city = restaurant.getCity();
			if (city != null) {
				city.setRestaurants(null);
				city.setCountry(null);
			}
			restaurant.setDishes(null);
			restaurant.setOrders(null);
			restaurant.setTablets(null);
			restaurant.setComponents(null);
			restaurant.setVisits(null);
			restaurant.setEventStatistics(null);
			restaurant.setEvents(null);
			for (RestaurantType type : restaurant.getRestaurantTypes()) {
				type.setRestaurants(null);
			}
			restaurant.setCalls(null);
			restaurant.setRestaurantChain(null);
		}
	}

	class NameAndAddress implements Serializable {

		private static final long serialVersionUID = 1L;

		private String name;

		private String address;

		public NameAndAddress(String name, String address) {
			super();
			this.name = name;
			this.address = address;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

	}

}