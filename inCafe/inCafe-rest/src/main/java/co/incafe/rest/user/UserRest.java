package co.incafe.rest.user;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Allergy;
import co.incafe.ejb.entity.Hate;
import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.entity.Product;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.model.UserService;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("user")
@javax.enterprise.context.RequestScoped
public class UserRest {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private UserService userService;

    @GET
    @Path("getAll")
    @Produces(JSON_MEDIA_TYPE)
    public List<User> getAll() {
	try {
	    List<User> result = userService.selectAll();
	    prepareUser(result);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getAll: ", e);
	    throw new WebApplicationException(e.getMessage(), Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getById")
    @Produces(JSON_MEDIA_TYPE)
    public User getById(@QueryParam("id") Long id, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "id", id);
	try {
	    User result = userService.selectById(id);
	    prepareUser(Arrays.asList(result));
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getById: ", e);
	    throw new WebApplicationException(e.getMessage(), Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getByGuid")
    @Produces(JSON_MEDIA_TYPE)
    public User getByGuid(@QueryParam("guid") String guid, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid", guid);
	try {
	    User result = userService.selectByGuid(guid);
	    if (result != null) {
		prepareUser(Arrays.asList(result));
	    }
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getByGuid: ", e);
	    throw new WebApplicationException(e.getMessage(), Response.Status.EXPECTATION_FAILED);
	}
    }

    private void prepareUser(List<User> users) throws UnsupportedEncodingException {
	for (User user : users) {
	    user.setDislikes(null);
	    user.setUserDishRatings(null);
	    user.setUserMainTagRatings(null);
	    for (Allergy allergy : user.getAllergies()) {
		allergy.setUsers(null);
		allergy.setTags(null);
		allergy.setLocalization(null);
		allergy.setEditable(null);
	    }
	    user.setVisits(null);
	    user.setUserOrders(null);
	    user.setPhoto(RestUtil.getImageUrl(user.getPhoto()));
	    for (Hate hate : user.getHates()) {
		hate.setUsers(null);
		Product prod = hate.getProduct();
		setProductData(prod);
	    }
	    for (Prefer prefer : user.getPrefers()) {
		prefer.setUsers(null);
		Product prod = prefer.getProduct();
		setProductData(prod);
	    }
	    user.setOrders(null);
	}
    }

    private void setProductData(Product prod) {
	if (prod != null) {
	    prod.setHates(null);
	    prod.setPrefers(null);
	    prod.setTags(null);
	    prod.setEditable(null);
	}
    }

}