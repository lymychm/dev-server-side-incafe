package co.incafe.rest.push.user;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.ejb.entity.User;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.util.FieldNames;
import co.incafe.util.RestUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class UserData {

    private Long user_id;

    private String user_name;

    private String user_photo;

    private List<String> favorite_foods;

    private List<String> allergies;

    private List<String> dont_like_foods;

    private List<String> prefer;

    private List<String> hate;

    private String user_description;

    private Boolean regular_user;

    private boolean new_user;

    public UserData(User user) {
	this.user_id = user.getId();
	this.user_name = user.getFirstName();
	this.user_photo = RestUtil.getImageUrl(user.getPhoto());
	// this.favorite_foods =
	// BuilderUtil.getSubClassFieldValue(user.getUserDishRatings(),
	// FieldNames.NAME, String.class, FieldNames.DISH, Dish.class);
	this.allergies = BuilderUtil.getFieldValue(user.getAllergies(), FieldNames.NAME, String.class);
	// this.dont_like_foods =
	// BuilderUtil.getSubClassFieldValue(user.getDislikes(),
	// FieldNames.NAME, String.class, FieldNames.DISH, Dish.class);
	this.prefer = BuilderUtil.getFieldValue(user.getPrefers(), FieldNames.NAME, String.class);
	this.hate = BuilderUtil.getFieldValue(user.getHates(), FieldNames.NAME, String.class);
	this.user_description = user.getDescription();
	this.regular_user = user.isRegularUser();
	this.new_user = user.isNewUser();
    }

    public Long getUser_id() {
	return user_id;
    }

    public void setUser_id(Long user_id) {
	this.user_id = user_id;
    }

    public String getUser_name() {
	return user_name;
    }

    public void setUser_name(String user_name) {
	this.user_name = user_name;
    }

    public String getUser_photo() {
	return user_photo;
    }

    public void setUser_photo(String user_photo) {
	this.user_photo = user_photo;
    }

    public List<String> getFavorite_foods() {
	return favorite_foods;
    }

    public void setFavorite_foods(List<String> favorite_foods) {
	this.favorite_foods = favorite_foods;
    }

    public List<String> getAllergies() {
	return allergies;
    }

    public void setAllergies(List<String> allergies) {
	this.allergies = allergies;
    }

    public List<String> getDont_like_foods() {
	return dont_like_foods;
    }

    public void setDont_like_foods(List<String> dont_like_foods) {
	this.dont_like_foods = dont_like_foods;
    }

    public List<String> getPrefer() {
	return prefer;
    }

    public void setPrefer(List<String> prefer) {
	this.prefer = prefer;
    }

    public List<String> getHate() {
	return hate;
    }

    public void setHate(List<String> hate) {
	this.hate = hate;
    }

    public String getUser_description() {
	return user_description;
    }

    public void setUser_description(String user_description) {
	this.user_description = user_description;
    }

    public Boolean getRegular_user() {
	return regular_user;
    }

    public void setRegular_user(Boolean regular_user) {
	this.regular_user = regular_user;
    }

    public boolean isNew_user() {
	return new_user;
    }

    public void setNew_user(boolean new_user) {
	this.new_user = new_user;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((user_id == null) ? 0 : user_id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	UserData other = (UserData) obj;
	if (user_id == null) {
	    if (other.user_id != null) {
		return false;
	    }
	} else if (!user_id.equals(other.user_id)) {
	    return false;
	}
	return true;
    }

}
