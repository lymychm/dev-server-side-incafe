package co.incafe.rest.post.param;

import co.incafe.ejb.entity.User;

/**
 * @author Mykhailo Lymych
 * 
 */
public class UserParam {

	private String guid;

	private User user;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
