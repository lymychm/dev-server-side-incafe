package co.incafe.rest.user;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.TabletService;
import co.incafe.ejb.model.UserService;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.rest.enums.InCafeResponse;
import co.incafe.rest.enums.PushCode;
import co.incafe.rest.push.Data;
import co.incafe.rest.push.user.UserData;
import co.incafe.util.ImageUtil;
import co.incafe.util.PushHelper;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("user/deletePhoto")
@javax.enterprise.context.RequestScoped
public class DeleteUserPhoto {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private UserService userService;

    @EJB
    private TabletService tabletService;

    @EJB
    private OrderService orderService;

    @POST
    @Produces(JSON_MEDIA_TYPE)
    public String deletePhoto(@QueryParam("guid") String guid, @QueryParam("resId") Long resId, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid", guid);
	try {
	    logger.info("DELETE PHOTO: " + guid + ", resId: " + resId);
	    User result = userService.selectByGuid(guid);
	    logger.info("USER: " + result);
	    if (result != null && result.getPhoto() != null) {
		if (!result.getPhoto().contains("http")) {
		    ImageUtil.deleteFile(result.getPhoto());
		}
		result.setPhoto(null);
		userService.update(result);
		if (resId != null) {
		    List<Tablet> tablets = tabletService.selectByResIdWithPushId(resId);
		    if (!tablets.isEmpty()) {
			BuilderUtil.setIsUserRegularOrNew(result, resId, orderService);
			Data<UserData> pushData = new Data<>(new UserData(result), PushCode.USER_UPDATE);
			PushHelper.pushToTablets(tablets, pushData);
		    }
		}
	    }
	    return InCafeResponse.OK.getMsg();
	} catch (Exception e) {
	    logger.error(getClass().getName() + ", deletePhoto: ", e);
	    throw new WebApplicationException(e.getMessage(), Response.Status.EXPECTATION_FAILED);
	}
    }

}