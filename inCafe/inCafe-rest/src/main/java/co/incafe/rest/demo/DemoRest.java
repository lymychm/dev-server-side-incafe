//package co.incafe.rest.demo;
//
//import static co.incafe.ejb.util.ModelUtils.getClassName;
//import static co.incafe.util.RestUtil.ENCODING_UTF_8;
//
//import java.util.List;
//
//import javax.ejb.EJB;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.GET;
//import javax.ws.rs.POST;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.QueryParam;
//import javax.ws.rs.core.MediaType;
//
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//
//import co.incafe.ejb.entity.Restaurant;
//import co.incafe.ejb.entity.Tables;
//import co.incafe.ejb.entity.Tablet;
//import co.incafe.ejb.model.TableService;
//import co.incafe.ejb.model.TabletService;
//import co.incafe.ejb.model.UserService;
//import co.incafe.util.PushHelper;
//
///**
// * @author Mykhailo Lymych
// * 
// */
//@Path("demo")
//@javax.enterprise.context.RequestScoped
//public class DemoRest {
//
//	private final String RESPONSE_OK = "{\"response\": \"0\"}";
//
//	private final String RESPONSE_ERROR = "{\"response\": \"-1\"}";
//
//	private static final Logger logger = LogManager.getLogger(getClassName());
//
//	@EJB
//	private TabletService tabletService;
//
//	@EJB
//	private TableService tableService;
//
//	@EJB
//	private UserService userService;
//
//	@GET
//	@Path("clientIn")
//	@Produces(MediaType.APPLICATION_JSON + ENCODING_UTF_8)
//	public String clientIn(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon) {
//		try {
//			Tables table = tableService.selectByIbaconId(ibeacon);
//			List<Tablet> tablets = tabletService.selectAll();
//			for (Tablet tablet : tablets) {
//				PushHelper.push(getData(Phone.IN, table.getName()), tablet.getPushId(), false);
//			}
//		} catch (Exception e) {
//			logger.error(e);
//			return RESPONSE_ERROR;
//		}
//		return RESPONSE_OK;
//	}
//
//	@GET
//	@Path("clientOut")
//	@Produces(MediaType.APPLICATION_JSON + ENCODING_UTF_8)
//	public String clientOut(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon) {
//		try {
//			Tables table = tableService.selectByIbaconId(ibeacon);
//			List<Tablet> tablets = tabletService.selectAll();
//			for (Tablet tablet : tablets) {
//				PushHelper.push(getData(Phone.OUT, table.getName()), tablet.getPushId(), false);
//			}
//		} catch (Exception e) {
//			logger.error(e);
//			return RESPONSE_ERROR;
//		}
//		return RESPONSE_OK;
//	}
//
//	@GET
//	@Path("order")
//	@Produces(MediaType.APPLICATION_JSON + ENCODING_UTF_8)
//	public String order(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon, @QueryParam("dish") List<String> dishes) {
//		try {
//			Tables table = tableService.selectByIbaconId(ibeacon);
//			String bodyMessage = " {    \"code\": \"2\", \"dishes\" : " + prepareDish(dishes) + ", \"table\": \"" + table.getName() + "\"  },";
//			List<Tablet> tablets = tabletService.selectAll();
//			for (Tablet tablet : tablets) {
//				PushHelper.push(bodyMessage, tablet.getPushId(), false);
//			}
//		} catch (Exception e) {
//			logger.error(e);
//			return RESPONSE_ERROR;
//		}
//		return RESPONSE_OK;
//	}
//
//	@GET
//	@Path("check")
//	@Produces(MediaType.APPLICATION_JSON + ENCODING_UTF_8)
//	public String check(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon) {
//		try {
//			Tables table = tableService.selectByIbaconId(ibeacon);
//			List<Tablet> tablets = tabletService.selectAll();
//			for (Tablet tablet : tablets) {
//				PushHelper.push(getData(Phone.CHECK, table.getName()), tablet.getPushId(), false);
//			}
//		} catch (Exception e) {
//			logger.error(e);
//			return RESPONSE_ERROR;
//		}
//		return RESPONSE_OK;
//	}
//
//	@GET
//	@Path("callWaiter")
//	@Produces(MediaType.APPLICATION_JSON + ENCODING_UTF_8)
//	public String callWaiter(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon) {
//		try {
//			Tables table = tableService.selectByIbaconId(ibeacon);
//			List<Tablet> tablets = tabletService.selectAll();
//			for (Tablet tablet : tablets) {
//				PushHelper.push(getData(Phone.CALL_WAITER, table.getName()), tablet.getPushId(), false);
//			}
//		} catch (Exception e) {
//			logger.error(e);
//			return RESPONSE_ERROR;
//		}
//		return RESPONSE_OK;
//	}
//
//	@POST
//	@Path("registerTablet")
//	@Produces(MediaType.APPLICATION_JSON + ENCODING_UTF_8)
//	@Consumes(MediaType.APPLICATION_JSON + ENCODING_UTF_8)
//	public String registerTablet(Tablet tablet) {
//		try {
//			tabletService.insert(tablet);
//		} catch (Exception e) {
//			logger.error(e);
//			return RESPONSE_ERROR;
//		}
//		return RESPONSE_OK;
//	}
//
//	@GET
//	@Path("registerTablet")
//	@Produces(MediaType.APPLICATION_JSON + ENCODING_UTF_8)
//	public String registerTablet(@QueryParam("guid") String guid, @QueryParam("pushId") String pushId) {
//		try {
//			Tablet tablet = new Tablet();
//			tablet.setGuid(guid);
//			tablet.setPushId(pushId);
//			Restaurant restaurant = new Restaurant();
//			tablet.setRestaurant(restaurant);
//			tabletService.insert(tablet);
//		} catch (Exception e) {
//			logger.error(e);
//			return RESPONSE_ERROR;
//		}
//		return RESPONSE_OK;
//	}
//
//	private String getData(Phone code, String table) {
//		return " {    \"code\": \"" + code.getCode() + "\",    \"table\": \"" + table + "\"  }";
//	}
//
//	private String prepareDish(List<String> dishes) {
//		if (dishes == null || dishes.isEmpty()) {
//			return "[]";
//		}
//		char quote = '"';
//		StringBuilder sb = new StringBuilder();
//		sb.append('[');
//		for (String dish : dishes) {
//			sb.append(quote).append(dish).append(quote).append(',');
//		}
//		sb.setLength(sb.length() - 1);
//		sb.append(']');
//		return sb.toString();
//	}
//
//}
