package co.incafe.rest.demo;

/**
 * @author Mykhailo Lymych
 * 
 */
public enum Phone {
	IN(0), OUT(1), ORDER(2), CHECK(3), CALL_WAITER(4), ERROR(-1);

	private int code;

	Phone(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public static Phone getPhone(int code) {
		for (Phone phone : Phone.values()) {
			if (phone.getCode() == code) {
				return phone;
			}
		}
		return ERROR;
	}

}