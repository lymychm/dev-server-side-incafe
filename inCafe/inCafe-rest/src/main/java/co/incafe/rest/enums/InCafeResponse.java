package co.incafe.rest.enums;

/**
 * @author Mykhailo Lymych
 * 
 */
public enum InCafeResponse {

	OK("{ \"response\" : \"0\" }"), ERROR("{ \"response\" : \"999\" }");

	private String msg;

	InCafeResponse(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

}
