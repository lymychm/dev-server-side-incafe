package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Component;
import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.MealTime;
import co.incafe.ejb.entity.Suggestion;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.model.ComponentService;
import co.incafe.ejb.model.DishService;
import co.incafe.ejb.model.DishTypeService;
import co.incafe.ejb.model.MagicService;
import co.incafe.ejb.model.RestaurantService;
import co.incafe.ejb.model.TagService;
import co.incafe.ejb.model.UserService;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("dish")
@javax.enterprise.context.RequestScoped
public class DishRest {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    // private static SimpleDateFormat format = new
    // SimpleDateFormat("dd-MM-yyyy");

    @EJB
    private DishService dishService;

    @EJB
    private DishTypeService dishTypeService;

    @EJB
    private RestaurantService restaurantService;

    @EJB
    private UserService userService;

    @EJB
    private TagService tagService;

    @EJB
    private MagicService magic;

    @EJB
    private ComponentService componentService;

    @GET
    @Path("magic-test")
    @Produces(JSON_MEDIA_TYPE)
    public List<Long> magicTest(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon, @QueryParam("ibeacon") Long time, @Context HttpServletRequest request,
	    @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid, ibeacon", guid, ibeacon);
	logger.info("guid=" + guid + ", ibeacon=" + ibeacon + ", time=" + time + ", request.IP=" + request.getRemoteAddr());
	try {
	    long start = System.currentTimeMillis();
	    time = System.currentTimeMillis();
	    List<Long> result = magic.magic(guid, ibeacon, time);
	    long end = System.currentTimeMillis();
	    logger.info("TIME: " + (double) (end - start) / 1000);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " magic: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getAll")
    @Produces(JSON_MEDIA_TYPE)
    public List<Dish> getAll(@QueryParam("resId") Long resId, @QueryParam("date") Long date, @Context HttpServletRequest request, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "resId", resId);
	logger.info("DISH getALL: resId=" + resId + ", date=" + date + ", request.IP=" + request.getRemoteAddr());
	try {
	    if (date != null) {
		logger.info("DATE: " + date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date);
		logger.info("CALENDAR: " + calendar.getTime());
		List<Dish> result = dishService.selectAll(resId, date);
		prepereDishResult(result);
		return result;
	    }
	    long start = System.currentTimeMillis();
	    List<Dish> result = dishService.selectAll(resId);
	    prepereDishResult(result);
	    long end = System.currentTimeMillis();
	    logger.info("getAll TIME: " + (double) (end - start) / 1000);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getAll: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getByType")
    @Produces(JSON_MEDIA_TYPE)
    public List<Dish> getByType(@QueryParam("typeId") List<Long> type, @QueryParam("resId") Long resId, @Context HttpServletResponse response) {
	try {
	    RestUtil.validateRequestPrams(response, "typeId, resId", type, resId);
	    List<Dish> result = dishService.selectByType(type, resId);
	    prepereDishResult(result);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getByType: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getById")
    @Produces(JSON_MEDIA_TYPE)
    public Dish getById(@QueryParam("id") Long id, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "id", id);
	try {
	    long start = System.currentTimeMillis();
	    Dish result = dishService.selectById(id);
	    prepereDishResult(Arrays.asList(result));
	    long end = System.currentTimeMillis();
	    logger.info("getAll TIME: " + (double) (end - start) / 1000);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getById: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("magic")
    @Produces(JSON_MEDIA_TYPE)
    public List<Long> magic(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon, @QueryParam("ibeacon") Long time, @Context HttpServletRequest request,
	    @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid, ibeacon", guid, ibeacon);
	logger.info("guid=" + guid + ", ibeacon=" + ibeacon + ", time=" + time + ", request.IP=" + request.getRemoteAddr());
	try {
	    time = System.currentTimeMillis();
	    return magic.magic(guid, ibeacon, time);
	} catch (Exception e) {
	    logger.error(getClass().getName() + " magic: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    private void prepereDishResult(List<Dish> result) throws UnsupportedEncodingException {
	for (Dish dish : result) {
	    removeData(dish);
	    for (Component component : dish.getComponents()) {
		component.setDishs(null);
		component.setRestaurant(null);
	    }
	    preperaSuggestions(dish.getSuggestions());
	    dish.setPhotoMini(RestUtil.getImageUrl(dish.getPhotoMini()));
	    dish.setPhotoBig(RestUtil.getImageUrl(dish.getPhotoBig()));
	    if (dish.getCreateDate() != null) {
		dish.setCreateDateTimeStamp(dish.getCreateDate().getTimeInMillis());
	    }
	    if (dish.getUpdateDate() != null) {
		dish.setUpdateDateTimeStamp(dish.getUpdateDate().getTimeInMillis());
	    }
	}
    }

    private void preperaSuggestions(List<Suggestion> suggestions) {
	for (Suggestion suggestion : suggestions) {
	    Dish dish = suggestion.getDish();
	    Dish sDish = new Dish();
	    sDish.setId(dish.getId());
	    sDish.setName(dish.getName());
	    suggestion.setDish(sDish);
	}
    }

    private void removeData(Dish dish) {
	for (MealTime mealTime : dish.getMealTimes()) {
	    mealTime.setDishs(null);
	}
	dish.setRestaurants(null);
	DishType dishType = dish.getDishType();
	if (dishType != null) {
	    dish.setDishTypeId(dishType.getId());
	    dishType.setDishes(null);
	    dishType.setMainTags(null);
	}
	for (Tag tag : dish.getTags()) {
	    tag.setDishs(null);
	    tag.setAllergies(null);
	    tag.setProducts(null);
	    tag.setEditable(null);
	}
	dish.setOrders(null);
	dish.setMainTag(null);
	dish.setUserDishRatings(null);
    }
    //
    // @GET
    // @Path("add")
    // @Produces(JSON_MEDIA_TYPE)
    // public String addDish(@QueryParam("name") String name, @QueryParam("price") Double price, @QueryParam("cookTime") String cookTime, @QueryParam("resDishId") String resDishId,
    // @QueryParam("resId") Long resId, @QueryParam("type") Long type, @QueryParam("status") Integer status, @Context HttpServletRequest request,
    // @Context HttpServletResponse response) {
    // RestUtil.validateRequestPrams(response, "name, price, cookTime, resDishId, resId, type, status", name, price, cookTime, resDishId, resId, type, status);
    // try {
    // Dish dish = dishService.selectByResDishId(resDishId);
    // if (dish == null) {
    // dish = new Dish();
    // dish.setCreateDate(Calendar.getInstance());
    // dish.setResDishId(resDishId);
    // dish.setRating(0D);
    // dish.setStatus(status);
    // dish.setIsDish(1);
    // dish.setDishType(new DishType(type));
    // dish.setPositionInMenu(dish.getDishType().getId().intValue());
    // prepareDishToMarge(dish, name, price, cookTime, resId);
    // dishService.insert(dish);
    // } else {
    // dish.setUpdateDate(Calendar.getInstance());
    // prepareDishToMarge(dish, name, price, cookTime, resId);
    // dishService.update(dish);
    // }
    // } catch (Exception e) {
    // logger.error(getClass().getName() + " add: ", e);
    // return InCafeResponse.ERROR.name();
    // }
    // return InCafeResponse.OK.name();
    // }
    //
    // private void prepareDishToMarge(Dish dish, String name, Double price, String cookTime, Long resId) throws InCafeException {
    // dish.setCookTime(cookTime);
    // dish.setName(name);
    // dish.setPrice(price);
    // dish.setStatus(1);
    // Restaurant restaurant = restaurantService.selectById(resId);
    // if (restaurant == null) {
    // throw new RuntimeException("restaurant not found by id - " + resId);
    // }
    // dish.setRestaurants(Arrays.asList(restaurant));
    // }
    //
    // @GET
    // @Path("addXml")
    // @Produces(JSON_MEDIA_TYPE)
    // public String addXml(@QueryParam("price") Double price, @QueryParam("resDishId") String resDishId, @QueryParam("name") String name, @QueryParam("photo") String photo,
    // @QueryParam("rating") Double rating, @QueryParam("tag") List<String> tags, @QueryParam("component") List<String> components, @Context HttpServletRequest request,
    // @Context HttpServletResponse response) {
    // try {
    // Dish dish = new Dish();
    // dish.setPrice(price);
    // dish.setResDishId(resDishId);
    // dish.setName(name);
    // dish.setPhotoMini(photo);
    // dish.setRating(rating);
    // dish.setCreateDate(Calendar.getInstance());
    // Restaurant restaurant = restaurantService.selectById(2);
    // if (restaurant == null) {
    // throw new RuntimeException("restaurant not found by id - " + 2);
    // }
    // dish.setRestaurants(Arrays.asList(restaurant));
    // dish.setStatus(1);
    // List<Tag> tmpTag = new ArrayList<>();
    // for (String tag : tags) {
    // Tag entityTag = tagService.getByName(tag);
    // if (entityTag == null) {
    // entityTag = new Tag();
    // entityTag.setName(tag);
    // tagService.insert(entityTag);
    // }
    // entityTag.setDishs(Arrays.asList(dish));
    // tmpTag.add(entityTag);
    // }
    // dish.setTags(tmpTag);
    //
    // List<Component> tmpComponents = new ArrayList<>();
    // for (String com : components) {
    // Component component = componentService.getByNameAndRestaurant(com, 2L);
    // if (component == null) {
    // component = new Component();
    // component.setName(com);
    // component.setRestaurant(restaurant);
    // componentService.insert(component);
    // }
    // component.setDishs(Arrays.asList(dish));
    // tmpComponents.add(component);
    // }
    // dish.setComponents(tmpComponents);
    // DishType dishType = new DishType();
    // dishType.setId(1L);
    // dishType.setName("1");
    // dishType.setDishes(Arrays.asList(dish));
    // dish.setDishType(dishType);
    // dishService.insert(dish);
    // } catch (Exception e) {
    // logger.error(getClass().getName() + " addXml: ", e);
    // return InCafeResponse.ERROR.name();
    // }
    // return InCafeResponse.OK.name();
    // }

    @GET
    @Path("test")
    @Produces(JSON_MEDIA_TYPE)
    public String test(@Context HttpServletResponse response) {
	throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
    }

}