package co.incafe.rest.push.client.data;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.User;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.rest.push.user.UserData;
import co.incafe.util.FieldNames;

/**
 * @author Mykhailo Lymych
 * 
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class ClientOrderData {

	private UserData user;

	private String table_number;

	private Long order_id;

	private List<Long> dish_ids;

	private boolean free_coffee;

	public ClientOrderData(Order order) {
		User user = order.getUser();
		this.table_number = order.getTable().getName();
		this.order_id = order.getId();
		this.dish_ids = BuilderUtil.getFieldValue(order.getDishs(), FieldNames.ID, Long.class);
		this.user = new UserData(user);
	}

	public ClientOrderData(Order order, boolean freeCoffee) {
		this(order);
		this.free_coffee = freeCoffee;
	}

	public UserData getUser() {
		return user;
	}

	public void setUser(UserData user) {
		this.user = user;
	}

	public String getTable_number() {
		return table_number;
	}

	public void setTable_number(String table_number) {
		this.table_number = table_number;
	}

	public Long getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}

	public List<Long> getDish_ids() {
		return dish_ids;
	}

	public void setDish_ids(List<Long> dish_ids) {
		this.dish_ids = dish_ids;
	}

	public boolean isFree_coffee() {
		return free_coffee;
	}

	public void setFree_coffee(boolean free_coffee) {
		this.free_coffee = free_coffee;
	}

}
