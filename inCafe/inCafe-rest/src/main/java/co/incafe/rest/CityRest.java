package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.City;
import co.incafe.ejb.model.CityService;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("city")
@javax.enterprise.context.RequestScoped
public class CityRest {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private CityService cityService;

    @GET
    @Path("getByNameAndCountryId")
    @Produces(JSON_MEDIA_TYPE)
    public City getByNameAndCountryId(@QueryParam("name") String name, @QueryParam("countryId") Long countryId, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "name, countryId", name, countryId);
	try {
	    City result = cityService.selectByNameAndCountryId(name, countryId);
	    prepereCityResult(result);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getByNameAndCountryId: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getByNameAndCountryName")
    @Produces(JSON_MEDIA_TYPE)
    public City getByNameAndCountryName(@QueryParam("name") String name, @QueryParam("countryName") String countryName, @Context HttpServletResponse response) {
	try {
	    RestUtil.validateRequestPrams(response, "name, countryName", name, countryName);
	    City result = cityService.selectByNameAndCountryName(name, countryName);
	    prepereCityResult(result);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getByNameAndCountryName: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getById")
    @Produces(JSON_MEDIA_TYPE)
    public City getByNameAndCountry(@QueryParam("id") Long id, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "id", id);
	try {
	    City result = cityService.selectById(id);
	    prepereCityResult(result);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getById: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    private void prepereCityResult(City result) {
	result.setRestaurants(null);
	result.getCountry().setCities(null);
    }

}