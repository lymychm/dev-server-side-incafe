package co.incafe.rest.post.param;

import java.util.List;

/**
 * @author Mykhailo Lymych
 * 
 */
public class ConfirmCall {

	private String guid;

	private List<Long> callIds;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public List<Long> getCallIds() {
		return callIds;
	}

	public void setCallIds(List<Long> callIds) {
		this.callIds = callIds;
	}

	@Override
	public String toString() {
		return "ConfirmCall [guid=" + guid + ", callIds=" + callIds + "]";
	}

}
