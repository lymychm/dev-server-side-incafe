package co.incafe.rest.client;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.enums.OrderStatus;
import co.incafe.ejb.model.DishService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.TableService;
import co.incafe.ejb.model.TabletService;
import co.incafe.ejb.model.UserService;
import co.incafe.ejb.model.VisitService;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.rest.enums.InCafeResponse;
import co.incafe.rest.enums.PushCode;
import co.incafe.rest.post.param.OrderParam;
import co.incafe.rest.push.Data;
import co.incafe.rest.push.client.data.ClientOrderData;
import co.incafe.util.PushHelper;
import co.incafe.util.RestUtil;
import co.incafe.util.client.ClientHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("client/order")
@javax.enterprise.context.RequestScoped
public class ClientOrder {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private UserService userService;

    @EJB
    private TableService tableService;

    @EJB
    private TabletService tabletService;

    @EJB
    private OrderService orderService;

    @EJB
    private VisitService visitService;

    @EJB
    private DishService dishService;

    @POST
    @Produces(JSON_MEDIA_TYPE)
    @Consumes(JSON_MEDIA_TYPE)
    public String order(OrderParam orderParam, @Context HttpServletRequest request, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid, ibeacon", orderParam.getGuid(), orderParam.getIbeacon());
	logger.info("ORDER: orderParam=" + orderParam + ", request.IP=" + request.getRemoteAddr());
	try {
	    if (!orderParam.getDishes().isEmpty()) {
		Order order = new Order();
		order.setOrderDate(Calendar.getInstance());

		Tables table = tableService.selectByIbaconId(orderParam.getIbeacon());
		if (table == null) {
		    logger.error("ibeacon" + orderParam.getIbeacon() + " not found in DB");
		    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
		}
		order.setTable(table);

		Restaurant restaurant = table.getRestaurant();
		order.setRestaurant(restaurant);
		Long resId = restaurant.getId();

		List<Dish> dishs = dishService.selectByIds(orderParam.getDishes(), resId);
		if (!dishs.isEmpty()) {
		    order.setDishs(checkDoubleOrderDish(dishs, orderParam.getDishes()));
		    logger.info("order D :" + order.getDishs());
		    User user = ClientHelper.getUserByGuid(orderParam.getGuid(), request, userService);
		    order.setUser(user);
		    order.setStatus(OrderStatus.ORDER);
		    // TEST USERS
		    if (!resId.equals(2L) && RestUtil.checkUser(user.getId())) {
			return "";
		    }
		    // // TEST USERS END

		    orderService.insert(order);
		    userService.update(user);

		    ClientHelper.checkVisit(user, table, resId, visitService);

		    BuilderUtil.setIsUserRegularOrNew(user, resId, orderService);
		    List<Tablet> tablets = tabletService.selectByResIdWithPushId(resId);

		    boolean freeCoffee = BuilderUtil.freeCoffee(user, resId, orderService);
		    Data<ClientOrderData> pushData = new Data<>(new ClientOrderData(order, freeCoffee), PushCode.ORDER);
		    PushHelper.pushToTablets(tablets, pushData);
		} else {
		    logger.info("DISHES WITH ID  :" + orderParam.getDishes() + " and ibeacon: " + orderParam.getIbeacon() + " not FOUND!!!");
		}
	    }
	} catch (Exception e) {
	    logger.error(getClass().getName() + " order: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
	return InCafeResponse.OK.getMsg();
    }

    private List<Dish> checkDoubleOrderDish(List<Dish> dishs, List<Long> dishes) {
	List<Dish> result = new ArrayList<>();
	for (Long dishId : dishes) {
	    Dish dish = new Dish();
	    dish.setId(dishId);
	    result.add(dishs.get(dishs.indexOf(dish)));
	}
	return result;
    }

}
