package co.incafe.rest.post.param;

import java.util.List;

/**
 * @author Mykhailo Lymych
 * 
 */
public class ConfirmOrder {

	private String guid;

	private List<Long> orderIds;

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public List<Long> getOrderIds() {
		return orderIds;
	}

	public void setOrderIds(List<Long> orderIds) {
		this.orderIds = orderIds;
	}

	@Override
	public String toString() {
		return "ConfirmOrder [guid=" + guid + ", orderIds=" + orderIds + "]";
	}

}
