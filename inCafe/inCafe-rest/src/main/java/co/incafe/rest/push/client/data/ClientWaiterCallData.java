package co.incafe.rest.push.client.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.entity.User;
import co.incafe.rest.push.user.UserData;

/**
 * @author Mykhailo Lymych
 * 
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class ClientWaiterCallData {

	private UserData user;

	private String table_number;

	private int call_type_id;

	private Long client_call_id;

	public ClientWaiterCallData(ClientCall clientCall) {
		User user = clientCall.getUser();
		this.table_number = clientCall.getTable().getName();
		this.call_type_id = clientCall.getCallCode().ordinal();
		this.client_call_id = clientCall.getId();
		this.user = new UserData(user);
	}

	public UserData getUser() {
		return user;
	}

	public void setUser(UserData user) {
		this.user = user;
	}

	public String getTable_number() {
		return table_number;
	}

	public void setTable_number(String table_number) {
		this.table_number = table_number;
	}

	public int getCall_type_id() {
		return call_type_id;
	}

	public void setCall_type_id(int call_type_id) {
		this.call_type_id = call_type_id;
	}

	public Long getClient_call_id() {
		return client_call_id;
	}

	public void setClient_call_id(Long client_call_id) {
		this.client_call_id = client_call_id;
	}

}
