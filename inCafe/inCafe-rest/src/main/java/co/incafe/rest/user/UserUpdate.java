package co.incafe.rest.user;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Allergy;
import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.entity.EventStatistic;
import co.incafe.ejb.entity.Hate;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.UserDishRating;
import co.incafe.ejb.entity.UserMainTagRating;
import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.AllergyService;
import co.incafe.ejb.model.ClientCallService;
import co.incafe.ejb.model.EventStatisticService;
import co.incafe.ejb.model.HateService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.PreferService;
import co.incafe.ejb.model.TabletService;
import co.incafe.ejb.model.UserDishRatingService;
import co.incafe.ejb.model.UserMainTagRatingService;
import co.incafe.ejb.model.UserService;
import co.incafe.ejb.model.VisitService;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.rest.enums.InCafeResponse;
import co.incafe.rest.enums.PushCode;
import co.incafe.rest.push.Data;
import co.incafe.rest.push.user.UserData;
import co.incafe.util.ImageUtil;
import co.incafe.util.PushHelper;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("user/update")
@javax.enterprise.context.RequestScoped
public class UserUpdate {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private UserService userService;

    @EJB
    private TabletService tabletService;

    @EJB
    private AllergyService allergyService;

    @EJB
    private PreferService preferService;

    @EJB
    private HateService hateService;

    @EJB
    private VisitService visitService;

    @EJB
    private OrderService orderService;

    @EJB
    private ClientCallService callService;

    @EJB
    private UserDishRatingService userDishRatingService;

    @EJB
    private UserMainTagRatingService userMainTagRatingService;

    @EJB
    private EventStatisticService eventStatisticService;

    @POST
    @Produces(JSON_MEDIA_TYPE)
    @Consumes(JSON_MEDIA_TYPE)
    public String update(User userParam, @Context HttpServletRequest request, @Context HttpServletResponse response) {
	try {
	    logger.info("UPDATE: User=" + userParam + ", request.IP=" + request.getRemoteAddr());
	    User user = getUserForUpdate(userParam, request);
	    userService.update(user);
	    for (Hate hate : user.getHates()) {
		logger.info("HATE - " + hate);
	    }
	    for (Prefer prefer : user.getPrefers()) {
		logger.info("PREFER - " + prefer);
	    }
	    for (Allergy allergy : user.getAllergies()) {
		logger.info("ALLERGY - " + allergy);
	    }
	    if (user.getResId() != null) {
		logger.info("RES ID : " + user.getResId());
		logger.info("RES ID userParam: " + userParam.getResId());
		pushUpdates(user);
	    } else {
		logger.info("RES ID : " + user.getResId());
		logger.info("RES ID userParam: " + userParam.getResId());
	    }
	    return InCafeResponse.OK.getMsg();
	} catch (Exception e) {
	    logger.error(getClass().getName() + " update: ", e);
	    throw new WebApplicationException(e.getMessage(), Response.Status.EXPECTATION_FAILED);
	}
    }

    private User getUserForUpdate(User userParam, HttpServletRequest request) throws Exception {
	User user = null;
	String paramGuid = userParam.getGuid();
	List<User> users = userService.selectByGuidOrEmail(paramGuid, userParam.getEmail());
	if (users.isEmpty()) {
	    user = createUser(userParam, request);
	} else if (users.size() == 1) {
	    user = users.get(0);
	    if (!paramGuid.equals(user.getGuid())) {
		user.setGuid(paramGuid);
		margeUser(user, userParam);
		setAllergies(user, true);
		setPrefers(user, true);
		setHates(user, true);
		userService.update(user);
		return user;
	    } else {
		margeUser(user, userParam);
	    }
	} else {
	    user = users.get(0);
	    User user1 = users.get(1);
	    String guid = userParam.getGuid();
	    if (user.getEmail() != null) {
		margeUser(user, userParam);
		setAllergies(user, true);
		setPrefers(user, true);
		setHates(user, true);
		margeUsers(user, user1, guid);
		return user;
	    } else {
		margeUser(user1, userParam);
		setAllergies(user1, true);
		setPrefers(user1, true);
		setHates(user1, true);
		margeUsers(user1, user, guid);
		return user1;
	    }
	}
	user.setIp(request.getRemoteAddr());

	byte[] userPhoto = user.getUserPhoto();
	if (userPhoto != null) {
	    user.setPhoto(ImageUtil.savePhotoOnServer(userPhoto));
	}
	setAllergies(user, false);
	setPrefers(user, false);
	setHates(user, false);
	return user;
    }

    private void margeUsers(User user, User user1, String guid) throws Exception {
	List<Visit> visits = user1.getVisits();
	// List<Visit> visits = visitService.selectByUser(user1.getId());
	for (Visit visit : visits) {
	    visit.setUser(user);
	    visitService.update(visit);
	}
	// List<Order> orders = orderService.selectByUser(user1);
	List<Order> orders = user1.getOrders();
	for (Order order : orders) {
	    order.setUser(user);
	    orderService.update(order);
	}
	List<ClientCall> calls = callService.selectByUser(user1.getId());
	for (ClientCall call : calls) {
	    call.setUser(user);
	    callService.update(call);
	}
	List<EventStatistic> events = eventStatisticService.selectByUser(user1.getId());
	for (EventStatistic eventStatistic : events) {
	    eventStatistic.setUser(user);
	    eventStatisticService.update(eventStatistic);
	}
	List<UserDishRating> udrs = user1.getUserDishRatings();
	for (UserDishRating user1DishRating : udrs) {
	    boolean found = false;
	    for (UserDishRating userDishRating : user.getUserDishRatings()) {
		if (userDishRating.getDish().equals(user1DishRating.getDish())) {
		    userDishRating.setRating(userDishRating.getRating() + user1DishRating.getRating());
		    found = true;
		}
	    }
	    if (!found) {
		UserDishRating rating = new UserDishRating();
		rating.setDish(user1DishRating.getDish());
		rating.setLastUpdate(user1DishRating.getLastUpdate());
		rating.setRating(user1DishRating.getRating());
		rating.setUser(user);
		user.getUserDishRatings().add(rating);
	    }
	}
	List<UserMainTagRating> umtrs = user1.getUserMainTagRatings();
	for (UserMainTagRating user1MainTagRating : umtrs) {
	    boolean found = false;
	    for (UserMainTagRating userMainTagRating : user.getUserMainTagRatings()) {
		if (userMainTagRating.getMainTag().equals(user1MainTagRating.getMainTag())) {
		    userMainTagRating.setRating(user1MainTagRating.getRating() + userMainTagRating.getRating());
		    found = true;
		}
	    }
	    if (!found) {
		UserMainTagRating rating = new UserMainTagRating();
		rating.setLastUpdate(user1MainTagRating.getLastUpdate());
		rating.setRating(user1MainTagRating.getRating());
		rating.setUser(user);
		rating.setMainTag(user1MainTagRating.getMainTag());
		user.getUserMainTagRatings().add(rating);
	    }
	}
	userService.delete(user1);
	user.setGuid(guid);
	byte[] userPhoto = user.getUserPhoto();
	if (userPhoto != null) {
	    user.setPhoto(ImageUtil.savePhotoOnServer(userPhoto));
	}
    }

    private User createUser(User userParam, HttpServletRequest request) throws EntityExistException, InCafeException {
	User user = new User();
	user.setGuid(userParam.getGuid());
	user.setIp(request.getRemoteAddr());
	user.setRegistrationDate(Calendar.getInstance().getTime());
	userService.insert(user);
	return user;
    }

    private void setAllergies(User user, boolean isMagre) throws Exception {
	logger.info("AL_IDS - " + user.getAllergyIds());
	if (user.getAllergyIds() != null) {
	    if (!user.getAllergyIds().isEmpty()) {
		List<Allergy> allergies = allergyService.selectByIds(user.getAllergyIds());
		if (allergies.size() != user.getAllergyIds().size()) {
		    throw new IllegalArgumentException("non-existent allergy id: " + user.getAllergyIds());
		}
		setNewAllergies(allergies, user);
	    } else {
		user.setAllergies(new ArrayList<Allergy>());
	    }
	}
	if (isMagre && user.getAllergyIds() == null) {
	    user.setAllergies(new ArrayList<Allergy>());
	}
    }

    private void setNewAllergies(List<Allergy> allergies, User user) {
	user.setAllergies(new ArrayList<Allergy>());
	for (Allergy allergy : allergies) {
	    user.getAllergies().add(allergy);
	}
    }

    private void setPrefers(User user, boolean isMarge) throws Exception {
	logger.info("PREFER_IDS - " + user.getPreferIds());
	if (user.getPreferIds() != null) {
	    if (!user.getPreferIds().isEmpty()) {
		List<Prefer> prefers = preferService.selectByIds(user.getPreferIds());
		if (prefers.size() != user.getPreferIds().size()) {
		    throw new IllegalArgumentException("non-existent prefers id: " + user.getPreferIds());
		}
		setNewPrefers(prefers, user);
	    } else {
		user.setPrefers(new ArrayList<Prefer>());
	    }
	}
	if (isMarge && user.getPreferIds() == null) {
	    user.setPrefers(new ArrayList<Prefer>());
	}
    }

    private void setNewPrefers(List<Prefer> prefers, User user) {
	user.setPrefers(new ArrayList<Prefer>());
	for (Prefer prefer : prefers) {
	    user.getPrefers().add(prefer);
	}
    }

    private void setHates(User user, boolean isMarge) throws Exception {
	logger.info("HATE_ID - " + user.getHateIds());
	if (user.getHateIds() != null) {
	    if (!user.getHateIds().isEmpty()) {
		List<Hate> hates = hateService.selectByIds(user.getHateIds());
		if (hates.size() != user.getHateIds().size()) {
		    throw new IllegalArgumentException("non-existent hates id: " + user.getHateIds());
		}
		setNewHates(hates, user);
	    } else {
		user.setHates(new ArrayList<Hate>());
	    }
	}
	if (isMarge && user.getHateIds() == null) {
	    user.setHates(new ArrayList<Hate>());
	}
    }

    private void setNewHates(List<Hate> hates, User user) {
	user.setHates(new ArrayList<Hate>());
	for (Hate hate : hates) {
	    user.getHates().add(hate);
	}
    }

    private void pushUpdates(User user) throws Exception {
	List<Tablet> tablets = tabletService.selectByResIdWithPushId(user.getResId());
	if (!tablets.isEmpty()) {
	    BuilderUtil.setIsUserRegularOrNew(user, user.getResId(), orderService);
	}
	// TEST USERS
	if (user.getResId().equals(5L) && RestUtil.checkUser(user.getId())) {
	    return;
	}
	// // TEST USERS END
	Data<UserData> pushData = new Data<>(new UserData(user), PushCode.USER_UPDATE);
	PushHelper.pushToTablets(tablets, pushData);
    }

    private void margeUser(User result, User user) throws Exception {
	for (Field field : User.class.getDeclaredFields()) {
	    field.setAccessible(true);
	    Object value = field.get(user);
	    if (value != null && !field.getName().equals("serialVersionUID")) {
		if (field.getName().equals("photo") && !((String) value).startsWith("http")) {
		    continue;
		}
		Field resultField = result.getClass().getDeclaredField(field.getName());
		resultField.setAccessible(true);
		resultField.set(result, value);
	    }
	}
    }

}