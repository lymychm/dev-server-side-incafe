package co.incafe.rest.user;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.ejb.entity.Allergy;
import co.incafe.ejb.entity.Hate;
import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.entity.Product;

/**
 * @author LM
 *
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class AllergyHatePrefer implements Serializable {

    private static final long serialVersionUID = -8134839934773525578L;

    private List<Allergy> allergies;

    private List<Prefer> prefers;

    private List<Hate> hates;

    public AllergyHatePrefer(List<Allergy> allergies, List<Prefer> prefers, List<Hate> hates) {
	super();
	this.allergies = allergies;
	this.prefers = prefers;
	this.hates = hates;
	prepereAllergyResult(this.allergies);
	preperePreferResult(this.prefers);
	prepereHateResult(this.hates);
    }

    private void prepereAllergyResult(List<Allergy> allergies) {
	for (Allergy allergy : allergies) {
	    allergy.setTags(null);
	    allergy.setUsers(null);
	    allergy.setLocalization(null);
	    allergy.setEditable(null);
	}
    }

    private void prepereHateResult(List<Hate> hates) {
	for (Hate hate : hates) {
	    hate.setUsers(null);
	    hate.setTags(null);
	    Product product = new Product();
	    product.setName(hate.getName());
	    hate.setProduct(product);
	    hate.setName(null);
	}
    }

    private void preperePreferResult(List<Prefer> prefers) {
	for (Prefer prefer : prefers) {
	    prefer.setUsers(null);
	    prefer.setTags(null);
	    Product product = new Product();
	    product.setName(prefer.getName());
	    prefer.setProduct(product);
	    prefer.setName(null);
	}
    }

    public List<Allergy> getAllergies() {
	return allergies;
    }

    public void setAllergies(List<Allergy> allergies) {
	this.allergies = allergies;
    }

    public List<Prefer> getPrefers() {
	return prefers;
    }

    public void setPrefers(List<Prefer> prefers) {
	this.prefers = prefers;
    }

    public List<Hate> getHates() {
	return hates;
    }

    public void setHates(List<Hate> hates) {
	this.hates = hates;
    }

}
