package co.incafe.rest.push;

import java.io.Serializable;

import co.incafe.rest.enums.PushCode;

/**
 * @author Mykhailo Lymych
 * 
 * @param <T>
 */
public class Data<T> implements Serializable {

	private static final long serialVersionUID = 7438562795753304641L;

	private int code;

	private T data;

	public Data(T data, PushCode code) {
		this.code = code.getCode();
		this.data = data;
	}

	public T getData() {
		return data;
	}

	public int getCode() {
		return code;
	}

}
