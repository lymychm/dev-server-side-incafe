package co.incafe.rest.enums;

/**
 * @author Mykhailo Lymych
 * 
 */
public enum WaiterCode {

	COME(0), MENU(1), CLEAN(2), CHECK(3), ERROR(-1);

	private Integer code;

	WaiterCode(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public static WaiterCode getWcCode(Integer code) {
		for (WaiterCode wc : WaiterCode.values()) {
			if (wc.getCode().equals(code)) {
				return wc;
			}
		}
		return ERROR;
	}

}
