package co.incafe.rest.builder;

/**
 * @author Mykhailo Lymych
 * 
 */
@Deprecated
public interface PushDataBuilder {

	public String build() throws Exception;

}
