package co.incafe.rest.enums;

/**
 * @author Mykhailo Lymych
 * 
 */
public enum PushCode {

	CLIENT_COME(0), CLIENT_COME_TO_TABLE(1), CLIENT_WENT(2), ORDER(3), OFFICIANT_CALL(4), CONFIRM_ORDER(5), CONFIRM_WAITER_CALL(6), USER_UPDATE(7), INFO(8), ERROR(-1);

	private int code;

	PushCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public static PushCode getClientCode(int code) {
		for (PushCode c : PushCode.values()) {
			if (c.getCode() == code) {
				return c;
			}
		}
		return ERROR;
	}

}
