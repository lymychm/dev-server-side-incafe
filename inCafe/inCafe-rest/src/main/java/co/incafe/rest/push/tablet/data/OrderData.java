package co.incafe.rest.push.tablet.data;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.ejb.entity.Order;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.util.FieldNames;

/**
 * @author Mykhailo Lymych
 * 
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class OrderData {

    private Long user_id;

    private String user_name;

    private String table_number;

    private Long order_id;

    private List<Long> dish_ids;

    private boolean free_coffee;

    public OrderData(Order order) {
	super();
	this.user_id = order.getUser().getId();
	this.user_name = order.getUser().getFirstName();
	this.table_number = order.getTable().getName();
	this.order_id = order.getId();
	this.dish_ids = BuilderUtil.getFieldValue(order.getDishs(), FieldNames.ID, Long.class);
	this.free_coffee = BuilderUtil.freeCoffee(order);
    }

    public Long getUser_id() {
	return user_id;
    }

    public void setUser_id(Long user_id) {
	this.user_id = user_id;
    }

    public String getTable_number() {
	return table_number;
    }

    public void setTable_number(String table_number) {
	this.table_number = table_number;
    }

    public Long getOrder_id() {
	return order_id;
    }

    public void setOrder_id(Long order_id) {
	this.order_id = order_id;
    }

    public List<Long> getDish_ids() {
	return dish_ids;
    }

    public void setDish_ids(List<Long> dish_ids) {
	this.dish_ids = dish_ids;
    }

    public String getUser_name() {
	return user_name;
    }

    public void setUser_name(String user_name) {
	this.user_name = user_name;
    }

    public boolean isFree_coffee() {
	return free_coffee;
    }

    public void setFree_coffee(boolean free_coffee) {
	this.free_coffee = free_coffee;
    }

}
