package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.model.ComponentService;
import co.incafe.ejb.model.DishTypeService;
import co.incafe.util.RestUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("dish-type")
@javax.enterprise.context.RequestScoped
public class DishTypeRest {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private DishTypeService dishTypeService;

    @EJB
    private ComponentService componentService;

    @GET
    @Path("getAll")
    @Produces(JSON_MEDIA_TYPE)
    public List<DishType> getAll(@Context HttpServletRequest request, @Context HttpServletResponse response) {
	try {
	    logger.info("DishType getAll, request.IP=" + request.getRemoteAddr());
	    List<DishType> result = dishTypeService.selectAll();
	    prepereResult(result);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getAll: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    @GET
    @Path("getById")
    @Produces(JSON_MEDIA_TYPE)
    public DishType getById(@QueryParam("id") Long id, @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "id", id);
	try {
	    DishType result = dishTypeService.selectById(id);
	    prepereResult(Arrays.asList(result));
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getById: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

    private void prepereResult(List<DishType> result) throws UnsupportedEncodingException {
	for (DishType type : result) {
	    type.setMainTags(null);
	    type.setDishes(null);
	}
    }

}