package co.incafe.rest.push.tablet.data;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.ejb.entity.ClientCall;

/**
 * @author Mykhailo Lymych
 * 
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class ConfirmClientCallData {

	private List<ClientCallData> calls;

	public ConfirmClientCallData(List<ClientCall> calls) {
		this.calls = setCallsData(calls);
	}

	private List<ClientCallData> setCallsData(List<ClientCall> calls) {
		List<ClientCallData> result = new ArrayList<>();
		for (ClientCall call : calls) {
			ClientCallData callData = new ClientCallData(call);
			result.add(callData);
		}
		return result;
	}

	public List<ClientCallData> getCalls() {
		return calls;
	}

}
