package co.incafe.rest.push.tablet.data;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.ejb.entity.ClientCall;

/**
 * @author Mykhailo Lymych
 * 
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class ClientCallData {

    private Long user_id;

    private String table_number;

    private Long client_call_id;

    private Integer call_type_id;

    public ClientCallData(ClientCall call) {
	super();
	this.user_id = call.getUser().getId();
	this.table_number = call.getTable().getName();
	this.client_call_id = call.getId();
	this.call_type_id = call.getCallCode().ordinal();
    }

    public Long getUser_id() {
	return user_id;
    }

    public void setUser_id(Long user_id) {
	this.user_id = user_id;
    }

    public String getTable_number() {
	return table_number;
    }

    public void setTable_number(String table_number) {
	this.table_number = table_number;
    }

    public Long getClient_call_id() {
	return client_call_id;
    }

    public void setClient_call_id(Long client_call_id) {
	this.client_call_id = client_call_id;
    }

    public Integer getCall_type_id() {
	return call_type_id;
    }

    public void setCall_type_id(Integer call_type_id) {
	this.call_type_id = call_type_id;
    }

}
