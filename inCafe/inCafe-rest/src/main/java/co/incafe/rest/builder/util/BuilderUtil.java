package co.incafe.rest.builder.util;

import static co.incafe.util.DataHelper.CODE;
import static co.incafe.util.DataHelper.COMA;
import static co.incafe.util.DataHelper.DATA;
import static co.incafe.util.DataHelper.LEFT_BRACKET;
import static co.incafe.util.DataHelper.QUOTE;
import static co.incafe.util.DataHelper.RIGHT_BRACKET;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.EventStatistic;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.OrderService;
import co.incafe.rest.enums.PushCode;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
public class BuilderUtil {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    public static <T, T1> List<T1> getFieldValue(List<T> entities, String fieldName, Class<T1> fieldClass) {
	return getSubClassFieldValue(entities, fieldName, fieldClass, null, null);
    }

    @SuppressWarnings("unchecked")
    public static <T, T1, T2> List<T1> getSubClassFieldValue(List<T> entities, String fieldName, Class<T1> fieldType, String subClassFieldName, Class<T2> subClassfieldType) {
	try {
	    List<T1> result = new ArrayList<>();
	    if (entities != null) {
		for (T e : entities) {
		    if (subClassFieldName != null) {
			T2 field = (T2) field(e, subClassFieldName).get(e);
			T1 value = (T1) field(field, fieldName).get(field);
			result.add(value);
		    } else {
			T1 value = (T1) field(e, fieldName).get(e);
			result.add(value);
		    }
		}
	    }
	    return result;
	} catch (Exception e) {
	    logger.error(BuilderUtil.class.getName() + " getSubClassFieldValue: ", e);
	    return null;
	}
    }

    private static <T> Field field(T entity, String fieldName) throws Exception {
	Field field = entity.getClass().getDeclaredField(fieldName);
	field.setAccessible(true);
	return field;
    }

    public static <T> List<Object> getFieldValues(List<T> entities, String fieldName) throws Exception {
	List<Object> result = new ArrayList<>();
	if (entities != null) {
	    for (T entity : entities) {
		Object value = field(entity, fieldName).get(entity);
		result.add(value);
	    }
	}
	return result;
    }

    public static String prepareData(PushCode code, String data) {
	StringBuffer sb = new StringBuffer();
	sb.append(LEFT_BRACKET);

	sb.append(CODE).append(QUOTE).append(code.getCode()).append(QUOTE).append(COMA);

	sb.append(DATA).append(LEFT_BRACKET);

	sb.append(data);

	sb.append(RIGHT_BRACKET);

	sb.append(RIGHT_BRACKET);

	return sb.toString();
    }

    public static <T> String toJsonArray(List<T> objects) {
	if (objects == null || objects.isEmpty()) {
	    return "[]";
	}
	char quote = '"';
	StringBuilder sb = new StringBuilder();
	sb.append('[');
	for (T object : objects) {
	    sb.append(quote).append(object).append(quote).append(',');
	}
	sb.setLength(sb.length() - 1);
	sb.append(']');
	return sb.toString();
    }

    public static void setIsUserRegularOrNew(User user, Long resId, OrderService orderService) throws InCafeException {
	if ("1".equals(user.getGuid())) {
	    user.setNewUser(true);
	}
	if (resId != null && user.getId() != null) {
	    user.setNewUser(orderService.isNewUser(user.getId(), resId));
	    user.setRegularUser(orderService.isRegularUser(user.getId(), resId));
	}

    }

    public static boolean freeCoffee(User user, Long resId, OrderService orderService) throws InCafeException {
	if (user.getGuid().equals("1")) {
	    return true;
	}
	return orderService.freeCoffee(user.getId(), resId) == null ? false : true;
    }

    public static boolean freeCoffee(Order order) {
	logger.info("order_id: " + order.getId() + ", EventStatistic SIZE: " + order.getEventStatistics().size());
	for (EventStatistic es : order.getEventStatistics()) {
	    String eventName = es.getEvent().getName();
	    logger.info("eventName: " + eventName);
	    if (eventName.equals("free_coffee")) {
		return true;
	    }
	}
	return false;
    }
}
