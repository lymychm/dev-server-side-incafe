package co.incafe.rest;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import co.incafe.rest.enums.InCafeResponse;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("test")
@javax.enterprise.context.RequestScoped
public class TestRest {

    @GET
    @Path("test-server")
    @Produces(JSON_MEDIA_TYPE)
    public String restServer(@Context HttpServletResponse response) {
	return InCafeResponse.OK.getMsg();
    }

}
