package co.incafe.rest.client;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import java.util.List;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.entity.enums.VisitStatus;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.TableService;
import co.incafe.ejb.model.TabletService;
import co.incafe.ejb.model.UserService;
import co.incafe.ejb.model.VisitService;
import co.incafe.rest.builder.util.BuilderUtil;
import co.incafe.rest.enums.PushCode;
import co.incafe.rest.push.Data;
import co.incafe.rest.push.client.data.ClientComeToTableData;
import co.incafe.util.PushHelper;
import co.incafe.util.RestUtil;
import co.incafe.util.client.ClientHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("client/comeToTable")
@javax.enterprise.context.RequestScoped
public class ClientComeToTable {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private UserService userService;

    @EJB
    private TableService tableService;

    @EJB
    private TabletService tabletService;

    @EJB
    private OrderService orderService;

    @EJB
    private VisitService visitService;

    @GET
    @Produces(JSON_MEDIA_TYPE)
    public String clientComeToTable(@QueryParam("guid") String guid, @QueryParam("ibeacon") String ibeacon, @Context HttpServletRequest request,
	    @Context HttpServletResponse response) {
	RestUtil.validateRequestPrams(response, "guid, ibeacon", guid, ibeacon);
	logger.info("COME_TO_TABLE: guid=" + guid + ", ibeacon=" + ibeacon + ", request.IP=" + request.getRemoteAddr());
	try {
	    User user = ClientHelper.getUserByGuid(guid, request, userService);
	    Tables table = tableService.selectByIbaconId(ibeacon);
	    Long resId = table.getRestaurant().getId();

	    // TEST USERS
	    if (!resId.equals(2L) && RestUtil.checkUser(user.getId())) {
		return table.getName();
	    }
	    // // TEST USERS END

	    List<Tablet> tablets = tabletService.selectByResIdWithPushId(resId);
	    List<Visit> visits = visitService.selectTodayVisitByUserAndRes(user.getId(), resId);
	    logger.info("visit: " + visits.size());
	    if (visits.isEmpty()) {
		Visit visit = new Visit();
		ClientHelper.setVisitData(user, table, visit, VisitStatus.TABLE);
		visitService.insert(visit);
	    } else {
		ClientHelper.checkPreviusVisits(visits, table, visitService);
		ClientHelper.fixPreviusOrdersTable(user.getId(), resId, table, orderService);
	    }
	    BuilderUtil.setIsUserRegularOrNew(user, resId, orderService);
	    Data<ClientComeToTableData> pushData = new Data<>(new ClientComeToTableData(user, table.getName()), PushCode.CLIENT_COME_TO_TABLE);
	    PushHelper.pushToTablets(tablets, pushData);
	    return table.getName();
	} catch (Exception e) {
	    logger.error(getClass().getName() + " comeToTable: ", e);
	    throw new WebApplicationException(Response.Status.EXPECTATION_FAILED);
	}
    }

}
