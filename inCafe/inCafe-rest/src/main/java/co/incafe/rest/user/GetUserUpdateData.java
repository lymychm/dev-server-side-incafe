package co.incafe.rest.user;

import static co.incafe.util.RestUtil.JSON_MEDIA_TYPE;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.model.AllergyService;
import co.incafe.ejb.model.HateService;
import co.incafe.ejb.model.PreferService;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Path("user/get-user-update-data")
@javax.enterprise.context.RequestScoped
public class GetUserUpdateData {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @EJB
    private AllergyService allergyService;

    @EJB
    private PreferService preferService;

    @EJB
    private HateService hateService;

    @GET
    @Produces(JSON_MEDIA_TYPE)
    public AllergyHatePrefer getUserUpdateData(@Context HttpServletRequest request, @Context HttpServletResponse response) {
	try {
	    logger.info("getUserUpdateData: request.IP=" + request.getRemoteAddr());
	    return new AllergyHatePrefer(allergyService.selectAll(), preferService.selectAll(), hateService.selectAll());
	} catch (Exception e) {
	    logger.error(getClass().getName() + " getUserUpdateData: ", e);
	    throw new WebApplicationException(e.getMessage(), Response.Status.EXPECTATION_FAILED);
	}
    }

}