package co.incafe.rest.push.tablet.data;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.ejb.entity.Order;

/**
 * @author Mykhailo Lymych
 * 
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class ConfirmOrderData {

    private List<OrderData> orders;

    public ConfirmOrderData(List<Order> orders) {
	this.orders = getOrderData(orders);
    }

    private List<OrderData> getOrderData(List<Order> orders) {
	List<OrderData> result = new ArrayList<>();
	for (Order order : orders) {
	    OrderData orderData = new OrderData(order);
	    result.add(orderData);
	}
	return result;
    }

    public List<OrderData> getOrders() {
	return orders;
    }

}
