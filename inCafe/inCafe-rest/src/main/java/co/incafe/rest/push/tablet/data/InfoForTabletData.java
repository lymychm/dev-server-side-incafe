package co.incafe.rest.push.tablet.data;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import co.incafe.rest.push.user.UserData;

/**
 * @author Mykhailo Lymych
 * 
 */
@JsonSerialize(include = Inclusion.NON_NULL)
public class InfoForTabletData {

	private String table;

	private List<UserData> users;

	private List<OrderData> orders;

	public List<OrderData> confirmedOrders;

	private List<ClientCallData> calls;

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public List<UserData> getUsers() {
		if (this.users == null) {
			this.users = new ArrayList<>();
		}
		return users;
	}

	public void setUsers(List<UserData> users) {
		this.users = users;
	}

	public List<OrderData> getOrders() {
		if (this.orders == null) {
			this.orders = new ArrayList<>();
		}
		return orders;
	}

	public void setOrders(List<OrderData> orders) {
		this.orders = orders;
	}

	public List<ClientCallData> getCalls() {
		if (this.calls == null) {
			this.calls = new ArrayList<>();
		}
		return calls;
	}

	public void setCalls(List<ClientCallData> calls) {
		this.calls = calls;
	}

	public List<OrderData> getConfirmedOrders() {
		if (this.confirmedOrders == null) {
			this.confirmedOrders = new ArrayList<>();
		}
		return confirmedOrders;
	}

	public void setConfirmedOrders(List<OrderData> confirmedOrders) {
		this.confirmedOrders = confirmedOrders;
	}

}
