package co.incafe.rest.test;

import java.util.Arrays;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import co.incafe.ejb.entity.Allergy;
import co.incafe.util.general.LoggerUtil;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

public class AllergyRestTest {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	private static final ObjectMapper MAPPER = new ObjectMapper();

	// @Test
	// public void getAllAllergiesTest() throws Exception {
	// Response resp = RestAssured.get("http://localhost:8080/incafe-rest/allergy/getAll");
	// logger.info("RESPONSE: " + resp.asString());
	// Allergy[] allergies = MAPPER.readValue(resp.asString(), Allergy[].class);
	// logger.info("allergies: " + Arrays.toString(allergies));
	// Assert.assertTrue(allergies != null && allergies.length > 0);
	// }
	//
	// @Test
	// public void getByIdTest() throws Exception {
	// Response response = RestAssured.get("http://localhost:8080/incafe-rest/allergy/getById?id=1");
	// logger.info("RESPONSE: " + response.asString());
	// Allergy allergy = MAPPER.readValue(response.asString(), Allergy.class);
	// Allergy peanut = new Allergy();
	// peanut.setId(1L);
	// peanut.setName("Арахис");
	// Assert.assertTrue(allergy != null && allergy.equals(peanut));
	// }

}
