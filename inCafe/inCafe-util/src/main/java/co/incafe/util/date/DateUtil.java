package co.incafe.util.date;

import java.util.Calendar;

public class DateUtil {

	public static Calendar removeTimeFromDate(Calendar date) {
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		date.add(Calendar.HOUR, 0);
		return date;
	}

	public static Calendar removeTimeFromDate(Calendar date, int days) {
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		date.add(Calendar.HOUR, 0);
		date.add(Calendar.DAY_OF_MONTH, days);
		return date;
	}

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		System.out.println(removeTimeFromDate(calendar).getTime());
		System.out.println(removeTimeFromDate(calendar, -1).getTime());
	}
}
