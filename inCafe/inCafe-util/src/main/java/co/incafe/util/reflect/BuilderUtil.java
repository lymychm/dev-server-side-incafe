package co.incafe.util.reflect;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mykhailo Lymych
 * 
 */
public class BuilderUtil {

	public static <T, T1> List<T1> getFieldValue(List<T> entities, String fieldName, Class<T1> fieldClass) {
		return getSubClassFieldValue(entities, fieldName, fieldClass, null, null);
	}

	@SuppressWarnings("unchecked")
	public static <T, T1, T2> List<T1> getSubClassFieldValue(List<T> entities, String fieldName, Class<T1> fieldType, String subClassFieldName, Class<T2> subClassfieldType) {
		try {
			List<T1> result = new ArrayList<>();
			if (entities != null) {
				for (T e : entities) {
					if (subClassFieldName != null) {
						T2 field = (T2) field(e, subClassFieldName).get(e);
						T1 value = (T1) field(field, fieldName).get(field);
						result.add(value);
					} else {
						T1 value = (T1) field(e, fieldName).get(e);
						result.add(value);
					}
				}
			}
			return result;
		} catch (Exception e) {
			return null;
		}
	}

	private static <T> Field field(T entity, String fieldName) throws Exception {
		Field field = entity.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);
		return field;
	}

	public static <T> List<Object> getFieldValues(List<T> entities, String fieldName) throws Exception {
		List<Object> result = new ArrayList<>();
		if (entities != null) {
			for (T entity : entities) {
				Object value = field(entity, fieldName).get(entity);
				result.add(value);
			}
		}
		return result;
	}
}
