package co.incafe.ejb.entity.enums;

/**
 * @author Mykhailo Lymych
 * 
 */
public enum CallingCode {

	COME, MENU, CLEAN, CHECK, BRING_WATER, CHANGE;

	public static CallingCode getWcCode(int code) {
		for (CallingCode cc : CallingCode.values()) {
			if (cc.ordinal() == code) {
				return cc;
			}
		}
		return null;
	}

}
