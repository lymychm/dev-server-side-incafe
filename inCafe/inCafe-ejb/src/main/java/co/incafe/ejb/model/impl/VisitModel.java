package co.incafe.ejb.model.impl;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.VisitService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.date.DateUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class VisitModel implements VisitService {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public VisitModel() {
    }

    @Override
    public List<Visit> selectAll() throws InCafeException {
	try {
	    return ModelHelper.selectAll(Visit.class, entityManager);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public Visit selectById(long id) throws InCafeException {
	try {
	    return entityManager.find(Visit.class, id);
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void insert(Visit entity) throws InCafeException {
	try {
	    entityManager.persist(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void update(Visit entity) throws InCafeException {
	try {
	    entityManager.merge(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void delete(Visit entity) throws InCafeException {
	try {
	    entity = selectById(entity.getId());
	    entityManager.remove(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Visit> selectTodayVisitByUserAndRes(Long userId, Long resId) throws InCafeException {
	try {
	    String queryString = "SELECT x FROM Visit x WHERE x.restaurant.id = :resId AND x.user.id = :userId AND x.outTime = null AND x.inTime >= :date ORDER BY x.inTime desc";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resId", resId);
	    query.setParameter("date", DateUtil.removeTimeFromDate(Calendar.getInstance()), TemporalType.TIMESTAMP);
	    query.setParameter("userId", userId);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Visit> selectTodayUsersInRestaurant(Long resId) throws InCafeException {
	try {
	    String queryString = "SELECT x FROM Visit x WHERE x.restaurant.id = :resId AND x.outTime = null AND x.inTime >= :date";
	    Query query = entityManager.createQuery(queryString, User.class);
	    query.setParameter("date", DateUtil.removeTimeFromDate(Calendar.getInstance()), TemporalType.TIMESTAMP);
	    query.setParameter("resId", resId);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Visit> selectForLast30DaysByRestaurant(Long resId) throws InCafeException {
	try {
	    String queryString = "SELECT x FROM Visit x WHERE x.restaurant.id = :resId AND x.inTime > :date order by x.user";
	    Query query = entityManager.createQuery(queryString, User.class);
	    query.setParameter("resId", resId);
	    Calendar date = Calendar.getInstance();
	    date.add(Calendar.DAY_OF_MONTH, -30);
	    query.setParameter("date", DateUtil.removeTimeFromDate(date), TemporalType.TIMESTAMP);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Visit> selectByUser(Long userId) throws InCafeException {
	try {
	    String queryString = "SELECT x FROM Visit x WHERE x.user.id = :userId";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("userId", userId);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    
    @Override
    @SuppressWarnings("unchecked")
    public List<Visit> selectFromJan19Statistic(Long resId) throws InCafeException {
	try {
	    String queryString = "SELECT x FROM Visit x WHERE x.restaurant.id = :resId AND x.inTime > :date";
	    Query query = entityManager.createQuery(queryString, User.class);
	    query.setParameter("resId", resId);
	    Calendar date = Calendar.getInstance();
	    date.set(Calendar.YEAR, 2016);
	    date.set(Calendar.MONTH, Calendar.JANUARY);
	    date.set(Calendar.DAY_OF_MONTH, 19);
	    query.setParameter("date", DateUtil.removeTimeFromDate(date), TemporalType.TIMESTAMP);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

}
