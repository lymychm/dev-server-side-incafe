/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface RestaurantService extends GeneralService<Restaurant> {

	public Restaurant selectByIbeacon(String ibeacon) throws InCafeException;

	public Restaurant selectByResId(String clientResId) throws InCafeException;

	public Restaurant selectByNameAndCity(String name, Long cityId) throws InCafeException;

	public List<String> getUids() throws InCafeException;

	public List<Restaurant> selectActiveByChain(RestaurantChain restaurantChain) throws InCafeException;

	public List<Restaurant> selectLimitedByChain(int offset, int limit, RestaurantChain restaurantChain, String searchField) throws InCafeException;

	public long count(RestaurantChain restaurantChain, String searchField) throws InCafeException;

	public List<Restaurant> selectAllTest() throws InCafeException;

}
