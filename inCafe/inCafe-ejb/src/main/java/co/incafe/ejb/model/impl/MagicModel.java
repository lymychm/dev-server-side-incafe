package co.incafe.ejb.model.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Allergy;
import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.Hate;
import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.magic.strategy.BreakfastMagic;
import co.incafe.ejb.magic.strategy.DinnerMagic;
import co.incafe.ejb.magic.strategy.DoMagic;
import co.incafe.ejb.magic.strategy.LunchMagic;
import co.incafe.ejb.magic.strategy.MagicEntity;
import co.incafe.ejb.model.DishService;
import co.incafe.ejb.model.MagicService;
import co.incafe.ejb.model.TableService;
import co.incafe.ejb.model.UserService;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class MagicModel implements MagicService {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final Hate HATE_ALCOHOL = new Hate(10L);

    private static final Tag vegTag = new Tag(176L);

    @SuppressWarnings("serial")
    private static final List<Long> IGNORE_MEAT_TAGS = new ArrayList<Long>() {
	{
	    add(170L); // "говядина"
	    add(173L); // "свинина"
	    add(174L); // "курица"
	    add(179L); // "морепродукты"
	}
    };

    @SuppressWarnings("serial")
    private static final List<Long> IGNORE_TYPES = new ArrayList<Long>() {
	{
	    add(29L); // хлеб
	    add(33L); // соусы
	    add(40L); // вода и прочее
	    add(44L); // прочие
	    add(28L); // аперитивы
	}
    };

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @EJB
    private UserService userService;

    @EJB
    private TableService tableService;

    @EJB
    private DishService dishService;

    public MagicModel() {
    }

    @Override
    public List<Long> magic(String userGuid, String ibeacon, Long time) throws InCafeException {
	try {
	    MagicEntity magicEntity = getMagicEntity(userGuid, ibeacon, time);
	    return new DoMagic(magicEntity).magic();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public List<Dish> magicDishes(String userGuid, String ibeacon, Long time) throws InCafeException {
	try {
	    MagicEntity magicEntity = getMagicEntity(userGuid, ibeacon, time);
	    return new DoMagic(magicEntity).magicDishes();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    private MagicEntity getMagicEntity(String userGuid, String ibeacon, Long time) throws InCafeException {
	User user = userService.selectByGuid(userGuid);
	if (user == null) {
	    user = new User();
	}
	Long resId = tableService.selectByIbaconId(ibeacon).getRestaurant().getId();
	if (resId == null) {
	    throw new IllegalArgumentException("Table not found by ibeacon: " + ibeacon);
	}
	List<Allergy> allergies = user.getAllergies();
	List<Hate> hates = user.getHates();
	List<Prefer> prefers = user.getPrefers();

	Set<Long> ignoreTags = getIgnoreTagIds(allergies, hates);
	List<Long> ignoreTypes = new ArrayList<>(IGNORE_TYPES);

	checkVegiterian(prefers, ignoreTags);

	Calendar now = Calendar.getInstance();
	now.setTimeInMillis(time);
	int hours = now.get(Calendar.HOUR_OF_DAY);
	boolean breakfast = hours > 8 && hours < 11;
	boolean lunch = hours > 11 && hours < 16;
	MagicEntity magicEntity = null;
	if (breakfast) {
	    ignoreTypes.add(39L); // lunch
	    magicEntity = new BreakfastMagic(user);
	    doMagic(user, resId, hates, magicEntity, ignoreTags, false, ignoreTypes);
	} else if (lunch) {
	    ignoreTypes.add(1L); // breakfast
	    magicEntity = new LunchMagic(user);
	    doMagic(user, resId, hates, magicEntity, ignoreTags, false, ignoreTypes);
	} else {
	    boolean includeAlcohol = !hates.contains(HATE_ALCOHOL);
	    ignoreTypes.add(1L); // breakfast
	    ignoreTypes.add(39L); // lunch

	    magicEntity = new DinnerMagic(user);
	    doMagic(user, resId, hates, magicEntity, ignoreTags, includeAlcohol, ignoreTypes);
	}
	return magicEntity;
    }

    private void checkVegiterian(List<Prefer> prefers, Set<Long> ignoreTags) {
	for (Prefer prefer : prefers) {
	    for (Tag tag : prefer.getTags()) {
		if (tag.equals(vegTag)) {
		    ignoreTags.addAll(IGNORE_MEAT_TAGS);
		    return;
		}
	    }
	}
    }

    private void doMagic(User user, Long resId, List<Hate> hates, MagicEntity magicEntity, Set<Long> ignoreTags, boolean includeAlcohol, List<Long> ignoreTypes)
	    throws InCafeException {
	List<Dish> ratingDishes = dishService.selectByUserRating(resId, user.getId(), includeAlcohol, ignoreTypes);
	magicEntity.fillData(ratingDishes, magicEntity.getMainPositions());

	List<Dish> mainTagDishes = dishService.selectByMainTags(resId, user.getId(), includeAlcohol, ignoreTypes);
	magicEntity.fillData(mainTagDishes, magicEntity.getMainPositions());

	List<Long> ignoreDishes = new ArrayList<>();
	ratingDishes.forEach(dish -> ignoreDishes.add(dish.getId()));
	mainTagDishes.forEach(dish -> ignoreDishes.add(dish.getId()));

	List<Dish> dishs = dishService.selectAllAndIgnoreByTags(resId, ignoreTags, includeAlcohol, ignoreTypes, ignoreDishes);
	magicEntity.fillData(dishs, magicEntity.getPositions());
    }

    private Set<Long> getIgnoreTagIds(List<Allergy> allergies, List<Hate> hates) {
	Set<Long> result = new HashSet<>();
	for (Allergy allergy : allergies) {
	    for (Tag tag : allergy.getTags()) {
		result.add(tag.getId());
	    }
	}
	for (Hate hate : hates) {
	    for (Tag tag : hate.getTags()) {
		result.add(tag.getId());
	    }
	}
	// allergies.forEach(allergy -> allergy.getTags().forEach(tag -> result.add(tag.getId())));
	// hates.forEach(hate -> hate.getTags().forEach(tag -> result.add(tag.getId())));
	return result;
    }

}
