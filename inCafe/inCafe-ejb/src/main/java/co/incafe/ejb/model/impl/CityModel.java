package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.City;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.CityService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class CityModel implements CityService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public CityModel() {
	}

	@Override
	public List<City> selectAll() throws InCafeException {
		try {
			return ModelHelper.selectAll(City.class, entityManager);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public City selectById(long id) throws InCafeException {
		try {
			return entityManager.find(City.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(City entity) throws InCafeException {
		try {
			entityManager.persist(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(City entity) throws InCafeException {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(City entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public City selectByNameAndCountryId(String name, Long countryId) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT x FROM City x WHERE x.name = :name AND x.country.id = :countryId", City.class);
			query.setParameter("countryId", countryId);
			query.setParameter("name", name);
			return (City) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public City selectByNameAndCountryName(String name, String countryName) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT x FROM City x WHERE x.name = :name AND x.country.named = :countryName", City.class);
			query.setParameter("countryName", countryName);
			query.setParameter("name", name);
			return (City) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

}
