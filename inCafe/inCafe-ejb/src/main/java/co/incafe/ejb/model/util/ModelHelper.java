package co.incafe.ejb.model.util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 * @author Mykhailo Lymych
 * 
 */
public class ModelHelper {

	// private static final int MAX_RESULT = 100;

	public static <T> List<T> selectAll(Class<T> clazz, EntityManager entityManager, String... orderBy) throws Exception {
		try {
			return selectLimited(0, Integer.MAX_VALUE, clazz, entityManager, orderBy);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> selectLimited(int offset, int limit, Class<T> clazz, EntityManager entityManager, String... orderBy) throws Exception {
		try {
			Query query = entityManager.createQuery("SELECT x FROM " + clazz.getSimpleName() + " x "
					+ ((orderBy == null || orderBy.length == 0) ? " ORDER BY x.id" : getOrderBy(orderBy)), clazz);
			return query.setFirstResult(offset).setMaxResults(limit).getResultList();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	private static String getOrderBy(String[] orderBy) {
		String result = "";
		for (String fields : orderBy) {
			result += fields;
		}
		return result;
	}

	public static <T> long getCount(Class<T> clazz, EntityManager entityManager) throws Exception {
		try {
			CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
			criteriaQuery.select(criteriaBuilder.count(criteriaQuery.from(clazz)));
			return entityManager.createQuery(criteriaQuery).getSingleResult();
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	public static Long getNumberFromSearchField(String searchField) {
		try {
			return Long.valueOf(searchField.replaceAll("%", ""));
		} catch (Exception e) {
			return -1L;
		}
	}

}
