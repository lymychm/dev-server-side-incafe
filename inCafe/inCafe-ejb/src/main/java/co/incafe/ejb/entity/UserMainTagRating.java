package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "user_main_tag_rating")
public class UserMainTagRating implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_update")
    private Calendar lastUpdate;

    private double rating;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "main_tag_id")
    private MainTag mainTag;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public UserMainTagRating() {
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Calendar getLastUpdate() {
	return lastUpdate;
    }

    public void setLastUpdate(Calendar lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    public double getRating() {
	return this.rating;
    }

    public void setRating(double rating) {
	this.rating = rating;
    }

    public MainTag getMainTag() {
	return this.mainTag;
    }

    public void setMainTag(MainTag mainTag) {
	this.mainTag = mainTag;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	UserMainTagRating other = (UserMainTagRating) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "UserMainTagRating [id=" + id + ", lastUpdate=" + lastUpdate + ", rating=" + rating + ", mainTag=" + mainTag + ", user=" + user + "]";
    }

}