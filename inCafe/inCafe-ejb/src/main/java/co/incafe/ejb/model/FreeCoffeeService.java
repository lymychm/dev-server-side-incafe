/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.FreeCoffee;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface FreeCoffeeService extends GeneralService<FreeCoffee> {

	public FreeCoffee selectByResId(Long resId) throws InCafeException;

}
