package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@Table(name = "web_locale_key")
public class WebLocaleKey implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	@OneToMany(mappedBy = "webLocaleKey", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<WebLocaleValue> webLocaleValues;

	public WebLocaleKey() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WebLocaleValue> getWebLocaleValues() {
		return this.webLocaleValues;
	}

	public void setWebLocaleValues(List<WebLocaleValue> webLocaleValues) {
		this.webLocaleValues = webLocaleValues;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		WebLocaleKey other = (WebLocaleKey) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "WebLocaleKey [id=" + id + ", name=" + name + "]";
	}

}