/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.demo.Demo;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface DemoService extends GeneralService<Demo> {

	public Demo selectDemo(String guid, String ibeacon) throws InCafeException;

}
