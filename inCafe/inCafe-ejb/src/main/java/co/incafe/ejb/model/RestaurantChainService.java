/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.RestaurantChain;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface RestaurantChainService extends GeneralService<RestaurantChain> {

}
