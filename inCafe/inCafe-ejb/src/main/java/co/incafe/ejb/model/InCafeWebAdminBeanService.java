package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;

public interface InCafeWebAdminBeanService<T> extends GeneralService<T> {

	public List<T> selectLimited(int offset, int limit, String searchField) throws InCafeException, Exception;

	public long count(String searchField) throws InCafeException, Exception;

	public void checkUniqueName(String name) throws EntityExistException, InCafeException;

}
