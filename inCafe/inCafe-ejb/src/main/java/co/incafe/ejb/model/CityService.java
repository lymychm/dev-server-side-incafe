/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.City;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface CityService extends GeneralService<City> {

	public City selectByNameAndCountryId(String name, Long countryId) throws InCafeException;
	
	public City selectByNameAndCountryName(String name, String countryName) throws InCafeException;

}
