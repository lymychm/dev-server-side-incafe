package co.incafe.ejb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
public class Tablet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String guid;

    @Column(name = "push_id")
    private String pushId;

    private int status;

    @ManyToOne(fetch = FetchType.LAZY)
    private Restaurant restaurant;

    public Tablet() {
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getGuid() {
	return this.guid;
    }

    public void setGuid(String guid) {
	this.guid = guid;
    }

    public String getPushId() {
	return this.pushId;
    }

    public void setPushId(String pushId) {
	this.pushId = pushId;
    }

    public Restaurant getRestaurant() {
	return this.restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
	this.restaurant = restaurant;
    }

    public int getStatus() {
	return status;
    }

    public void setStatus(int status) {
	this.status = status;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Tablet other = (Tablet) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Tablet [id=" + id + ", guid=" + guid + ", pushId=" + pushId + ", status=" + status + "]";
    }

}