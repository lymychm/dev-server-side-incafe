/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.Product;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface ProductService extends InCafeWebAdminBeanService<Product> {

}
