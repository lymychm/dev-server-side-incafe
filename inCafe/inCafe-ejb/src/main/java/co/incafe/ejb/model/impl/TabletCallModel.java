package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Hate;
import co.incafe.ejb.entity.TabletServiceCall;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.TabletCallService;
import co.incafe.ejb.model.util.FieldHolder;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class TabletCallModel implements TabletCallService {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public TabletCallModel() {
    }

    @Override
    public List<TabletServiceCall> selectAll() throws InCafeException {
	try {
	    return ModelHelper.selectAll(TabletServiceCall.class, entityManager);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public TabletServiceCall selectById(long id) throws InCafeException {
	try {
	    return entityManager.find(TabletServiceCall.class, id);
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void insert(TabletServiceCall entity) throws EntityExistException, InCafeException {
	try {
	    entityManager.persist(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void update(TabletServiceCall entity) throws EntityExistException, InCafeException {
	try {
	    entityManager.merge(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void delete(TabletServiceCall entity) throws InCafeException {
	try {
	    entity = selectById(entity.getId());
	    entityManager.remove(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public TabletServiceCall selectByTabletGuid(String guid) throws InCafeException {
	try {
	    String queryString = "SELECT x FROM TabletServiceCall x WHERE x.tablet.guid = :guid";
	    Query query = entityManager.createQuery(queryString, Hate.class);
	    query.setParameter(FieldHolder.GUID, guid);
	    return (TabletServiceCall) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

}
