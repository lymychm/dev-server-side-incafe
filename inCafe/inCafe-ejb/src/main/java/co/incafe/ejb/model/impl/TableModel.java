package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.TableService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class TableModel implements TableService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public TableModel() {
	}

	@Override
	public List<Tables> selectAll() throws InCafeException {
		try {
			return ModelHelper.selectAll(Tables.class, entityManager);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Tables selectById(long id) throws InCafeException {
		try {
			return entityManager.find(Tables.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(Tables entity) throws InCafeException {
		try {
			entityManager.persist(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(Tables entity) throws InCafeException {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(Tables entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Tables selectByIbaconId(String ibeacon) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT u FROM Tables u WHERE u.ibeacon = :ibeacon", Tables.class);
			query.setParameter("ibeacon", ibeacon);
			return (Tables) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return new Tables();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

}
