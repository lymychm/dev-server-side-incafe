/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.UserMainTagRating;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface UserMainTagRatingService extends GeneralService<UserMainTagRating> {

}
