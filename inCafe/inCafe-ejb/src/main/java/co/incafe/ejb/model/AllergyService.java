/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Allergy;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface AllergyService extends InCafeWebAdminBeanService<Allergy> {

	public List<Allergy> selectByIds(List<Long> allergyIds) throws InCafeException;

}
