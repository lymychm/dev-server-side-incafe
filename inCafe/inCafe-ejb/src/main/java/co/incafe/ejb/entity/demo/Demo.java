package co.incafe.ejb.entity.demo;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.entity.User;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@NamedQuery(name = "Demo.findAll", query = "SELECT d FROM Demo d")
public class Demo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	private Tables table;

	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	public Demo() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Tables getTable() {
		return this.table;
	}

	public void setTable(Tables table) {
		this.table = table;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}