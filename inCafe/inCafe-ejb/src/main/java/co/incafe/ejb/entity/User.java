package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import co.incafe.ejb.entity.enums.Gender;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    @Column(name = "first_name")
    private String firstName;

    private String guid;

    private String info;

    private String description;

    private String ip;

    @Enumerated(EnumType.ORDINAL)
    private Gender gender;

    @Column(name = "last_name")
    private String lastName;

    private String photo;

    private Integer os;

    private String fbid;

    @Column(name = "registration_date")
    @Temporal(TemporalType.DATE)
    private Date registrationDate;

    @Column(name = "birth_date")
    private Long birthDate;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private List<Visit> visits;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private List<Order> orders;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<UserDishRating> userDishRatings;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private List<Dislike> dislikes;

    @ManyToMany
    @JoinTable(name = "user_allergy", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "allergy_id", insertable = false) })
    private List<Allergy> allergies;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<UserOrder> userOrders;

    @ManyToMany
    @JoinTable(name = "user_hate", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "hate_id", insertable = false) })
    private List<Hate> hates;

    @ManyToMany
    @JoinTable(name = "user_prefer", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "prefer_id", insertable = false) })
    private List<Prefer> prefers;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserMainTagRating> userMainTagRatings;

    @Transient
    private List<Long> preferIds;

    @Transient
    private List<Long> hateIds;

    @Transient
    private List<Long> allergyIds;

    @Transient
    private byte[] userPhoto;

    @Transient
    private Long resId;

    @Transient
    private boolean regularUser;

    @Transient
    private boolean newUser;

    public User() {
    }

    public User(Long id) {
	super();
	this.id = id;
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getEmail() {
	return this.email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getFirstName() {
	return this.firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getGuid() {
	return this.guid;
    }

    public void setGuid(String guid) {
	this.guid = guid;
    }

    public String getLastName() {
	return this.lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getPhoto() {
	return this.photo;
    }

    public void setPhoto(String photo) {
	this.photo = photo;
    }

    public List<Dislike> getDislikes() {
	return this.dislikes;
    }

    public void setDislikes(List<Dislike> dislikes) {
	this.dislikes = dislikes;
    }

    public List<UserDishRating> getUserDishRatings() {
	if (userDishRatings == null) {
	    userDishRatings = new ArrayList<>();
	}
	return userDishRatings;
    }

    public void setUserDishRatings(List<UserDishRating> userDishRatings) {
	this.userDishRatings = userDishRatings;
    }

    public List<Allergy> getAllergies() {
	if (allergies == null) {
	    allergies = new ArrayList<>();
	}
	return allergies;
    }

    public void setAllergies(List<Allergy> allergies) {
	this.allergies = allergies;
    }

    public Long getBirthDate() {
	return birthDate;
    }

    public void setBirthDate(Long birthDate) {
	this.birthDate = birthDate;
    }

    public List<Visit> getVisits() {
	return visits;
    }

    public void setVisits(List<Visit> visits) {
	this.visits = visits;
    }

    public List<UserOrder> getUserOrders() {
	return userOrders;
    }

    public void setUserOrders(List<UserOrder> userOrders) {
	this.userOrders = userOrders;
    }

    public Gender getGender() {
	return gender;
    }

    public void setGender(Gender gender) {
	this.gender = gender;
    }

    public String getInfo() {
	return info;
    }

    public void setInfo(String info) {
	this.info = info;
    }

    public Date getRegistrationDate() {
	return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
	this.registrationDate = registrationDate;
    }

    public List<Hate> getHates() {
	if (hates == null) {
	    hates = new ArrayList<>();
	}
	return hates;
    }

    public void setHates(List<Hate> hates) {
	this.hates = hates;
    }

    public List<Prefer> getPrefers() {
	if (prefers == null) {
	    prefers = new ArrayList<>();
	}
	return prefers;
    }

    public void setPrefers(List<Prefer> prefers) {
	this.prefers = prefers;
    }

    public byte[] getUserPhoto() {
	return userPhoto;
    }

    public void setUserPhoto(byte[] userPhoto) {
	this.userPhoto = userPhoto;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Long getResId() {
	return resId;
    }

    public void setResId(Long resId) {
	this.resId = resId;
    }

    public List<Long> getAllergyIds() {
	return allergyIds;
    }

    public void setAllergyIds(List<Long> allergyIds) {
	this.allergyIds = allergyIds;
    }

    public List<Long> getPreferIds() {
	return preferIds;
    }

    public void setPreferIds(List<Long> preferIds) {
	this.preferIds = preferIds;
    }

    public List<Long> getHateIds() {
	return hateIds;
    }

    public void setHateIds(List<Long> hateIds) {
	this.hateIds = hateIds;
    }

    public String getIp() {
	return ip;
    }

    public void setIp(String ip) {
	this.ip = ip;
    }

    public boolean isRegularUser() {
	return regularUser;
    }

    public void setRegularUser(boolean regularUser) {
	this.regularUser = regularUser;
    }

    public List<Order> getOrders() {
	return orders;
    }

    public void setOrders(List<Order> orders) {
	this.orders = orders;
    }

    public boolean isNewUser() {
	return newUser;
    }

    public void setNewUser(boolean newUser) {
	this.newUser = newUser;
    }

    public String getFbid() {
	return fbid;
    }

    public void setFbid(String fbid) {
	this.fbid = fbid;
    }

    public List<UserMainTagRating> getUserMainTagRatings() {
	if (userMainTagRatings == null) {
	    userMainTagRatings = new ArrayList<>();
	}
	return userMainTagRatings;
    }

    public void setUserMainTagRatings(List<UserMainTagRating> userMainTagRatings) {
	this.userMainTagRatings = userMainTagRatings;
    }

    public Integer getOs() {
	return os;
    }

    public void setOs(Integer os) {
	this.os = os;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	User other = (User) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "User [id=" + id + ", email=" + email + ", firstName=" + firstName + ", guid=" + guid + ", info=" + info + ", description=" + description + ", ip=" + ip
		+ ", gender=" + gender + ", lastName=" + lastName + ", photo=" + photo + ", fbid=" + fbid + ", registrationDate=" + registrationDate + ", birthDate=" + birthDate
		+ ", preferIds=" + preferIds + ", hateIds=" + hateIds + ", allergyIds=" + allergyIds + ", resId=" + resId + ", regularUser=" + regularUser + ", newUser=" + newUser
		+ "]";
    }

}