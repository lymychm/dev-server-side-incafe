package co.incafe.ejb.entity.comparators;

import java.util.Comparator;

import co.incafe.ejb.entity.Dish;

public class MagicComparator implements Comparator<Dish> {

    @Override
    public int compare(Dish dish1, Dish dish2) {
	int c;
	c = dish1.getPositionInMenu().compareTo(dish2.getPositionInMenu());
	if (c == 0) {
	    c = new Boolean(dish1.getPhotoBig() == null).compareTo(new Boolean(dish2.getPhotoBig() == null));
	}
	if (c == 0) {
	    c = dish2.getRating().compareTo(dish1.getRating());
	}
	if (c == 0) {
	    c = dish1.getId().compareTo(dish2.getId());
	}
	return c;
    }

}
