package co.incafe.ejb.magic.strategy;

import java.util.List;

import co.incafe.ejb.entity.Dish;

public class DoMagic {

    private MagicStrategy magic;

    public DoMagic(MagicStrategy magic) {
	super();
	this.magic = magic;
    }

    public List<Long> magic() {
	return magic.doMagic();
    }

    public List<Dish> magicDishes() {
	return magic.doMagicDishes();
    }

}
