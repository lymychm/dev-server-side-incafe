/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.EventStatistic;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface EventStatisticService extends GeneralService<EventStatistic> {

    public List<EventStatistic> selectByUser(Long userId) throws InCafeException;

}
