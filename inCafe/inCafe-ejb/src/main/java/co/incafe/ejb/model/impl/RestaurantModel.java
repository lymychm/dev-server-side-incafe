package co.incafe.ejb.model.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.RestaurantService;
import co.incafe.ejb.model.util.FieldHolder;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class RestaurantModel implements RestaurantService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public RestaurantModel() {
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Restaurant> selectAll() throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT x FROM Restaurant x WHERE x.status = 1", Restaurant.class);
			return query.getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Restaurant> selectAllTest() throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT x FROM Restaurant x ", Restaurant.class);
			return query.getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Restaurant selectById(long id) throws InCafeException {
		try {
			return entityManager.find(Restaurant.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(Restaurant entity) throws InCafeException {
		try {
			entityManager.persist(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(Restaurant entity) throws InCafeException {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(Restaurant entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Restaurant selectByIbeacon(String ibeacon) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT t FROM Tables t WHERE t.ibeacon = :ibeacon", Tables.class);
			query.setParameter("ibeacon", ibeacon);
			query.setFirstResult(0);
			query.setMaxResults(1);
			return ((Tables) query.getSingleResult()).getRestaurant();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Restaurant selectByResId(String clientResId) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT r FROM Restaurant r WHERE r.clientResId = :clientResId", Restaurant.class);
			query.setParameter("clientResId", clientResId);
			return (Restaurant) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Restaurant selectByNameAndCity(String name, Long cityId) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT r FROM Restaurant r WHERE r.name = :name and r.city.id = :cityId", Restaurant.class);
			query.setParameter("name", name);
			query.setParameter("cityId", cityId);
			return (Restaurant) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<String> getUids() throws InCafeException {
		List<String> result = new ArrayList<>();
		try {
			Query query = entityManager.createQuery("SELECT new co.incafe.ejb.entity.Restaurant(r.regionUUID) FROM Restaurant r ", Restaurant.class);
			for (Restaurant uuid : (List<Restaurant>) query.getResultList()) {
				result.add(uuid.getRegionUUID());
			}
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
		return result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Restaurant> selectActiveByChain(RestaurantChain restaurantChain) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT x FROM Restaurant x WHERE x.restaurantChain.id = :chainId", Restaurant.class);
			query.setParameter("chainId", restaurantChain.getId());
			return query.getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Restaurant> selectLimitedByChain(int offset, int limit, RestaurantChain restaurantChain, String searchField) throws InCafeException {
		try {
			Query query = getSearchQuery(false, restaurantChain, searchField);
			return query.setFirstResult(offset).setMaxResults(limit).getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public long count(RestaurantChain restaurantChain, String searchField) throws InCafeException {
		try {
			Query query = getSearchQuery(true, restaurantChain, searchField);
			return (long) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return 0;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	private Query getSearchQuery(boolean isCount, RestaurantChain restaurantChain, String searchField) {
		String count = " count(x) ";
		String queryString = "SELECT" + (isCount ? count : " x ")
				+ "FROM Restaurant x WHERE x.restaurantChain.id = :chainId AND UPPER(x.name) like UPPER(:searchField) OR x.id = :id";
		Query result = entityManager.createQuery(queryString, Dish.class);

		result.setParameter(FieldHolder.CHAIN_ID, restaurantChain.getId());
		result.setParameter(FieldHolder.SEARCH_FIELD, FieldHolder.PERCENT + searchField.trim() + FieldHolder.PERCENT);
		result.setParameter(FieldHolder.ID, ModelHelper.getNumberFromSearchField(searchField));
		return result;
	}

}
