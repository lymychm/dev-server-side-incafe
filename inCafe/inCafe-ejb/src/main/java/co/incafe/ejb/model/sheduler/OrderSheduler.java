package co.incafe.ejb.model.sheduler;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.util.general.LoggerUtil;

@Singleton
public class OrderSheduler {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	@PostConstruct
	private void init() {
		logger.info("OrderSheduler INIT");
	}

	@PreDestroy
	private void destroy() {
		logger.info("OrderSheduler DESTROY");
	}

	// 0 to 7 (both 0 and 7 refer to Sunday). For example: dayOfWeek="3".
	// Sun, Mon, Tue, Wed, Thu, Fri, Sat. For example: dayOfWeek="Mon".

	// @Schedule(minute = "*/1", hour = "*")
	// @Schedule(dayOfWeek = "1")
	public void cleanOrder() {
		try {
			// TODO
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
		}
	}
}
