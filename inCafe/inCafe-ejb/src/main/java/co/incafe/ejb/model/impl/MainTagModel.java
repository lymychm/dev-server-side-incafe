package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.MainTag;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.MainTagService;
import co.incafe.ejb.model.util.FieldHolder;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class MainTagModel implements MainTagService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public MainTagModel() {
	}

	@Override
	public List<MainTag> selectAll() throws InCafeException {
		try {
			return ModelHelper.selectAll(MainTag.class, entityManager);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public MainTag selectById(long id) throws InCafeException {
		try {
			return entityManager.find(MainTag.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(MainTag entity) throws EntityExistException, InCafeException {
		try {
			String name = entity.getName().trim().toLowerCase();
			checkUniqueName(name);
			entity.setName(name);
			entityManager.persist(entity);
		} catch (EntityExistException e) {
			throw new EntityExistException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(MainTag entity) throws EntityExistException, InCafeException {
		try {
			String name = entity.getName().trim().toLowerCase();
			entity.setName(name);
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(MainTag entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<MainTag> selectLimited(int offset, int limit, String searchField) throws InCafeException {
		try {
			Query query = getSearchQuery(false, searchField);
			return query.setFirstResult(offset).setMaxResults(limit).getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public long count(String searchField) throws InCafeException {
		try {
			Query query = getSearchQuery(true, searchField);
			return (long) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return 0;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	private Query getSearchQuery(boolean isCount, String searchField) {
		String count = " count(x) ";
		String queryString = "SELECT" + (isCount ? count : " x ") + "FROM MainTag x WHERE x.id = :id or (UPPER(x.name) like UPPER(:searchField))"
				+ (isCount ? "" : "ORDER By x.id");
		Query result = entityManager.createQuery(queryString, MainTag.class);

		result.setParameter(FieldHolder.SEARCH_FIELD, FieldHolder.PERCENT + searchField.trim() + FieldHolder.PERCENT);
		result.setParameter(FieldHolder.ID, ModelHelper.getNumberFromSearchField(searchField));
		return result;
	}

	@Override
	public void checkUniqueName(String name) throws EntityExistException, InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT x FROM MainTag x WHERE UPPER(x.name) = UPPER(:name)", MainTag.class);
			query.setParameter("name", name);
			MainTag result = (MainTag) query.getSingleResult();
			if (result != null) {
				throw new EntityExistException();
			}
		} catch (NoResultException e) {
			/* NOP */
		} catch (EntityExistException e) {
			throw new EntityExistException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

}
