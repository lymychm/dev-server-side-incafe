package co.incafe.ejb.model.util;

/**
 * @author LM
 *
 */
public class FieldHolder {

    public static final String PERCENT = "%";

    public static final String SEARCH_FIELD = "searchField";

    public static final String CHAIN_ID = "chainId";

    public static final String ID = "id";

    public static final String GUID = "guid";

}
