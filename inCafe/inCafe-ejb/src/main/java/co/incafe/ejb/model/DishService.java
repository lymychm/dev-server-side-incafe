/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;
import java.util.Set;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface DishService extends GeneralService<Dish> {

    public List<Dish> selectAll(Long resId) throws InCafeException;

    public List<Dish> selectByType(List<Long> typeIds, Long resId) throws InCafeException;

    public List<Dish> selectByIds(List<Long> ids, Long resId) throws InCafeException;

    public Dish selectByResDishId(String resDishId) throws InCafeException;
    
    public Dish selectByRestaurantData(String resDishId, Long resId) throws InCafeException;

    public List<Dish> selectLimited(Long resId, int offset, int limit) throws InCafeException;

    public List<Dish> selectLimitedByRestaurantChain(int offset, int limit, RestaurantChain restaurantChain, String searchField) throws InCafeException;

    public long count(int offset, int limit, RestaurantChain restaurantChain, String searchField) throws InCafeException;

    public List<Long> magic(String guid, String ibeacon) throws InCafeException;

    public List<Long> magic(String guid, String ibeacon, Long time) throws InCafeException;

    public List<Dish> selectAllAndIgnoreByTags(Long resId, Set<Long> tagIds, boolean includeAlcohol, List<Long> ignoreTypes, List<Long> ignoreDishes) throws InCafeException;

    public List<Dish> selectByUserRating(Long resId, Long userId, boolean includeAlcohol, List<Long> ignoreTypes) throws InCafeException;

    public List<Dish> selectByMainTags(Long resId, Long userId, boolean includeAlcohol, List<Long> ignoreTypes) throws InCafeException;

    public List<Dish> selectAll(Long resId, Long date) throws InCafeException;

}
