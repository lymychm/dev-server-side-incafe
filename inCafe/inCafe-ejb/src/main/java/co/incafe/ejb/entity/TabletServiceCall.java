package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author LM
 *
 */
@Entity
@Table(name = "tablet_service_call")
public class TabletServiceCall implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_call")
    private Calendar lastCall;

    @ManyToOne
    @JoinColumn(name = "tablet_guid", referencedColumnName = "guid")
    private Tablet tablet;

    public TabletServiceCall() {
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Calendar getLastCall() {
	return lastCall;
    }

    public void setLastCall(Calendar lastCall) {
	this.lastCall = lastCall;
    }

    public Tablet getTablet() {
	return this.tablet;
    }

    public void setTablet(Tablet tablet) {
	this.tablet = tablet;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (!(obj instanceof TabletServiceCall)) {
	    return false;
	}
	TabletServiceCall other = (TabletServiceCall) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "TabletServiceCall [id=" + id + ", lastCall=" + (lastCall != null ? lastCall.getTime() : null) + ", tablet=" + (tablet != null ? tablet.getGuid() : null) + "]";
    }

}