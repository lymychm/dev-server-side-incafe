package co.incafe.ejb.model.sheduler;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.TabletServiceCall;
import co.incafe.ejb.model.TabletCallService;
import co.incafe.util.general.LoggerUtil;

@Startup
@Singleton
public class CheckTabletIsActive {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    private static final String USER_NAME = "incafe.server@gmail.com";

    private static final String PASS = "12345qweincafe!XXX";

    @EJB
    private TabletCallService tabletCallService;

    @PostConstruct
    private void init() {
	logger.info("CheckTabletIsActive INIT");
    }

    @PreDestroy
    private void destroy() {
	logger.info("CheckTabletIsActive DESTROY");
    }

    // @Schedule(minute = "*/5", hour = "*")
    public void checkTablets() {
	try {
	    logger.info("CheckTabletIsActive " + new Date());

	    Calendar lastTwoMin = Calendar.getInstance();
	    lastTwoMin.add(Calendar.MINUTE, -2);

	    List<TabletServiceCall> tablets = tabletCallService.selectAll();
	    for (TabletServiceCall tabletServiceCall : tablets) {
		if (tabletServiceCall.getLastCall().before(lastTwoMin)) {
		    String res = tabletServiceCall.getTablet().getRestaurant().getName();
		    String result = "RESTAURANT: " + res + ", tablet: " + tabletServiceCall.getTablet().getGuid() + ", last call: " + tabletServiceCall.getLastCall().getTime();
		    sendMail(result);
		}
	    }
	} catch (Exception e) {
	    logger.error("checkTablets : ", e);
	}
    }

    private static Properties getSessionProp() {
	Properties props = new Properties();
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.host", "smtp.gmail.com");
	props.put("mail.smtp.port", "587");
	return props;
    }

    private Session session = Session.getInstance(getSessionProp(), new javax.mail.Authenticator() {
	protected PasswordAuthentication getPasswordAuthentication() {
	    return new PasswordAuthentication(USER_NAME, PASS);
	}
    });

    public void sendMail(String content) throws IOException {
	try {
	    MimeMessage msg = new MimeMessage(session);
	    msg.setFrom(new InternetAddress("incafe.server", "incafe.server"));
	    Address[] to = new Address[3];
	    to[0] = new InternetAddress("lymychm@gmail.com");
	    to[1] = new InternetAddress("maksym.dovbnia@gmail.com");
	    to[2] = new InternetAddress("a.a.ilnitskiy@gmail.com");
	    msg.addRecipients(Message.RecipientType.TO, to);
	    msg.setSubject("INCAFE: TABLET PROBLEM", "UTF-8");
	    msg.setContent(content, MediaType.TEXT_PLAIN + ";charset=utf-8");
	    Transport.send(msg);
	} catch (MessagingException e) {
	    throw new RuntimeException(e);
	}
    }

}
