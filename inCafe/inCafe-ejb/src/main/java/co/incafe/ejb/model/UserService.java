/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.User;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface UserService extends InCafeWebAdminBeanService<User> {

	public User selectByGuid(String guid) throws InCafeException;

	public User selectByEmail(String email) throws InCafeException;

	public User test(String guid, String locale) throws InCafeException;

	public List<User> selectByGuidOrEmail(String guid, String email) throws InCafeException;

}
