package co.incafe.ejb.model.impl;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.User;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.UserService;
import co.incafe.ejb.model.util.FieldHolder;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class UserModel implements UserService {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public UserModel() {
    }

    @Override
    public List<User> selectAll() throws InCafeException {
	try {
	    return ModelHelper.selectAll(User.class, entityManager);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public User selectById(long id) throws InCafeException {
	try {
	    return entityManager.find(User.class, id);
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void insert(User entity) throws InCafeException {
	try {
	    if (entity.getId() == null) {
		entity.setRegistrationDate(new Date());
	    }
	    entityManager.persist(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void update(User entity) throws InCafeException {
	try {
	    entityManager.merge(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void delete(User entity) throws InCafeException {
	try {
	    entity = selectById(entity.getId());
	    entityManager.remove(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public User selectByGuid(String guid) throws InCafeException {
	try {
	    // String queryString = "SELECT DISTINCT u FROM User u INNER JOIN u.allergies a INNER JOIN a.localization l WHERE u.guid = :guid AND l.locale.id = :localeId";
	    String queryString = "SELECT u FROM User u WHERE u.guid = :guid";
	    Query query = entityManager.createQuery(queryString, User.class);
	    query.setParameter("guid", guid);
	    // query.setParameter("localeId", 1);
	    return (User) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public User selectByEmail(String email) throws InCafeException {
	try {
	    String queryString = "SELECT u FROM User u WHERE u.email = :email";
	    Query query = entityManager.createQuery(queryString, User.class);
	    query.setParameter("email", email);
	    return (User) query.getSingleResult();
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    return null;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> selectByGuidOrEmail(String guid, String email) throws InCafeException {
	try {
	    String queryString = "SELECT u FROM User u WHERE u.email = :email or u.guid = :guid";
	    Query query = entityManager.createQuery(queryString, User.class);
	    query.setParameter("email", email);
	    query.setParameter("guid", guid);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public User test(String guid, String locale) throws InCafeException {
	try {
	    String queryString = "SELECT DISTINCT u FROM User u INNER JOIN u.allergies a LEFT JOIN a.localization l WHERE u.guid = :guid AND l.locale.name = :locale";
	    Query query = entityManager.createQuery(queryString, User.class);
	    query.setParameter("guid", guid);
	    query.setParameter("locale", locale);
	    return (User) query.getSingleResult();
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    return null;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> selectLimited(int offset, int limit, String searchField) throws InCafeException {
	try {
	    Query query = getSearchQuery(false, searchField);
	    return query.setFirstResult(offset).setMaxResults(limit).getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public long count(String searchField) throws InCafeException {
	try {
	    Query query = getSearchQuery(true, searchField);
	    return (long) query.getSingleResult();
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    return 0;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    private Query getSearchQuery(boolean isCount, String searchField) {
	String count = " count(x) ";
	String queryString = "SELECT" + (isCount ? count : " x ") + "FROM User x ";
	boolean searchMode = !searchField.trim().equals("");
	queryString += searchMode ? "WHERE (UPPER(x.firstName) like UPPER(:searchField)) or (UPPER(x.guid) like UPPER(:searchField))" : "";

	queryString += (isCount ? "" : "ORDER By x.id");
	Query result = entityManager.createQuery(queryString, User.class);
	if (searchMode) {
	    result.setParameter(FieldHolder.SEARCH_FIELD, FieldHolder.PERCENT + searchField.trim() + FieldHolder.PERCENT);
	}
	return result;
    }

    @Override
    public void checkUniqueName(String name) throws EntityExistException, InCafeException {
	// TODO
    }

}
