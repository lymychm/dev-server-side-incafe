/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.TabletServiceCall;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface TabletCallService extends GeneralService<TabletServiceCall> {

    public TabletServiceCall selectByTabletGuid(String guid) throws InCafeException;

}
