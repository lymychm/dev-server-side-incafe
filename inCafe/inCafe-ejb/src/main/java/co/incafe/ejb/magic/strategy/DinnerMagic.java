package co.incafe.ejb.magic.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.Hate;
import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.entity.User;

public class DinnerMagic extends MagicEntity {

    private static final DishType BEER = new DishType(18L);

    private static final Hate HATE_ALCOHOL = new Hate(10L);

    private static final int DISHES_COUNT = 6;

    private static final int DRINK_COUNT = 2;

    private static final int ALCOHOL_COUNT = 2;

    private static final int MAIN_DISH_COUNT = 2;

    private Set<Dish> mainDishes = new HashSet<>();

    private Set<Dish> dishes = new HashSet<>();

    private Set<Dish> drinks = new HashSet<>();

    private Set<Dish> alcohol = new HashSet<>();

    int mainDishCount = 0;

    int dishCount = 0;

    int drinksCount = 0;

    int alcoholCount = 0;

    private boolean includeAlcohol;

    public DinnerMagic(User user) {
	// preferTags.forEach(prefer -> prefer.getTags().forEach(tag -> this.preferTags.add(tag)));
	boolean includeAlcohol = !user.getHates().contains(HATE_ALCOHOL);
	for (Prefer prefer : user.getPrefers()) {
	    for (Tag tag : prefer.getTags()) {
		this.preferTags.add(tag);
	    }
	}
	this.includeAlcohol = includeAlcohol;
	this.gender = user.getGender();
    }

    @Override
    protected void prepareData() {
	fillMainDish();
	fillDishes();
	fillDrinks();
	fillAlcohol();
    }

    @Override
    public List<Long> doMagic() {
	prepareData();
	List<Long> res = new ArrayList<>();
	Set<Dish> result = new TreeSet<>(magicComparator);
	mainDishes.forEach(dish -> result.add(dish));
	dishes.forEach(dish -> result.add(dish));
	drinks.forEach(dish -> result.add(dish));
	alcohol.forEach(dish -> result.add(dish));
	result.forEach(dish -> res.add(dish.getId()));
	return res;
    }

    @Override
    public List<Dish> doMagicDishes() {
	prepareData();
	List<Dish> result = new ArrayList<>();
	mainDishes.forEach(dish -> result.add(dish));
	dishes.forEach(dish -> result.add(dish));
	drinks.forEach(dish -> result.add(dish));
	alcohol.forEach(dish -> result.add(dish));
	Collections.sort(result, magicComparator);
	return result;
    }

    private void fillMainDish() {
	Set<Dish> mmd = getMainPositions().getDishes().get(MAIN_DISH);
	if (mmd != null) {
	    for (Dish dish : mmd) {
		mainDishes.add(dish);
		mainDishCount++;
		if (mainDishCount == MAIN_DISH_COUNT) {
		    break;
		}
	    }
	    mainDishCount = mainDishes.size();
	}
	Set<Dish> md = getPositions().getDishes().get(MAIN_DISH);
	if (mainDishCount < MAIN_DISH_COUNT && md != null) {
	    for (Dish dish : md) {
		mainDishes.add(dish);
		mainDishCount++;
		if (mainDishCount == MAIN_DISH_COUNT) {
		    break;
		}
	    }
	    mainDishCount = mainDishes.size();
	}
	getMainPositions().getDishes().remove(MAIN_DISH);
	getPositions().getDishes().remove(MAIN_DISH);
    }

    private void fillDishes() {
	fillDishes(DISHES_COUNT + (includeAlcohol ? 0 : 1));
    }

    private void fillDishes(int dishesCount) {
	addDishes(getMainPositions().getDishes(), dishes, dishesCount + dishCount(), dishDupl);
	dishCount = dishes.size();
	if ((dishesCount + dishCount() - dishCount) > 0) {
	    addPrefer(getPositions().getDishes(), dishes, (dishesCount + dishCount() - dishCount), dishDupl);
	    dishCount = dishes.size();
	}
	if ((dishesCount + dishCount() - dishCount) > 0) {
	    addDishes(getPositions().getDishes(), dishes, (dishesCount + dishCount() - dishCount), dishDupl);
	}
    }

    private int dishCount() {
	return MAIN_DISH_COUNT - mainDishCount;
    }

    private void fillDrinks() {
	fillDrinks(DRINK_COUNT + (includeAlcohol ? 0 : 1));
    }

    private void fillDrinks(int drCount) {
	addDishes(getMainPositions().getDrinks(), drinks, drCount, drinkDupl);
	drinksCount = drinks.size();
	if ((drCount - drinksCount) > 0) {
	    addPrefer(getPositions().getDrinks(), drinks, drCount - drinksCount, drinkDupl);
	    drinksCount = drinks.size();
	}
	if ((drCount - drinksCount) > 0) {
	    addDishes(getPositions().getDrinks(), drinks, drCount - drinksCount, drinkDupl);
	}
    }

    private void fillAlcohol() {
	if (includeAlcohol) {
	    checkDrageeBuffaloWingsDish();
	    addDishes(getMainPositions().getAlcohols(), alcohol, ALCOHOL_COUNT - alcoholCount, alcoholDupl);
	    alcoholCount = alcohol.size();
	    if ((ALCOHOL_COUNT - alcoholCount) > 0) {
		addPrefer(getPositions().getAlcohols(), alcohol, ALCOHOL_COUNT - alcoholCount, alcoholDupl);
		alcoholCount = alcohol.size();
	    }
	    if ((ALCOHOL_COUNT - alcoholCount) > 0) {
		Set<Dish> tmp = new TreeSet<>(dishPhotoRatingComparator);
		safetyAddAlcohol(tmp, getPositions().getAlcohols().get(new DishType(18L)));// "пиво"
		safetyAddAlcohol(tmp, getPositions().getAlcohols().get(new DishType(21L)));// "вина"
		// if (gender == Gender.MALE) {
		// safetyAddAlcohol(tmp, getPositions().getAlcohols().get(new DishType(22L))); // "ром"
		// safetyAddAlcohol(tmp, getPositions().getAlcohols().get(new DishType(23L))); // "водка"
		// safetyAddAlcohol(tmp, getPositions().getAlcohols().get(new DishType(24L))); // "виски"
		// safetyAddAlcohol(tmp, getPositions().getAlcohols().get(new DishType(26L)));// "текила"
		// } else {
		// }
		safetyAddAlcohol(tmp, getPositions().getAlcohols().get(new DishType(42L)));// "алкогольные напитки"
		safetyAddAlcohol(tmp, getPositions().getAlcohols().get(new DishType(27L))); // "алкогольные коктейли"
		addToResult(tmp, alcohol, ALCOHOL_COUNT - alcoholCount, alcoholDupl);
	    }
	}
    }

    private void safetyAddAlcohol(Set<Dish> to, Set<Dish> from) {
	if (from != null) {
	    to.addAll(from);
	}
    }

    private void checkDrageeBuffaloWingsDish() {
	Dish buffaloWings = new Dish(5882L);
	if (mainDishes.contains(buffaloWings) && includeAlcohol) {
	    Set<Dish> beers = getMainPositions().getAlcohols().get(BEER);
	    if (beers != null) {
		alcohol.add(beers.iterator().next());
	    } else {
		beers = getPositions().getAlcohols().get(BEER);
		if (beers != null) {
		    alcohol.add(beers.iterator().next());
		}
	    }
	    alcoholCount = alcohol.size();
	}
    }

    private void addDishes(Map<DishType, Set<Dish>> from, Set<Dish> to, int quantity, int duplicateTypes) {
	Set<Dish> tmp = new TreeSet<>(dishPhotoRatingComparator);
	from.values().forEach(items -> tmp.addAll(items));
	addToResult(tmp, to, quantity, duplicateTypes);
    }

    private void addPrefer(Map<DishType, Set<Dish>> from, Set<Dish> to, int quantity, int duplicateTypes) {
	Set<Dish> tmp = new TreeSet<>(dishPhotoRatingComparator);
	for (Map.Entry<DishType, Set<Dish>> mainPos : from.entrySet()) {
	    for (Dish dish : mainPos.getValue()) {
		if (isPrefer(dish.getTags())) {
		    tmp.add(dish);
		}
	    }
	}
	addToResult(tmp, to, quantity, duplicateTypes);
    }

    boolean isPrefer(List<Tag> dishTags) {
	return !Collections.disjoint(dishTags, preferTags);
    }

    public void addToResult(Set<Dish> from, Set<Dish> to, int quantity, int typeDuplicates) {
	int count = 0;
	for (Dish dish : from) {
	    int dishTypeCount = 0;
	    for (Dish toDish : to) {
		if (toDish.getDishType().equals(dish.getDishType())) {
		    dishTypeCount++;
		}
		if (dishTypeCount >= typeDuplicates) {
		    break;
		}
	    }
	    if (dishTypeCount < typeDuplicates) {
		to.add(dish);
		count++;
		if (count == quantity) {
		    return;
		}
	    }
	}
    }

}
