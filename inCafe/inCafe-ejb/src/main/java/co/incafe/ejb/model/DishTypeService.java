/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.DishType;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface DishTypeService extends InCafeWebAdminBeanService<DishType> {

}
