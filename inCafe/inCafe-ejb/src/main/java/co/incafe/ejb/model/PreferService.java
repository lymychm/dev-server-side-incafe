/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface PreferService extends InCafeWebAdminBeanService<Prefer> {

	public List<Prefer> selectByIds(List<Long> preferIds) throws InCafeException;

}
