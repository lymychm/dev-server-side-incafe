package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@Table(name = "incafe_user")
public class IncafeUser implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private String password;

	@ManyToMany
	@JoinTable(name = "incafe_user_group", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "group_id") })
	private List<IncafeGroup> incafeGroups;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "restaurant_chain_id")
	private RestaurantChain restaurantChain;

	public IncafeUser() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<IncafeGroup> getIncafeGroups() {
		return this.incafeGroups;
	}

	public void setIncafeGroups(List<IncafeGroup> incafeGroups) {
		this.incafeGroups = incafeGroups;
	}

	public RestaurantChain getRestaurantChain() {
		return restaurantChain;
	}

	public void setRestaurantChain(RestaurantChain restaurantChain) {
		this.restaurantChain = restaurantChain;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		IncafeUser other = (IncafeUser) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "IncafeUser [id=" + id + ", name=" + name + "]";
	}

}