/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface VisitService extends GeneralService<Visit> {

    public List<Visit> selectTodayVisitByUserAndRes(Long userId, Long resId) throws InCafeException;

    public List<Visit> selectTodayUsersInRestaurant(Long resId) throws InCafeException;

    public List<Visit> selectForLast30DaysByRestaurant(Long resId) throws InCafeException;
    
    public List<Visit> selectFromJan19Statistic(Long resId) throws InCafeException;

    public List<Visit> selectByUser(Long userId) throws InCafeException;

}
