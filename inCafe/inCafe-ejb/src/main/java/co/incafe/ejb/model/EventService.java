/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Event;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface EventService extends InCafeWebAdminBeanService<Event> {

	public Event getByName(String name) throws InCafeException;

	public Event getByNameAndRes(String name, Long resId) throws InCafeException;

	public Event selectByResChainAndName(Long id, String name) throws InCafeException;

	public List<Event> selectByResChain(Long resChainId) throws InCafeException;

	public long count(RestaurantChain restaurantChain, String searchField) throws InCafeException;

	public List<Event> selectLimited(int offset, int limit, RestaurantChain restaurantChain, String searchField) throws InCafeException;
	
	public boolean checkUniqueName(String name, RestaurantChain restaurantChain) throws EntityExistException, InCafeException;

}
