/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.Locale;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface LocaleService extends GeneralService<Locale> {

}
