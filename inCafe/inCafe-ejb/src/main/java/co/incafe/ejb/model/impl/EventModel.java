package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Event;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.EventService;
import co.incafe.ejb.model.util.FieldHolder;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class EventModel implements EventService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public EventModel() {
	}

	@Override
	public List<Event> selectAll() throws InCafeException {
		try {
			return ModelHelper.selectAll(Event.class, entityManager);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Event selectById(long id) throws InCafeException {
		try {
			return entityManager.find(Event.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(Event entity) throws EntityExistException, InCafeException {
		try {
			String name = entity.getName().trim().toLowerCase();
			checkUniqueName(name, entity.getRestaurantChain());
			entity.setName(name);
			entityManager.persist(entity);
		} catch (EntityExistException e) {
			throw new EntityExistException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(Event entity) throws EntityExistException, InCafeException {
		try {
			String name = entity.getName().trim().toLowerCase();
			entity.setName(name);
			checkUniqueName(name, entity.getRestaurantChain());
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(Event entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Event getByName(String name) throws InCafeException {
		try {
			String queryString = "SELECT x FROM Event x WHERE x.name = :name ";
			Query query = entityManager.createQuery(queryString, Event.class);
			query.setParameter("name", name);
			return (Event) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Event getByNameAndRes(String name, Long resId) throws InCafeException {
		try {
			String queryString = "SELECT x FROM Event x INNER JOIN x.restaurants r WHERE UPPER(x.name) = UPPER(:name) AND r.id = :resId";
			Query query = entityManager.createQuery(queryString, Event.class);
			query.setParameter("name", name);
			query.setParameter("resId", resId);
			return (Event) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Event selectByResChainAndName(Long resChainId, String name) throws InCafeException {
		try {
			String queryString = "SELECT x FROM Event x WHERE x.name = :name AND x.restaurantChain.id = :resChainId";
			Query query = entityManager.createQuery(queryString, Event.class);
			query.setParameter("name", name);
			query.setParameter("resChainId", resChainId);
			return (Event) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Event> selectByResChain(Long resChainId) throws InCafeException {
		try {
			String queryString = "SELECT x FROM Event x WHERE x.restaurantChain.id = :resChainId";
			Query query = entityManager.createQuery(queryString, Event.class);
			query.setParameter("resChainId", resChainId);
			return query.getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Event> selectLimited(int offset, int limit, RestaurantChain restaurantChain, String searchField) throws InCafeException {
		try {
			Query query = getSearchQuery(false, restaurantChain, searchField);
			return query.setFirstResult(offset).setMaxResults(limit).getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public long count(RestaurantChain restaurantChain, String searchField) throws InCafeException {
		try {
			Query query = getSearchQuery(true, restaurantChain, searchField);
			return (long) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return 0;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	private Query getSearchQuery(boolean isCount, RestaurantChain restaurantChain, String searchField) {
		String count = " count(x) ";
		String queryString = "SELECT" + (isCount ? count : " x ")
				+ "FROM Event x WHERE x.restaurantChain.id = :chainId AND (x.id = :id or (UPPER(x.name) like UPPER(:searchField)))" + (isCount ? "" : "ORDER By x.id");
		Query result = entityManager.createQuery(queryString, Event.class);

		result.setParameter(FieldHolder.SEARCH_FIELD, FieldHolder.PERCENT + searchField.trim() + FieldHolder.PERCENT);
		result.setParameter(FieldHolder.ID, ModelHelper.getNumberFromSearchField(searchField));
		result.setParameter(FieldHolder.CHAIN_ID, restaurantChain.getId());
		return result;
	}

	@Override
	public void checkUniqueName(String name) throws EntityExistException, InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT x FROM Event x WHERE UPPER(x.name) = UPPER(:name)", Event.class);
			query.setParameter("name", name);
			Event result = (Event) query.getSingleResult();
			if (result != null) {
				throw new EntityExistException();
			}
		} catch (NoResultException e) {
			/* NOP */
		} catch (EntityExistException e) {
			throw new EntityExistException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public List<Event> selectLimited(int offset, int limit, String searchField) throws InCafeException, Exception {
		throw new UnsupportedOperationException();
	}

	@Override
	public long count(String searchField) throws InCafeException, Exception {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean checkUniqueName(String name, RestaurantChain restaurantChain) throws EntityExistException, InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT count(x) FROM Event x WHERE UPPER(x.name) = UPPER(:name) AND x.restaurantChain.id = :chainId", Event.class);
			query.setParameter("name", name);
			query.setParameter(FieldHolder.CHAIN_ID, restaurantChain.getId());
			Long result = (Long) query.getSingleResult();
			return result > 0L ? true : false;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

}
