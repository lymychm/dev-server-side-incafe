package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToMany;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@NamedStoredProcedureQuery(name = "Dish.magic", procedureName = "v4_magic", parameters = { @StoredProcedureParameter(mode = ParameterMode.IN, name = "guid", type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, name = "ibeacon", type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, name = "time", type = java.sql.Time.class) })
public class Dish implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cook_time")
    private String cookTime;

    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createDate;

    @Column(name = "is_dish")
    private Integer isDish;

    @Column(name = "is_alcohol")
    private boolean isAlcohol;

    private String name;

    private String description;

    @Column(name = "photo_mini")
    private String photoMini;

    @Column(name = "photo_big")
    private String photoBig;

    private Double price;

    private Double rating;

    @Column(name = "res_dish_id")
    private String resDishId;

    private Integer status;

    private Float quantity;

    private String weight;

    @Column(name = "position_in_menu")
    private Integer positionInMenu;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar updateDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "dish_component", joinColumns = { @JoinColumn(name = "dish_id") }, inverseJoinColumns = { @JoinColumn(name = "component_id") })
    private List<Component> components;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dish_type_id")
    private DishType dishType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "main_tag_id")
    private MainTag mainTag;

    @ManyToMany
    @JoinTable(name = "dish_meal_time", joinColumns = { @JoinColumn(name = "dish_id") }, inverseJoinColumns = { @JoinColumn(name = "meal_time_id") })
    private List<MealTime> mealTimes;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "dish_tag", joinColumns = { @JoinColumn(name = "dish_id") }, inverseJoinColumns = { @JoinColumn(name = "tag_id") })
    private List<Tag> tags;

    @ManyToMany(mappedBy = "dishs")
    private List<Order> orders;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "dish_suggestions", joinColumns = { @JoinColumn(name = "dish_id") }, inverseJoinColumns = { @JoinColumn(name = "suggestions_id") })
    private List<Suggestion> suggestions;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "dish_restaurant", joinColumns = { @JoinColumn(name = "dish_id") }, inverseJoinColumns = { @JoinColumn(name = "restaurant_id") })
    private List<Restaurant> restaurants;

    @OneToMany(mappedBy = "dish")
    private List<UserDishRating> userDishRatings;

    @Transient
    private Long dishTypeId;

    @Transient
    private Long createDateTimeStamp;

    @Transient
    private Long updateDateTimeStamp;

    public Dish() {
    }

    public Dish(Long id) {
	super();
	this.id = id;
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public List<MealTime> getMealTimes() {
	return this.mealTimes;
    }

    public void setMealTimes(List<MealTime> mealTimes) {
	this.mealTimes = mealTimes;
    }

    public List<Tag> getTags() {
	return this.tags;
    }

    public void setTags(List<Tag> tags) {
	this.tags = tags;
    }

    public Double getPrice() {
	return price;
    }

    public void setPrice(Double price) {
	this.price = price;
    }

    public String getCookTime() {
	return cookTime;
    }

    public void setCookTime(String cookTime) {
	this.cookTime = cookTime;
    }

    public String getPhotoMini() {
	return photoMini;
    }

    public void setPhotoMini(String photoMini) {
	this.photoMini = photoMini;
    }

    public String getPhotoBig() {
	return photoBig;
    }

    public void setPhotoBig(String photoBig) {
	this.photoBig = photoBig;
    }

    public String getResDishId() {
	return resDishId;
    }

    public void setResDishId(String resDishId) {
	this.resDishId = resDishId;
    }

    public DishType getDishType() {
	if (dishType == null) {
	    dishType = new DishType();
	}
	return dishType;
    }

    public void setDishType(DishType dishType) {
	this.dishType = dishType;
    }

    public Integer getStatus() {
	return status;
    }

    public void setStatus(Integer status) {
	this.status = status;
    }

    public Double getRating() {
	return rating;
    }

    public void setRating(Double rating) {
	this.rating = rating;
    }

    public Calendar getCreateDate() {
	return createDate;
    }

    public void setCreateDate(Calendar createDate) {
	this.createDate = createDate;
    }

    public Calendar getUpdateDate() {
	return updateDate;
    }

    public void setUpdateDate(Calendar updateDate) {
	this.updateDate = updateDate;
    }

    public List<Suggestion> getSuggestions() {
	return suggestions;
    }

    public void setSuggestions(List<Suggestion> suggestions) {
	this.suggestions = suggestions;
    }

    public List<Component> getComponents() {
	return components;
    }

    public void setComponents(List<Component> components) {
	this.components = components;
    }

    public List<Order> getOrders() {
	return orders;
    }

    public void setOrders(List<Order> orders) {
	this.orders = orders;
    }

    public Float getQuantity() {
	return quantity;
    }

    public void setQuantity(Float quantity) {
	this.quantity = quantity;
    }

    public List<Restaurant> getRestaurants() {
	return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
	this.restaurants = restaurants;
    }

    public String getWeight() {
	return weight;
    }

    public void setWeight(String weight) {
	this.weight = weight;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Integer getIsDish() {
	return isDish;
    }

    public void setIsDish(Integer isDish) {
	this.isDish = isDish;
    }

    public Integer getPositionInMenu() {
	return positionInMenu;
    }

    public void setPositionInMenu(Integer positionInMenu) {
	this.positionInMenu = positionInMenu;
    }

    public Long getDishTypeId() {
	return dishTypeId;
    }

    public void setDishTypeId(Long dishTypeId) {
	this.dishTypeId = dishTypeId;
    }

    public boolean isAlcohol() {
	return isAlcohol;
    }

    public void setAlcohol(boolean isAlcohol) {
	this.isAlcohol = isAlcohol;
    }

    public MainTag getMainTag() {
	return mainTag;
    }

    public void setMainTag(MainTag mainTag) {
	this.mainTag = mainTag;
    }

    public List<UserDishRating> getUserDishRatings() {
	return userDishRatings;
    }

    public void setUserDishRatings(List<UserDishRating> userDishRatings) {
	this.userDishRatings = userDishRatings;
    }

    public Long getCreateDateTimeStamp() {
	return createDateTimeStamp;
    }

    public void setCreateDateTimeStamp(Long createDateTimeStamp) {
	this.createDateTimeStamp = createDateTimeStamp;
    }

    public Long getUpdateDateTimeStamp() {
	return updateDateTimeStamp;
    }

    public void setUpdateDateTimeStamp(Long updateDateTimeStamp) {
	this.updateDateTimeStamp = updateDateTimeStamp;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Dish other = (Dish) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Dish [id=" + id + ", name=" + name + "]";
    }

}