package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.exception.InCafeException;

public interface MagicService {

    public List<Long> magic(String userGuid, String ibeacon, Long time) throws InCafeException;

    public List<Dish> magicDishes(String userGuid, String ibeacon, Long time) throws InCafeException;

}
