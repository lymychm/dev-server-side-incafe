package co.incafe.ejb.exception;

/**
 * @author Mykhailo Lymych
 * 
 */
public class EntityExistException extends InCafeException {

	private static final long serialVersionUID = 1L;

	public EntityExistException() {
		super();
	}

	public EntityExistException(String msg) {
		super(msg);
	}

	public EntityExistException(Throwable cause) {
		super(cause);
	}

	public EntityExistException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
