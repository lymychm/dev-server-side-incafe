package co.incafe.ejb.entity.enums;

public enum ClientCallStatus {

    CALL, CONFIRMED, NOT_CONFIRMED;

}
