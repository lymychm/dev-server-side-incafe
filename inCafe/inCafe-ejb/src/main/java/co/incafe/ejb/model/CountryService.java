/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.Country;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface CountryService extends GeneralService<Country> {

	public Country selectByName(String name) throws InCafeException;

}
