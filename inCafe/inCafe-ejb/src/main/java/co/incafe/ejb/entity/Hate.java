package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@JsonSerialize(include = Inclusion.NON_NULL)
public class Hate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    private String name;

    @ManyToMany(mappedBy = "hates", fetch = FetchType.LAZY)
    private List<User> users;

    @ManyToMany
    @JoinTable(name = "hate_tag", joinColumns = { @JoinColumn(name = "hate_id") }, inverseJoinColumns = { @JoinColumn(name = "tag_id") })
    private List<Tag> tags;

    public Hate() {
    }

    public Hate(Long id) {
	this.id = id;
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Product getProduct() {
	return this.product;
    }

    public void setProduct(Product product) {
	this.product = product;
    }

    public List<User> getUsers() {
	return this.users;
    }

    public void setUsers(List<User> users) {
	this.users = users;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public List<Tag> getTags() {
	return tags;
    }

    public void setTags(List<Tag> tags) {
	this.tags = tags;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Hate other = (Hate) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Hate [id=" + id + ", name=" + name + "]";
    }

}