package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
public class Event implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	@Temporal(TemporalType.DATE)
	@Column(name = "end_date")
	private Calendar endDate;

	@Temporal(TemporalType.DATE)
	@Column(name = "start_date")
	private Calendar startDate;

	@OneToMany(mappedBy = "event", fetch = FetchType.LAZY)
	private List<EventStatistic> eventStatistics;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "event_restaurant", joinColumns = { @JoinColumn(name = "event_id") }, inverseJoinColumns = { @JoinColumn(name = "res_id") })
	private List<Restaurant> restaurants;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "res_chain")
	private RestaurantChain restaurantChain;

	@Transient
	private Boolean editable;

	private boolean active;

	public Event() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public List<EventStatistic> getEventStatistics() {
		return this.eventStatistics;
	}

	public void setEventStatistics(List<EventStatistic> eventStatistics) {
		this.eventStatistics = eventStatistics;
	}

	public RestaurantChain getRestaurantChain() {
		return this.restaurantChain;
	}

	public void setRestaurantChain(RestaurantChain restaurantChain) {
		this.restaurantChain = restaurantChain;
	}

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	public List<Restaurant> getRestaurants() {
		if (restaurants == null) {
			restaurants = new ArrayList<>();
		}
		return restaurants;
	}

	public void setRestaurants(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", name=" + name + ", endDate=" + (endDate != null ? endDate.getTime() : null) + ", startDate="
				+ (startDate != null ? startDate.getTime() : null) + "]";
	}

}