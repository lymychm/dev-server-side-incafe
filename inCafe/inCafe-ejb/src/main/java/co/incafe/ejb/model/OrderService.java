/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Event;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface OrderService extends GeneralService<Order> {

    public List<Order> selectConfirmedOrders(Long resId) throws InCafeException;

    public List<Order> selectByIds(List<Long> orderId) throws InCafeException;

    public List<Order> selectOrdersByResstaurant(Long resId) throws InCafeException;

    public Event freeCoffee(Long userId, Long resId) throws InCafeException;

    public List<Order> selectByUser(User user) throws InCafeException;

    public List<Order> selectTodayOrdersByResId(Long resId) throws InCafeException;

    public int selectOrdersCountByUserAndRes(Long userId, Long resId) throws InCafeException;

    public List<Order> selectTodayOrdersByUserAndRes(Long userId, Long resId) throws InCafeException;

    public List<Order> selectForLast30DaysByRestaurant(Long resId) throws InCafeException;

    public List<Order> selectFromJan19Statistic(Long resId) throws InCafeException;

    public boolean isNewUser(Long userId, Long resId) throws InCafeException;

    public boolean isRegularUser(Long userId, Long resId) throws InCafeException;

}
