package co.incafe.ejb.entity.helper;

import java.io.Serializable;

import javax.persistence.Transient;

public class Editable implements Serializable {

	private static final long serialVersionUID = -3252067227034934895L;

	@Transient
	private Boolean editable;

	public Boolean getEditable() {
		return editable;
	}

	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

}
