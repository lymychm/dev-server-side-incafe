package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<Dish> dishs;

    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<Allergy> allergies;

    @ManyToMany(mappedBy = "tags", fetch = FetchType.LAZY)
    private List<Product> products;

    @Transient
    private Boolean editable;

    public Tag() {
    }

    public Tag(Long id) {
	super();
	this.id = id;
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public List<Dish> getDishs() {
	return dishs;
    }

    public void setDishs(List<Dish> dishs) {
	this.dishs = dishs;
    }

    public Boolean getEditable() {
	return editable;
    }

    public void setEditable(Boolean editable) {
	this.editable = editable;
    }

    public List<Allergy> getAllergies() {
	return allergies;
    }

    public void setAllergies(List<Allergy> allergies) {
	this.allergies = allergies;
    }

    public List<Product> getProducts() {
	return products;
    }

    public void setProducts(List<Product> products) {
	this.products = products;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Tag other = (Tag) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Tag [id=" + id + ", name=" + name + ", description=" + description + "]";
    }

}