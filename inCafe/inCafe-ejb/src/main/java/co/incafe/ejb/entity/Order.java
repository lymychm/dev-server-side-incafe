package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.incafe.ejb.entity.enums.OrderStatus;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@Table(name = "orders")
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Double checksum = 0D;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "order_date")
	private Calendar orderDate;

	@Enumerated(EnumType.ORDINAL)
	private OrderStatus status;

	@ManyToMany
	@JoinTable(name = "dish_order", joinColumns = { @JoinColumn(name = "order_id", updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "dish_id", updatable = false) })
	private List<Dish> dishs;

	@OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
	private List<EventStatistic> eventStatistics;

	@ManyToOne(fetch = FetchType.LAZY)
	private Restaurant restaurant;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "table_id")
	private Tables table;

	@ManyToOne(fetch = FetchType.LAZY)
	private User user;

	public Order() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getChecksum() {
		return this.checksum;
	}

	public void setChecksum(Double checksum) {
		this.checksum = checksum;
	}

	public Calendar getOrderDate() {
		return this.orderDate;
	}

	public void setOrderDate(Calendar orderDate) {
		this.orderDate = orderDate;
	}

	public List<Dish> getDishs() {
		return dishs;
	}

	public void setDishs(List<Dish> dishs) {
		this.dishs = dishs;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public Tables getTable() {
		return table;
	}

	public void setTable(Tables table) {
		this.table = table;
	}

	public List<EventStatistic> getEventStatistics() {
		if (eventStatistics == null) {
			eventStatistics = new ArrayList<>();
		}
		return eventStatistics;
	}

	public void setEventStatistics(List<EventStatistic> eventStatistics) {
		this.eventStatistics = eventStatistics;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Order other = (Order) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", checksum=" + checksum + ", orderDate=" + (orderDate != null ? orderDate.getTime() : null) + "]";
	}
}