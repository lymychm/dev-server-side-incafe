package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@Table(name = "dish_type")
public class DishType implements Serializable, Comparable<DishType> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @OneToMany(mappedBy = "dishType", fetch = FetchType.LAZY)
    private List<Dish> dishes;

    @OneToMany(mappedBy = "dishType", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<MainTag> mainTags;

    @Transient
    private Boolean editable;

    public DishType() {
    }

    public DishType(Long id) {
	this.id = id;
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public List<Dish> getDishes() {
	return dishes;
    }

    public void setDishes(List<Dish> dishes) {
	this.dishes = dishes;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Boolean getEditable() {
	return editable;
    }

    public void setEditable(Boolean editable) {
	this.editable = editable;
    }

    public List<MainTag> getMainTags() {
	return mainTags;
    }

    public void setMainTags(List<MainTag> mainTags) {
	this.mainTags = mainTags;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	DishType other = (DishType) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "DishType [id=" + id + ", name=" + name + ", description=" + description + "]";
    }

    @Override
    public int compareTo(DishType dishType) {
	return id.compareTo(dishType.getId());
    }

}