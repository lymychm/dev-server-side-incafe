package co.incafe.ejb.magic.strategy;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.DishType;

public class Positions {

    private Map<DishType, Set<Dish>> dishes = new TreeMap<>();

    private Map<DishType, Set<Dish>> drinks = new TreeMap<>();

    private Map<DishType, Set<Dish>> alcohols = new TreeMap<>();

    public Map<DishType, Set<Dish>> getDishes() {
	return dishes;
    }

    public Map<DishType, Set<Dish>> getDrinks() {
	return drinks;
    }

    public Map<DishType, Set<Dish>> getAlcohols() {
	return alcohols;
    }

}
