package co.incafe.ejb.util;


/**
 * @author Mykhailo Lymych
 * 
 */
public class ModelUtils {


	public static String toUpperCaseFirstChar(String name) {
		name = name.trim();
		char first = name.charAt(0);
		String result = Character.toUpperCase(first) + name.substring(1, name.length()).toLowerCase();
		return result;
	}

}
