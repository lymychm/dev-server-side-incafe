package co.incafe.ejb.entity.enums;

public enum OrderStatus {

    CENCEL,

    ORDER,

    CONFIRM,

    CHECK,

    NOT_CONFIRMED,

}
