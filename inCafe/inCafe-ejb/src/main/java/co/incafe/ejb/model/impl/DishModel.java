package co.incafe.ejb.model.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TemporalType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.DishService;
import co.incafe.ejb.model.TableService;
import co.incafe.ejb.model.util.FieldHolder;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.date.DateUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class DishModel implements DishService {

    private static final List<Long> EMPTY_LIST = Arrays.asList(-1L);

    private static final double PREV_DISH_RATING = 0.2D;

    public static final int MAGIC_QUANTITY = 8;

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @EJB
    private TableService tableService;

    public DishModel() {
    }

    @Override
    public List<Dish> selectAll() throws InCafeException {
	throw new UnsupportedOperationException();
    }

    @Override
    public Dish selectById(long id) throws InCafeException {
	try {
	    return entityManager.find(Dish.class, id);
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void insert(Dish entity) throws InCafeException {
	try {
	    entityManager.persist(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void update(Dish entity) throws InCafeException {
	try {
	    entityManager.merge(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void delete(Dish entity) throws InCafeException {
	try {
	    entity = selectById(entity.getId());
	    entityManager.remove(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dish> selectAll(Long resId) throws InCafeException {
	try {
	    String queryString = "SELECT d FROM Dish d INNER JOIN d.restaurants r WHERE r.id IN :resId AND d.status = 1 ORDER BY d.dishType.id, d.photoBig";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resId", Arrays.asList(resId));
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dish> selectAll(Long resId, Long date) throws InCafeException {
	try {
	    String queryString = "SELECT d FROM Dish d INNER JOIN d.restaurants r WHERE r.id IN :resId and (d.createDate >= :date or d.updateDate >= :date)";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resId", Arrays.asList(resId));
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(date);
	    query.setParameter("date", calendar);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dish> selectByType(List<Long> typeIds, Long resId) throws InCafeException {
	try {
	    String queryString = "SELECT d FROM Dish d WHERE d.restaurant.id = :resId AND d.dishType.id IN :typeIds";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resId", resId);
	    query.setParameter("typeIds", typeIds);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public Dish selectByResDishId(String resDishId) throws InCafeException {
	try {
	    String queryString = "SELECT d FROM Dish d WHERE d.resDishId = :resDishId";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resDishId", resDishId);
	    return (Dish) query.getSingleResult();
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    return null;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public List<Dish> selectLimited(Long resId, int offset, int limit) throws InCafeException {
	throw new UnsupportedOperationException("TODO");
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dish> selectByIds(List<Long> ids, Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT DISTINCT d FROM Dish d INNER JOIN d.restaurants r WHERE r.id IN :resId AND d.id IN :ids", Dish.class);
	    query.setParameter("resId", Arrays.asList(resId));
	    query.setParameter("ids", ids);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Long> magic(String guid, String ibeacon) throws InCafeException {
	try {
	    long start = System.currentTimeMillis();
	    List<Long> result = new ArrayList<>();
	    StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery("Dish.magic");
	    query.setParameter("guid", guid);
	    query.setParameter("ibeacon", ibeacon);
	    List<Object> resultList = query.getResultList();
	    for (Object array : resultList) {
		Object[] resultId = (Object[]) array;
		result.add((Long) resultId[0]);
	    }
	    long end = System.currentTimeMillis();
	    logger.info("TIME: " + (double) (end - start) / 1000);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Long> magic(String guid, String ibeacon, Long time) throws InCafeException {
	try {
	    long start = System.currentTimeMillis();
	    List<Long> result = new ArrayList<>();
	    StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery("Dish.magic");
	    query.setParameter("guid", guid);
	    query.setParameter("ibeacon", ibeacon);
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(time);
	    query.setParameter("time", calendar, TemporalType.TIME);
	    List<Object> resultList = query.getResultList();
	    for (Object array : resultList) {
		Object[] resultId = (Object[]) array;
		result.add((Long) resultId[0]);
	    }
	    long end = System.currentTimeMillis();
	    logger.info("OLD MAGIC EXECUTION TIME: " + (double) (end - start) / 1000);
	    return result;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dish> selectLimitedByRestaurantChain(int offset, int limit, RestaurantChain restaurantChain, String searchField) throws InCafeException {
	try {
	    Query query = getSearchQuery(false, restaurantChain, searchField);
	    return query.setFirstResult(offset).setMaxResults(limit).getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public long count(int offset, int limit, RestaurantChain restaurantChain, String searchField) throws InCafeException {
	try {
	    Query query = getSearchQuery(true, restaurantChain, searchField);
	    return (long) query.getSingleResult();
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    return 0;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    private Query getSearchQuery(boolean isCount, RestaurantChain restaurantChain, String searchField) {
	String count = " count(DISTINCT d) ";
	String search = " DISTINCT d ";
	String queryString = "SELECT" + (isCount ? count : search)
		+ "FROM Dish d INNER JOIN d.restaurants r WHERE r.restaurantChain.id = :chainId AND ((UPPER(d.name) like UPPER(:searchField) OR d.id = :id))"
		+ (isCount ? "" : " ORDER BY d.id");
	Query result = entityManager.createQuery(queryString, Dish.class);

	result.setParameter(FieldHolder.CHAIN_ID, restaurantChain.getId());
	result.setParameter(FieldHolder.SEARCH_FIELD, FieldHolder.PERCENT + searchField.trim() + FieldHolder.PERCENT);
	result.setParameter(FieldHolder.ID, ModelHelper.getNumberFromSearchField(searchField));
	return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dish> selectAllAndIgnoreByTags(Long resId, Set<Long> ignoreTags, boolean includeAlcohol, List<Long> ignoreTypes, List<Long> ignoreDishes) throws InCafeException {
	try {
	    String queryString = "SELECT DISTINCT d FROM Dish d INNER JOIN d.restaurants r WHERE r.id IN :resId and d.id NOT IN (SELECT x.id FROM Dish x INNER JOIN x.restaurants r inner join x.tags t WHERE r.id IN :resId and t.id IN :tags ) AND d.id NOT IN :ignoreDishes and d.status = 1 and d.isAlcohol IN :includeAlcohol and d.dishType.id NOT IN :ignoreTypes ";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resId", Arrays.asList(resId));
	    query.setParameter("includeAlcohol", Arrays.asList(false, includeAlcohol));
	    query.setParameter("tags", ignoreTags.isEmpty() ? EMPTY_LIST : ignoreTags);
	    query.setParameter("ignoreTypes", ignoreTypes.isEmpty() ? EMPTY_LIST : ignoreTypes);
	    query.setParameter("ignoreDishes", ignoreDishes.isEmpty() ? EMPTY_LIST : ignoreDishes);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dish> selectByUserRating(Long resId, Long userId, boolean includeAlcohol, List<Long> ignoreTypes) throws InCafeException {
	try {
	    String queryString = "SELECT DISTINCT d FROM Dish d INNER JOIN d.restaurants r " + "INNER JOIN d.userDishRatings u " + "WHERE r.id IN :resId "
		    + "and u.user.id = :userId " + "and d.status = 1 " + "and u.rating > :rating " + "and u.lastUpdate > :date " + "and d.isAlcohol IN :includeAlcohol "
		    + "and d.dishType.id NOT IN :ignoreTypes " + "ORDER BY d.rating desc";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resId", Arrays.asList(resId));
	    query.setParameter("rating", PREV_DISH_RATING);
	    query.setParameter("userId", userId);
	    query.setParameter("ignoreTypes", ignoreTypes.isEmpty() ? EMPTY_LIST : ignoreTypes);
	    query.setParameter("includeAlcohol", Arrays.asList(false, includeAlcohol));
	    Calendar date = DateUtil.removeTimeFromDate(Calendar.getInstance(), -30);
	    query.setParameter("date", date);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Dish> selectByMainTags(Long resId, Long userId, boolean includeAlcohol, List<Long> ignoreTypes) throws InCafeException {
	try {
	    String queryString = "SELECT DISTINCT d FROM Dish d INNER JOIN d.restaurants r " + "WHERE r.id IN :resId " + "and d.status = 1 " + "and d.isAlcohol IN :includeAlcohol "
		    + "and d.mainTag.id IN (SELECT x.id FROM UserMainTagRating x where x.user.id = :userId and x.rating > :rating and x.lastUpdate > :date ) "
		    + "and d.dishType.id NOT IN :ignoreTypes " + "ORDER BY d.rating desc";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resId", Arrays.asList(resId));
	    query.setParameter("rating", PREV_DISH_RATING);
	    query.setParameter("userId", userId);
	    query.setParameter("ignoreTypes", ignoreTypes.isEmpty() ? EMPTY_LIST : ignoreTypes);
	    query.setParameter("includeAlcohol", Arrays.asList(false, includeAlcohol));
	    Calendar date = DateUtil.removeTimeFromDate(Calendar.getInstance(), -30);
	    query.setParameter("date", date);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public Dish selectByRestaurantData(String resDishId, Long resId) throws InCafeException {
	try {
	    String queryString = "SELECT d FROM Dish d inner join d.restaurants r WHERE d.resDishId = :resDishId AND r.id IN :resId";
	    Query query = entityManager.createQuery(queryString, Dish.class);
	    query.setParameter("resDishId", resDishId);
	    query.setParameter("resId", Arrays.asList(resId));
	    return (Dish) query.getSingleResult();
	} catch (NoResultException e) {
	    return null;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }
}
