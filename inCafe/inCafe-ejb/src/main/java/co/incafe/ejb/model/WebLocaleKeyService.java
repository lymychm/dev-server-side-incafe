/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.WebLocaleKey;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface WebLocaleKeyService extends InCafeWebAdminBeanService<WebLocaleKey> {

}
