package co.incafe.ejb.magic.strategy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.entity.comparators.DishPhotoRatingComparator;
import co.incafe.ejb.entity.comparators.MagicComparator;
import co.incafe.ejb.entity.enums.Gender;

public abstract class MagicEntity implements MagicStrategy {

    protected static final DishType MAIN_DISH = new DishType(3L);

    protected static final int MAX_POSITIONS = 10;

    protected Gender gender;

    protected int currentPositions = 0;

    protected int dishDupl = 2;

    protected int drinkDupl = 1;

    protected int alcoholDupl = 1;

    // protected static Comparator<Dish> magicComparator = new DishTypePhotoRatingComparator();

    protected static Comparator<Dish> magicComparator = new MagicComparator();

    protected static Comparator<Dish> dishPhotoRatingComparator = new DishPhotoRatingComparator();

    private Positions mainPositions = new Positions();

    private Positions positions = new Positions();

    protected List<Tag> preferTags = new ArrayList<>();

    protected abstract void prepareData();

    public void fillData(List<Dish> dishs, Positions positions) {
	for (Dish dish : dishs) {
	    boolean isDish = dish.getIsDish().equals(1) ? true : false;
	    boolean isAlcohol = dish.isAlcohol();
	    if (isDish) {
		addDish(dish, positions.getDishes(), dishDupl);
	    } else {
		if (isAlcohol) {
		    addDish(dish, positions.getAlcohols(), alcoholDupl);
		} else {
		    addDish(dish, positions.getDrinks(), drinkDupl);
		}
	    }
	}
    }

    public void addDish(Dish dish, Map<DishType, Set<Dish>> result, int typeDuplicates) {
	Set<Dish> list = result.get(dish.getDishType());
	if (list == null) {
	    list = new TreeSet<>(dishPhotoRatingComparator);
	    list.add(dish);
	    result.put(dish.getDishType(), list);
	} else {
	    list.add(dish);
	}
    }

    public Positions getMainPositions() {
	return mainPositions;
    }

    public Positions getPositions() {
	return positions;
    }

}
