package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@Table(name = "main_tag")
public class MainTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dish_type_id")
    private DishType dishType;

    @OneToMany(mappedBy = "mainTag", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<UserMainTagRating> userMainTagRatings;

    @Transient
    private Boolean editable;

    public MainTag() {
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public DishType getDishType() {
	return this.dishType;
    }

    public void setDishType(DishType dishType) {
	this.dishType = dishType;
    }

    public List<UserMainTagRating> getUserMainTagRatings() {
	return this.userMainTagRatings;
    }

    public void setUserMainTagRatings(List<UserMainTagRating> userMainTagRatings) {
	this.userMainTagRatings = userMainTagRatings;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Boolean getEditable() {
	return editable;
    }

    public void setEditable(Boolean editable) {
	this.editable = editable;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	MainTag other = (MainTag) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    @Override
    public String toString() {
	return "MainTag [id=" + id + ", name=" + name + ", description=" + description + "]";
    }

}