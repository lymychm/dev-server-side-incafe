/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.Component;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface ComponentService extends GeneralService<Component> {

	public Component getByNameAndRestaurant(String name, Long restaurant) throws InCafeException;

}
