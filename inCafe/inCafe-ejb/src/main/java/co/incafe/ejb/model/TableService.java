/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface TableService extends GeneralService<Tables> {

	public Tables selectByIbaconId(String ibeacon) throws InCafeException;

}
