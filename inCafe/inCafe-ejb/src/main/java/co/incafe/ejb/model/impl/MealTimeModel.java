package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.MealTime;
import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.MealTimeService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class MealTimeModel implements MealTimeService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public MealTimeModel() {
	}

	@Override
	public List<MealTime> selectAll() throws InCafeException {
		try {
			return ModelHelper.selectAll(MealTime.class, entityManager);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public MealTime selectById(long id) throws InCafeException {
		try {
			return entityManager.find(MealTime.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(MealTime entity) throws EntityExistException, InCafeException {
		try {
			entityManager.persist(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(MealTime entity) throws EntityExistException, InCafeException {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(MealTime entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

}
