package co.incafe.ejb.magic.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import co.incafe.ejb.entity.Dish;
import co.incafe.ejb.entity.DishType;
import co.incafe.ejb.entity.Prefer;
import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.entity.User;

public class LunchMagic extends MagicEntity {

    private static final DishType LUNCH = new DishType(1L);

    private static final int DISHES_COUNT = 4;

    private static final int DRINK_COUNT = 3;

    private static final int LUNCH_COUNT = 2;

    private static final int MAIN_DISH_COUNT = 2;

    private Set<Dish> lunchs = new HashSet<>();

    private Set<Dish> mainDishes = new HashSet<>();

    private Set<Dish> dishes = new HashSet<>();

    private Set<Dish> drinks = new HashSet<>();

    int lunchCount = 0;

    int mainDishCount = 0;

    int dishCount = 0;

    int drinksCount = 0;

    public LunchMagic(User user) {
	// preferTags.forEach(prefer -> prefer.getTags().forEach(tag -> this.preferTags.add(tag)));
	for (Prefer prefer : user.getPrefers()) {
	    for (Tag tag : prefer.getTags()) {
		this.preferTags.add(tag);
	    }
	}
	this.gender = user.getGender();
    }

    @Override
    protected void prepareData() {
	fillLunch();
	fillMainDish();
	fillDishes();
	fillDrinks();
    }

    @Override
    public List<Long> doMagic() {
	prepareData();
	List<Long> res = new ArrayList<>();
	Set<Dish> result = new TreeSet<>(magicComparator);
	lunchs.forEach(dish -> result.add(dish));
	mainDishes.forEach(dish -> result.add(dish));
	dishes.forEach(dish -> result.add(dish));
	drinks.forEach(dish -> result.add(dish));
	result.forEach(dish -> res.add(dish.getId()));
	return res;
    }

    @Override
    public List<Dish> doMagicDishes() {
	prepareData();
	List<Dish> result = new ArrayList<>();
	lunchs.forEach(dish -> result.add(dish));
	mainDishes.forEach(dish -> result.add(dish));
	dishes.forEach(dish -> result.add(dish));
	drinks.forEach(dish -> result.add(dish));
	Collections.sort(result, magicComparator);
	return result;
    }

    private void fillLunch() {
	Set<Dish> mLunches = getMainPositions().getDishes().get(LUNCH);
	if (mLunches != null) {
	    for (Dish dish : mLunches) {
		lunchs.add(dish);
		lunchCount++;
		if (lunchCount == LUNCH_COUNT) {
		    break;
		}
	    }
	    lunchCount = lunchs.size();
	}
	Set<Dish> lunches = getPositions().getDishes().get(LUNCH);
	if (lunchCount < LUNCH_COUNT && lunches != null) {
	    for (Dish dish : lunches) {
		lunchs.add(dish);
		lunchCount++;
		if (lunchCount == LUNCH_COUNT) {
		    break;
		}
	    }
	    lunchCount = lunchs.size();
	}
	getMainPositions().getDishes().remove(LUNCH);
	getPositions().getDishes().remove(LUNCH);
    }

    private void fillMainDish() {
	Set<Dish> mmd = getMainPositions().getDishes().get(MAIN_DISH);
	if (mmd != null) {
	    for (Dish dish : mmd) {
		mainDishes.add(dish);
		mainDishCount++;
		if (mainDishCount == MAIN_DISH_COUNT) {
		    break;
		}
	    }
	    mainDishCount = mainDishes.size();
	}
	Set<Dish> md = getPositions().getDishes().get(MAIN_DISH);
	if (mainDishCount < MAIN_DISH_COUNT && md != null) {
	    for (Dish dish : md) {
		mainDishes.add(dish);
		mainDishCount++;
		if (mainDishCount == MAIN_DISH_COUNT) {
		    break;
		}
	    }
	    mainDishCount = mainDishes.size();
	}
	getMainPositions().getDishes().remove(MAIN_DISH);
	getPositions().getDishes().remove(MAIN_DISH);
    }

    private void fillDishes() {
	addDishes(getMainPositions().getDishes(), dishes, DISHES_COUNT + dishCount(), dishDupl);
	dishCount = dishes.size();
	if ((DISHES_COUNT + dishCount() - dishCount) > 0) {
	    addPrefer(getPositions().getDishes(), dishes, (DISHES_COUNT + dishCount() - dishCount), dishDupl);
	    dishCount = dishes.size();
	}
	if ((DISHES_COUNT + dishCount() - dishCount) > 0) {
	    addDishes(getPositions().getDishes(), dishes, (DISHES_COUNT + dishCount() - dishCount), dishDupl);
	}
    }

    private int dishCount() {
	return (LUNCH_COUNT + MAIN_DISH_COUNT) - (lunchCount + mainDishCount);
    }

    private void fillDrinks() {
	addDishes(getMainPositions().getDrinks(), drinks, DRINK_COUNT, drinkDupl);
	drinksCount = drinks.size();
	if ((DRINK_COUNT - drinksCount) > 0) {
	    addPrefer(getPositions().getDrinks(), drinks, DRINK_COUNT - drinksCount, drinkDupl);
	    drinksCount = drinks.size();
	}
	if ((DRINK_COUNT - drinksCount) > 0) {
	    addDishes(getPositions().getDrinks(), drinks, DRINK_COUNT - drinksCount, drinkDupl);
	}
    }

    private void addDishes(Map<DishType, Set<Dish>> from, Set<Dish> to, int quantity, int duplicateTypes) {
	Set<Dish> tmp = new TreeSet<>(dishPhotoRatingComparator);
	from.values().forEach(items -> tmp.addAll(items));
	addToResult(tmp, to, quantity, duplicateTypes);
    }

    private void addPrefer(Map<DishType, Set<Dish>> from, Set<Dish> to, int quantity, int duplicateTypes) {
	Set<Dish> tmp = new TreeSet<>(dishPhotoRatingComparator);
	for (Map.Entry<DishType, Set<Dish>> mainPos : from.entrySet()) {
	    for (Dish dish : mainPos.getValue()) {
		if (isPrefer(dish.getTags())) {
		    tmp.add(dish);
		}
	    }
	}
	addToResult(tmp, to, quantity, duplicateTypes);
    }

    boolean isPrefer(List<Tag> dishTags) {
	return !Collections.disjoint(dishTags, preferTags);
    }

    public void addToResult(Set<Dish> from, Set<Dish> to, int quantity, int typeDuplicates) {
	int count = 0;
	for (Dish dish : from) {
	    int dishTypeCount = 0;
	    for (Dish toDish : to) {
		if (toDish.getDishType().equals(dish.getDishType())) {
		    dishTypeCount++;
		}
		if (dishTypeCount >= typeDuplicates) {
		    break;
		}
	    }
	    if (dishTypeCount < typeDuplicates) {
		to.add(dish);
		count++;
		if (count == quantity) {
		    return;
		}
	    }
	}
    }

}
