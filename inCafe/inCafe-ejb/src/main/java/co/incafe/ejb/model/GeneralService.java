package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.exception.EntityExistException;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 * @param <T>
 */
public interface GeneralService<T> {

	public List<T> selectAll() throws InCafeException;

	public T selectById(long id) throws InCafeException;

	public void insert(T entity) throws EntityExistException, InCafeException;

	public void update(T entity) throws EntityExistException, InCafeException;

	public void delete(T entity) throws InCafeException;

}
