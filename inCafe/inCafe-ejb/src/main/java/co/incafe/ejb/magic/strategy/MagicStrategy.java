package co.incafe.ejb.magic.strategy;

import java.util.List;

import co.incafe.ejb.entity.Dish;

public interface MagicStrategy {

    public List<Long> doMagic();

    public List<Dish> doMagicDishes();

}
