package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
public class Restaurant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "region_uuid")
    private String regionUUID;

    private String description;

    private Integer status;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY)
    private List<Component> components;

    @ManyToMany(mappedBy = "restaurants", fetch = FetchType.LAZY)
    private List<Dish> dishes;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY)
    private List<Order> orders;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY)
    private List<ClientCall> calls;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_id")
    private RestaurantContact contact;

    @ManyToOne(fetch = FetchType.LAZY)
    private City city;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant_chain_id")
    private RestaurantChain restaurantChain;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "restaurant_restaurant_type", joinColumns = { @JoinColumn(name = "restaurant_id") }, inverseJoinColumns = { @JoinColumn(name = "restaurant_type_id") })
    private List<RestaurantType> restaurantTypes;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY)
    private List<Tables> tables;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY)
    private List<Tablet> tablets;

    @OneToMany(mappedBy = "restaurant", fetch = FetchType.LAZY)
    private List<Visit> visits;

    @ManyToMany(mappedBy = "restaurants")
    private List<Event> events;

    @OneToMany(mappedBy = "restaurant")
    private List<EventStatistic> eventStatistics;

    public Restaurant() {
    }

    public Restaurant(String regionUUID) {
	super();
	this.regionUUID = regionUUID;
    }

    public Restaurant(Long id) {
	super();
	this.id = id;
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public City getCity() {
	return this.city;
    }

    public void setCity(City city) {
	this.city = city;
    }

    public List<Dish> getDishes() {
	return dishes;
    }

    public void setDishes(List<Dish> dishes) {
	this.dishes = dishes;
    }

    public List<Tables> getTables() {
	return this.tables;
    }

    public void setTables(List<Tables> tables) {
	this.tables = tables;
    }

    public List<Tablet> getTablets() {
	return tablets;
    }

    public void setTablets(List<Tablet> tablets) {
	this.tablets = tablets;
    }

    public RestaurantContact getContact() {
	return contact;
    }

    public void setContact(RestaurantContact contact) {
	this.contact = contact;
    }

    public List<Visit> getVisits() {
	return visits;
    }

    public void setVisits(List<Visit> visits) {
	this.visits = visits;
    }

    public List<Order> getOrders() {
	return orders;
    }

    public void setOrders(List<Order> orders) {
	this.orders = orders;
    }

    public List<RestaurantType> getRestaurantTypes() {
	return restaurantTypes;
    }

    public void setRestaurantTypes(List<RestaurantType> restaurantTypes) {
	this.restaurantTypes = restaurantTypes;
    }

    public List<Component> getComponents() {
	return components;
    }

    public void setComponents(List<Component> components) {
	this.components = components;
    }

    public List<ClientCall> getCalls() {
	return calls;
    }

    public void setCalls(List<ClientCall> calls) {
	this.calls = calls;
    }

    public String getRegionUUID() {
	return regionUUID;
    }

    public void setRegionUUID(String regionUUID) {
	this.regionUUID = regionUUID;
    }

    public RestaurantChain getRestaurantChain() {
	return restaurantChain;
    }

    public void setRestaurantChain(RestaurantChain restaurantChain) {
	this.restaurantChain = restaurantChain;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Integer getStatus() {
	return status;
    }

    public void setStatus(Integer status) {
	this.status = status;
    }

    public List<Event> getEvents() {
	return events;
    }

    public void setEvents(List<Event> events) {
	this.events = events;
    }

    public List<EventStatistic> getEventStatistics() {
	return eventStatistics;
    }

    public void setEventStatistics(List<EventStatistic> eventStatistics) {
	this.eventStatistics = eventStatistics;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Restaurant other = (Restaurant) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "Restaurant [id=" + id + ", name=" + name + ", contact=" + contact + "]";
    }

}