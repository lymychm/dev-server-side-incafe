/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.MealTime;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface MealTimeService extends GeneralService<MealTime> {

}
