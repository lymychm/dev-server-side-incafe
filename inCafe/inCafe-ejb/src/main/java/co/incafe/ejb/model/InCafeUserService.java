/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.IncafeUser;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface InCafeUserService extends GeneralService<IncafeUser> {

	public IncafeUser selectByName(String name) throws InCafeException;

	public RestaurantChain selectByUserName(String userName) throws InCafeException;

}
