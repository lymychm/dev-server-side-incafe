/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface TabletService extends GeneralService<Tablet> {

	public Tables selectByIbaconId(String ibeacon) throws InCafeException;

	public List<Tablet> selectByResId(long resId) throws InCafeException;

	public List<Tablet> selectTabletsByResWithoutOne(long resId, String guid) throws InCafeException;

	public Tablet selectByGuid(String guid) throws InCafeException;

	public Tablet selectByPushId(String pushId) throws InCafeException;

	public Restaurant registration(String guid, String pushId) throws InCafeException;

	public List<Tablet> selectByResIdWithPushId(Long resId) throws InCafeException;

}
