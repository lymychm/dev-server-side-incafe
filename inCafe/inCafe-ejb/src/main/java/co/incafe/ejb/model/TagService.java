/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.Tag;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface TagService extends InCafeWebAdminBeanService<Tag> {

	public Tag getByName(String name) throws InCafeException;

}
