package co.incafe.ejb.exception;

/**
 * @author Mykhailo Lymych
 * 
 */
public class InCafeException extends Exception {

	private static final long serialVersionUID = 1L;

	public InCafeException() {
		super();
	}

	public InCafeException(String msg) {
		super(msg);
	}

	public InCafeException(Throwable cause) {
		super(cause);
	}

	public InCafeException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
