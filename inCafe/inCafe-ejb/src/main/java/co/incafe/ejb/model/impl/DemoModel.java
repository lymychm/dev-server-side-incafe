package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.demo.Demo;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.DemoService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class DemoModel implements DemoService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public DemoModel() {
	}

	@Override
	public List<Demo> selectAll() throws InCafeException {
		try {
			return ModelHelper.selectAll(Demo.class, entityManager);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Demo selectById(long id) throws InCafeException {
		try {
			return entityManager.find(Demo.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(Demo entity) throws InCafeException {
		try {
			entityManager.persist(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(Demo entity) throws InCafeException {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(Demo entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Demo selectDemo(String guid, String ibeacon) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT u FROM Demo u WHERE u.user.guid = :guid and u.table.ibeacon = :ibeacon", Demo.class);
			query.setParameter("guid", guid);
			query.setParameter("ibeacon", ibeacon);
			return (Demo) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

}
