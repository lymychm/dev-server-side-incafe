/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.MainTag;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface MainTagService extends InCafeWebAdminBeanService<MainTag> {

}
