package co.incafe.ejb.model.impl;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Event;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.enums.OrderStatus;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.EventService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.date.DateUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class OrderModel implements OrderService {

    private static final long REGULAR_USER_ORDERS_COUNT = 3L;

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @EJB
    private EventService eventService;

    public OrderModel() {
    }

    @Override
    public List<Order> selectAll() throws InCafeException {
	try {
	    return ModelHelper.selectAll(Order.class, entityManager);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public Order selectById(long id) throws InCafeException {
	try {
	    return entityManager.find(Order.class, id);
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void insert(Order entity) throws InCafeException {
	try {
	    entityManager.persist(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void update(Order entity) throws InCafeException {
	try {
	    entityManager.merge(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void delete(Order entity) throws InCafeException {
	try {
	    entity = selectById(entity.getId());
	    entityManager.remove(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectConfirmedOrders(Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM Order x WHERE x.restaurant.id = :resId AND x.status = :status", Order.class);
	    query.setParameter("resId", resId);
	    query.setParameter("status", OrderStatus.CONFIRM);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectByIds(List<Long> ids) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM Order x WHERE x.id IN :ids", Order.class);
	    query.setParameter("ids", ids);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectOrdersByResstaurant(Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM Order x WHERE x.restaurant.id = :resId", Order.class);
	    query.setParameter("resId", resId);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectByUser(User user) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM Order x WHERE x.user.id = :userId", Order.class);
	    query.setParameter("userId", user.getId());
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectTodayOrdersByResId(Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM Order x WHERE x.restaurant.id = :resId AND x.orderDate >= :date AND x.status IN :status", Order.class);
	    query.setParameter("resId", resId);
	    query.setParameter("date", DateUtil.removeTimeFromDate(Calendar.getInstance(), -1), TemporalType.TIMESTAMP);
	    query.setParameter("status", Arrays.asList(OrderStatus.ORDER, OrderStatus.CONFIRM));
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public Event freeCoffee(Long userId, Long resId) throws InCafeException {
	try {
	    Event event = eventService.getByNameAndRes("free_coffee", resId);
	    logger.info("event : " + event);
	    if (event == null || !event.isActive() || event.getEndDate().before(Calendar.getInstance()) || event.getStartDate().after(Calendar.getInstance())) {
		return null;
	    }
	    Query query = entityManager.createQuery(
		    "SELECT count(x) FROM Order x WHERE x.restaurant.id = :resId AND x.user.id = :userId AND x.orderDate > :startDate AND x.status IN :status", Order.class);
	    query.setParameter("resId", resId);
	    query.setParameter("userId", userId);
	    query.setParameter("status", Arrays.asList(OrderStatus.CONFIRM, OrderStatus.CHECK));
	    query.setParameter("startDate", event.getStartDate());
	    Long result = (Long) query.getSingleResult();
	    logger.info("FREE COFFEE: " + result);
	    return result > 0L ? null : event;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectTodayOrdersByUserAndRes(Long userId, Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM Order x WHERE x.restaurant.id = :resId AND x.user.id = :userId AND x.orderDate > :date AND x.status != :status",
		    Order.class);
	    query.setParameter("resId", resId);
	    query.setParameter("userId", userId);
	    Calendar date = DateUtil.removeTimeFromDate(Calendar.getInstance(), -1);
	    query.setParameter("date", date, TemporalType.TIMESTAMP);
	    query.setParameter("status", OrderStatus.CHECK);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public int selectOrdersCountByUserAndRes(Long userId, Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT count(x) FROM Order x WHERE x.restaurant.id = :resId AND x.user.id = :userId", Order.class);
	    query.setParameter("resId", resId);
	    query.setParameter("userId", userId);
	    return (int) query.getSingleResult();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectForLast30DaysByRestaurant(Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM Order x WHERE x.restaurant.id = :resId and x.orderDate > :date", Order.class);
	    query.setParameter("resId", resId);
	    Calendar date = Calendar.getInstance();
	    date.add(Calendar.DAY_OF_MONTH, -30);
	    DateUtil.removeTimeFromDate(date);
	    query.setParameter("date", date, TemporalType.TIMESTAMP);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectFromJan19Statistic(Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM Order x WHERE x.restaurant.id = :resId and x.orderDate > :date", Order.class);
	    query.setParameter("resId", resId);
	    Calendar date = Calendar.getInstance();
	    date.set(Calendar.YEAR, 2016);
	    date.set(Calendar.MONTH, Calendar.JANUARY);
	    date.set(Calendar.DAY_OF_MONTH, 19);
	    DateUtil.removeTimeFromDate(date);
	    query.setParameter("date", date, TemporalType.TIMESTAMP);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public boolean isNewUser(Long userId, Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT count(x) FROM Order x WHERE x.restaurant.id = :resId AND x.user.id = :userId ", Order.class);
	    query.setParameter("resId", resId);
	    query.setParameter("userId", userId);
	    Long result = (Long) query.getSingleResult();
	    return result > 0L ? false : true;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public boolean isRegularUser(Long userId, Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery(
		    "SELECT count(x) FROM Order x WHERE x.restaurant.id = :resId AND x.user.id = :userId AND x.orderDate > :startDate AND x.status IN :status", Order.class);
	    query.setParameter("resId", resId);
	    query.setParameter("userId", userId);
	    query.setParameter("status", Arrays.asList(OrderStatus.CONFIRM, OrderStatus.CHECK));
	    Calendar lastTwoWeeks = Calendar.getInstance();
	    lastTwoWeeks.add(Calendar.DAY_OF_MONTH, -14);
	    query.setParameter("startDate", lastTwoWeeks);
	    Long result = (Long) query.getSingleResult();
	    return result > REGULAR_USER_ORDERS_COUNT ? true : false;
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

}
