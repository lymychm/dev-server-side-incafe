package co.incafe.ejb.model.sheduler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.entity.Order;
import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.User;
import co.incafe.ejb.entity.Visit;
import co.incafe.ejb.entity.enums.ClientCallStatus;
import co.incafe.ejb.entity.enums.OrderStatus;
import co.incafe.ejb.entity.enums.VisitStatus;
import co.incafe.ejb.model.ClientCallService;
import co.incafe.ejb.model.OrderService;
import co.incafe.ejb.model.VisitService;
import co.incafe.util.general.LoggerUtil;

@Singleton
public class CheckUserInRestaurantSheduler {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    @EJB
    private VisitService visitService;

    @EJB
    private OrderService orderService;

    @EJB
    private ClientCallService clientCallService;

    @PostConstruct
    private void init() {
	logger.info("CheckUserInRestaurantSheduler INIT");
    }

    @PreDestroy
    private void destroy() {
	logger.info("CheckUserInRestaurantSheduler DESTROY");
    }

    @Schedule(hour = "*/1")
    public void cleanVisits() {
	try {
	    logger.info("CheckUserInRestaurantSheduler " + new Date());

	    Calendar calendar = Calendar.getInstance();
	    logger.info(calendar.getTime());

	    calendar.add(Calendar.MINUTE, -45);
	    logger.info(calendar.getTime());

	    List<Order> orders = getOrders(calendar);
	    List<ClientCall> calls = getCalls(calendar);
	    List<Visit> visits = getVisits(calendar);
	    logger.info("cleanVisits, visits:  " + visits.size());
	    visits = checkUsersForDelete(orders, calls, visits, calendar);
	    for (Visit visit : visits) {
		Long userId = visit.getUser().getId();
		Long resId = visit.getRestaurant().getId();

		orders = orderService.selectTodayOrdersByUserAndRes(userId, resId);
		for (Order order : orders) {
		    logger.info("UPDATE ORDER - " + order);
		    order.setStatus(OrderStatus.NOT_CONFIRMED);
		    orderService.update(order);
		}

		calls = clientCallService.selectTodayUserCallsByResId(userId, resId);
		for (ClientCall call : calls) {
		    logger.info("UPDATE CALL - " + call);
		    call.setStatus(ClientCallStatus.NOT_CONFIRMED);
		    clientCallService.update(call);
		}

		visit.setOutTime(Calendar.getInstance());
		visit.setStatus(VisitStatus.LEFT);
		visitService.update(visit);
		logger.info("REMOVE CLIENT FROM RESTAURANT - " + visit.getUser().getId() + " - " + visit.getId());
	    }
	} catch (Exception e) {
	    logger.error("cleanVisits : ", e);
	}
    }

    private List<Visit> checkUsersForDelete(List<Order> orders, List<ClientCall> calls, List<Visit> visits, Calendar date) {
	List<Visit> visitToRemove = new ArrayList<>();
	for (Visit visit : visits) {
	    User user = visit.getUser();
	    Restaurant restaurant = visit.getRestaurant();
	    boolean foundInOrder = false;
	    boolean isOrderLate = false;

	    boolean foundInCall = false;
	    boolean isCallLate = false;
	    for (Order order : orders) {
		User orderUser = order.getUser();
		if (user.equals(orderUser)) {
		    foundInOrder = true;
		    if (order.getOrderDate().before(date) && order.getRestaurant().equals(restaurant)) {
			isOrderLate = true;
		    }
		}
	    }
	    for (ClientCall call : calls) {
		User callUser = call.getUser();
		if (user.equals(callUser)) {
		    foundInCall = true;
		    if (call.getCallTime().before(date) && call.getRestaurant().equals(restaurant)) {
			isCallLate = true;
		    }
		}
	    }
	    if (((foundInOrder && foundInCall) && (isOrderLate && isCallLate)) || (!foundInOrder && !foundInCall) || (foundInOrder && isOrderLate && !foundInCall)
		    || (foundInCall && isCallLate && !foundInOrder)) {
		visitToRemove.add(visit);
	    }
	}
	return visitToRemove;
    }

    @SuppressWarnings("unchecked")
    private List<Visit> getVisits(Calendar calendar) {
	Query query = entityManager.createQuery("select v from Visit v where v.outTime = null and v.inTime > :today and v.inTime < :date");
	query.setParameter("date", calendar, TemporalType.TIMESTAMP);
	query.setParameter("today", todayWithoutTime(), TemporalType.TIMESTAMP);
	List<Visit> result = query.getResultList();
	return result;
    }

    @SuppressWarnings("unchecked")
    private List<ClientCall> getCalls(Calendar calendar) {
	Query query = entityManager
		.createQuery("select c from ClientCall c where c.callTime IN (select max(x.callTime) from ClientCall x WHERE x.callTime > :today group by x.user.id)");
	query.setParameter("today", todayWithoutTime(), TemporalType.TIMESTAMP);
	List<ClientCall> result = query.getResultList();
	return result;
    }

    @SuppressWarnings("unchecked")
    private List<Order> getOrders(Calendar calendar) {
	Query query = entityManager.createQuery("select o from Order o where o.orderDate IN (select max(o.orderDate) from Order o WHERE o.orderDate > :today group by o.user.id)");
	query.setParameter("today", todayWithoutTime(), TemporalType.TIMESTAMP);
	List<Order> result = query.getResultList();
	return result;
    }

    public Calendar todayWithoutTime() {
	Calendar calendar = Calendar.getInstance();
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.MILLISECOND, 0);
	calendar.add(Calendar.HOUR, -1);
	return calendar;
    }

}
