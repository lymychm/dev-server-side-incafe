package co.incafe.ejb.entity.enums;

public enum VisitStatus {

	COME, TABLE, LEFT;

}
