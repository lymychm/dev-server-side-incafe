package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Restaurant;
import co.incafe.ejb.entity.Tables;
import co.incafe.ejb.entity.Tablet;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.TabletService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class TabletModel implements TabletService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public TabletModel() {
	}

	@Override
	public List<Tablet> selectAll() throws InCafeException {
		try {
			return ModelHelper.selectAll(Tablet.class, entityManager);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Tablet selectById(long id) throws InCafeException {
		try {
			return entityManager.find(Tablet.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(Tablet entity) throws InCafeException {
		try {
			entityManager.persist(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(Tablet entity) throws InCafeException {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(Tablet entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Tables selectByIbaconId(String ibeacon) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT u FROM Tables u WHERE u.ibeacon = :ibeacon", Tables.class);
			query.setParameter("ibeacon", ibeacon);
			return (Tables) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Tablet> selectByResId(long resId) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT u FROM Tablet u WHERE u.restaurant.id = :resId AND u.pushId != NULL", Tablet.class);
			query.setParameter("resId", resId);
			return query.getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Tablet selectByGuid(String guid) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT t FROM Tablet t WHERE t.guid = :guid", Tablet.class);
			query.setParameter("guid", guid);
			return (Tablet) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Restaurant registration(String guid, String pushId) throws InCafeException {
		try {
			Tablet tablet = selectByGuid(guid);
			if (tablet == null) {
				throw new RuntimeException("Tablet with guid - " + guid + " does not exist");
			}
			tablet.setPushId(pushId);
			tablet.setStatus(1);
			update(tablet);
			return tablet.getRestaurant();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Tablet> selectTabletsByResWithoutOne(long resId, String guid) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT u FROM Tablet u WHERE u.restaurant.id = :resId AND u.guid != :guid AND u.pushId != null", Tablet.class);
			query.setParameter("resId", resId);
			query.setParameter("guid", guid);
			return query.getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Tablet> selectByResIdWithPushId(Long resId) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT u FROM Tablet u WHERE u.restaurant.id = :resId AND u.pushId != null", Tablet.class);
			query.setParameter("resId", resId);
			return query.getResultList();
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public Tablet selectByPushId(String pushId) throws InCafeException {
		try {
			Query query = entityManager.createQuery("SELECT u FROM Tablet u WHERE u.pushId = :pushId", Tablet.class);
			query.setParameter("pushId", pushId);
			return (Tablet) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

}
