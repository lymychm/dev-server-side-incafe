package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.Hate;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.HateService;
import co.incafe.ejb.model.util.FieldHolder;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class HateModel implements HateService {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public HateModel() {
    }

    @Override
    public List<Hate> selectAll() throws InCafeException {
	try {
	    return ModelHelper.selectAll(Hate.class, entityManager);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public Hate selectById(long id) throws InCafeException {
	try {
	    return entityManager.find(Hate.class, id);
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void insert(Hate entity) throws InCafeException {
	try {
	    entityManager.persist(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void update(Hate entity) throws InCafeException {
	try {
	    entityManager.merge(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void delete(Hate entity) throws InCafeException {
	try {
	    entity = selectById(entity.getId());
	    entityManager.remove(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Hate> selectByIds(List<Long> allergyIds) throws InCafeException {
	try {
	    String queryString = "SELECT x FROM Hate x WHERE x.id IN :id";
	    Query query = entityManager.createQuery(queryString, Hate.class);
	    query.setParameter(FieldHolder.ID, allergyIds);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

}
