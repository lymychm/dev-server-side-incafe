package co.incafe.ejb.model.impl;

import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.entity.enums.ClientCallStatus;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.ClientCallService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.date.DateUtil;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class ClientCallModel implements ClientCallService {

    private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

    @PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;

    public ClientCallModel() {
    }

    @Override
    public List<ClientCall> selectAll() throws InCafeException {
	try {
	    return ModelHelper.selectAll(ClientCall.class, entityManager);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public ClientCall selectById(long id) throws InCafeException {
	try {
	    return entityManager.find(ClientCall.class, id);
	} catch (NoResultException e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void insert(ClientCall entity) throws InCafeException {
	try {
	    entityManager.persist(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void update(ClientCall entity) throws InCafeException {
	try {
	    entityManager.merge(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    public void delete(ClientCall entity) throws InCafeException {
	try {
	    entity = selectById(entity.getId());
	    entityManager.remove(entity);
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ClientCall> selectByIds(List<Long> ids) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM ClientCall x WHERE x.id IN :ids", ClientCall.class);
	    query.setParameter("ids", ids);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ClientCall> selectTodayCallsByResId(Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM ClientCall x WHERE x.restaurant.id = :resId AND x.callTime >= :date AND x.status = :status", ClientCall.class);
	    query.setParameter("resId", resId);
	    query.setParameter("date", DateUtil.removeTimeFromDate(Calendar.getInstance(), -1), TemporalType.TIMESTAMP);
	    query.setParameter("status", ClientCallStatus.CALL);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ClientCall> selectTodayUserCallsByResId(Long userId, Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery(
		    "SELECT x FROM ClientCall x WHERE x.restaurant.id = :resId AND x.user.id = :userId AND x.callTime >= :date AND x.status != :status", ClientCall.class);
	    query.setParameter("resId", resId);
	    query.setParameter("userId", userId);
	    query.setParameter("date", DateUtil.removeTimeFromDate(Calendar.getInstance(), -1), TemporalType.TIMESTAMP);
	    query.setParameter("status", ClientCallStatus.CONFIRMED);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ClientCall> selectByUser(Long userId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM ClientCall x WHERE x.user.id = :userId", ClientCall.class);
	    query.setParameter("userId", userId);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ClientCall> selectForLast30DaysByRes(Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM ClientCall x WHERE x.restaurant.id = :resId AND x.callTime >= :date", ClientCall.class);
	    query.setParameter("resId", resId);
	    Calendar date = Calendar.getInstance();
	    date.add(Calendar.DAY_OF_MONTH, -30);
	    DateUtil.removeTimeFromDate(date);
	    query.setParameter("date", date, TemporalType.TIMESTAMP);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<ClientCall> selectFromJan19Statistic(Long resId) throws InCafeException {
	try {
	    Query query = entityManager.createQuery("SELECT x FROM ClientCall x WHERE x.restaurant.id = :resId AND x.callTime >= :date", ClientCall.class);
	    query.setParameter("resId", resId);
	    Calendar date = Calendar.getInstance();
	    date.set(Calendar.YEAR, 2016);
	    date.set(Calendar.MONTH, Calendar.JANUARY);
	    date.set(Calendar.DAY_OF_MONTH, 19);
	    DateUtil.removeTimeFromDate(date);
	    query.setParameter("date", date, TemporalType.TIMESTAMP);
	    return query.getResultList();
	} catch (Exception e) {
	    logger.error(getClass().getName(), e);
	    throw new InCafeException(e);
	}
    }

}
