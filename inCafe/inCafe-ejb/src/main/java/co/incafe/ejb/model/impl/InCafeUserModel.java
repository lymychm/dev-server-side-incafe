package co.incafe.ejb.model.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import co.incafe.ejb.entity.IncafeUser;
import co.incafe.ejb.entity.RestaurantChain;
import co.incafe.ejb.exception.InCafeException;
import co.incafe.ejb.model.InCafeUserService;
import co.incafe.ejb.model.util.ModelHelper;
import co.incafe.util.general.LoggerUtil;

/**
 * @author Mykhailo Lymych
 * 
 */
@Stateless
public class InCafeUserModel implements InCafeUserService {

	private static final Logger logger = LogManager.getLogger(LoggerUtil.getClassName());

	@PersistenceContext(name = "incafe.unit", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;

	public InCafeUserModel() {
	}

	@Override
	public List<IncafeUser> selectAll() throws InCafeException {
		try {
			return ModelHelper.selectAll(IncafeUser.class, entityManager);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public IncafeUser selectById(long id) throws InCafeException {
		try {
			return entityManager.find(IncafeUser.class, id);
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void insert(IncafeUser entity) throws InCafeException {
		try {
			entityManager.persist(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void update(IncafeUser entity) throws InCafeException {
		try {
			entityManager.merge(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public void delete(IncafeUser entity) throws InCafeException {
		try {
			entity = selectById(entity.getId());
			entityManager.remove(entity);
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public IncafeUser selectByName(String name) throws InCafeException {
		try {
			String queryString = "SELECT u FROM IncafeUser u WHERE u.name = :name";
			Query query = entityManager.createQuery(queryString, IncafeUser.class);
			query.setParameter("name", name);
			return (IncafeUser) query.getSingleResult();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

	@Override
	public RestaurantChain selectByUserName(String userName) throws InCafeException {
		try {
			String queryString = "SELECT u FROM IncafeUser u WHERE u.name = :name";
			Query query = entityManager.createQuery(queryString, IncafeUser.class);
			query.setParameter("name", userName);
			return ((IncafeUser) query.getSingleResult()).getRestaurantChain();
		} catch (NoResultException e) {
			logger.error(getClass().getName(), e);
			return null;
		} catch (Exception e) {
			logger.error(getClass().getName(), e);
			throw new InCafeException(e);
		}
	}

}
