/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.ClientCall;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface ClientCallService extends GeneralService<ClientCall> {

    public List<ClientCall> selectByIds(List<Long> ids) throws InCafeException;

    public List<ClientCall> selectTodayCallsByResId(Long resId) throws InCafeException;

    public List<ClientCall> selectTodayUserCallsByResId(Long userId, Long resId) throws InCafeException;

    public List<ClientCall> selectByUser(Long userId) throws InCafeException;

    public List<ClientCall> selectForLast30DaysByRes(Long resId) throws InCafeException;

    public List<ClientCall> selectFromJan19Statistic(Long resId) throws InCafeException;

}
