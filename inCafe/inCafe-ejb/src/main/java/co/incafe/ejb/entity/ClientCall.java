package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import co.incafe.ejb.entity.enums.CallingCode;
import co.incafe.ejb.entity.enums.ClientCallStatus;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@Table(name = "client_call")
public class ClientCall implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "call_code")
    @Enumerated(EnumType.ORDINAL)
    private CallingCode callCode;

    @Enumerated(EnumType.ORDINAL)
    private ClientCallStatus status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "call_time")
    private Calendar callTime;

    @Column(name = "confirm_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar confirmTime;

    @ManyToOne(fetch = FetchType.LAZY)
    private Restaurant restaurant;

    @ManyToOne(fetch = FetchType.LAZY)
    private Tables table;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public ClientCall() {
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public CallingCode getCallCode() {
	return callCode;
    }

    public void setCallCode(CallingCode callCode) {
	this.callCode = callCode;
    }

    public Calendar getCallTime() {
	return callTime;
    }

    public void setCallTime(Calendar callTime) {
	this.callTime = callTime;
    }

    public Restaurant getRestaurant() {
	return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
	this.restaurant = restaurant;
    }

    public Tables getTable() {
	return table;
    }

    public void setTable(Tables table) {
	this.table = table;
    }

    public User getUser() {
	return user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public ClientCallStatus getStatus() {
	return status;
    }

    public void setStatus(ClientCallStatus status) {
	this.status = status;
    }

    public Calendar getConfirmTime() {
	return confirmTime;
    }

    public void setConfirmTime(Calendar confirmTime) {
	this.confirmTime = confirmTime;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	ClientCall other = (ClientCall) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "ClientCall [id=" + id + ", callCode=" + callCode + ", status=" + status + ", callTime=" + callTime + ", restaurant=" + restaurant + ", table=" + table + ", user="
		+ user + "]";
    }

}