/**
 * 
 */
package co.incafe.ejb.model;

import co.incafe.ejb.entity.UserDishRating;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface UserDishRatingService extends GeneralService<UserDishRating> {

}
