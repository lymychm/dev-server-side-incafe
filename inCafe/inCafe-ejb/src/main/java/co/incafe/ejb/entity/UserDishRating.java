package co.incafe.ejb.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Mykhailo Lymych
 * 
 */
@Entity
@Table(name = "user_dish_rating")
public class UserDishRating implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Dish dish;

    private Double rating;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_update")
    private Calendar lastUpdate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public UserDishRating() {
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Double getRating() {
	return rating;
    }

    public void setRating(Double rating) {
	this.rating = rating;
    }

    public Dish getDish() {
	return dish;
    }

    public void setDish(Dish dish) {
	this.dish = dish;
    }

    public User getUser() {
	return user;
    }

    public void setUser(User user) {
	this.user = user;
    }

    public Calendar getLastUpdate() {
	return lastUpdate;
    }

    public void setLastUpdate(Calendar lastUpdate) {
	this.lastUpdate = lastUpdate;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	UserDishRating other = (UserDishRating) obj;
	if (id == null) {
	    if (other.id != null) {
		return false;
	    }
	} else if (!id.equals(other.id)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return "UserDishRating [id=" + id + ", dish=" + (dish != null ? dish.getId() : null) + ", rating=" + rating + ", lastUpdate="
		+ (lastUpdate != null ? lastUpdate.getTime() : null) + ", user=" + (user != null ? user.getId() : null) + "]";
    }

}