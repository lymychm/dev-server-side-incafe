/**
 * 
 */
package co.incafe.ejb.model;

import java.util.List;

import co.incafe.ejb.entity.Hate;
import co.incafe.ejb.exception.InCafeException;

/**
 * @author Mykhailo Lymych
 * 
 */
public interface HateService extends GeneralService<Hate> {

	public List<Hate> selectByIds(List<Long> hateIds) throws InCafeException;

}
